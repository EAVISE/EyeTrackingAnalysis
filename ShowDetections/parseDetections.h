#ifndef PARSEDETECTIONS_H
#define PARSEDETECTIONS_H

#include <iostream>
#include "opencv2/opencv.hpp"
#include <boost/bind.hpp>
#include <fstream>
#include <string>     // std::string, std::stoi

using namespace cv;
using namespace std;
// structures
struct detectionBB
{
    Rect BB;
    string name;
    float score;
};

struct DetectionsStruct
{
    string path;
    int nrDetections;
    int framenr;
    vector<detectionBB> boxes;
};


class ParseDetections
{
    public:
        ParseDetections(string path, vector<DetectionsStruct> &detections);
};


#endif // PARSEACFDETECTIONS_H
