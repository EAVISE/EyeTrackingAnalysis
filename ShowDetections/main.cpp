#include <iostream>
#include "opencv2/opencv.hpp"
#include <boost/bind.hpp>
#include <fstream>
#include <string>     // std::string, std::stoi

#include "parseDetections.h"
using namespace std;
using namespace cv;

//#define Debug
//#define Show
//#define Save

int main(int argc, char* argv[])
{
    
    if (argc != 2) {
            // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " <Path to root image directory>" << std::endl;
            return -1;
    }
    
    // parse the Detections
    string path1 = argv[1];
    string temp1 = path1;
    string DetectionsList = temp1.append("detections.txt");

    vector<DetectionsStruct> detections;
    ParseDetections dets(DetectionsList, detections);

#ifdef Debug
    cout << "give overview of detections" << endl;

    for(int i=0;i<detections.size();i++)
    {
        cout << "frame number = " << detections[i].framenr << endl;
        for(int j=0;j<detections[i].boxes.size();j++)
        {
            cout << "\t" << detections[i].boxes[j].BB  << "\t score = "<<  detections[i].boxes[j].score << endl;
        }
    }
#endif

    // READ IMAGE LIST
    string path = argv[1];
    string temp = path;
    string pathImageList = temp.append("Image_files.txt");

    ifstream ImageList;
    ImageList.open(pathImageList.c_str());

    if(!ImageList.is_open())
        cerr << "unable to open XML file list:" << pathImageList << endl;

    string line2;
    vector<string> Images;
    if (ImageList.is_open())
    {
        while ( getline (ImageList,line2) )
        {
            string temp = path;
            Images.push_back(temp.append(line2));
        }
        ImageList.close();
    }
    cout << "there are " << Images.size() << " images in this folder" << endl;

    for(int i=0;i<Images.size();i++)
    {
        cout << "path =  " << Images[i] << endl;

        string temp =  Images[i].substr (Images[i].size()-9,5);
        int frame = atoi(temp.c_str());
        Mat image;
        image = imread(Images[i]);

        // find detections for this frame
        vector<detectionBB> boxes;
        std::vector< DetectionsStruct >::iterator it = std::find_if ( detections.begin (), detections.end (),
        boost::bind ( &DetectionsStruct::framenr, _1 ) == frame );
        if(it != detections.end() )
        {
            boxes = (*it).boxes;
        }

        if(!image.empty())
        {
            // in case boxes were found for this frame, draw them!
            for(int b=0;b<boxes.size();b++)
            {
                if(boxes[b].score>55)
                {
                    rectangle(image,boxes[b].BB,Scalar(255,255,0),2);
                }
                cout << "\tdetections= " << boxes[b].BB << "\t score = " <<  boxes[b].score << endl;
            }
			#ifdef Show
			imshow("image",image);
			waitKey();
			#endif
			
			#ifdef Save
            char output[200];
            sprintf(output, "%s%05d%s", "Detections/", frame-2000, ".ppm");
            imwrite(output,image);
			#endif
            
        }
    }

    return 0;
}

