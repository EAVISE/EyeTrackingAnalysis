%function PRCurve

%names = {'result50', 'result60', 'result70', 'result80','result90', 'result100', 'result110'};
names = {'test'};

nr_kids = size(names, 2);

thresholds = 50;

% Empty matrix
data = zeros(thresholds, 6, nr_kids);

for i=1:nr_kids
    filename = ['/home/sdb/EyeTrackingAnalysis/PRCalculation_ACF/' names{i} '.txt'];
    
    [TH1 TP1 FP1 FN1 P1 R1] = textread(filename', '%f %d %d %d %f %f');
    data(:,:,i) = [TH1 TP1 FP1 FN1 P1 R1];
end


figure(1)
clf;
hold on;
colors = distinguishable_colors(nr_kids);
set(gcf, 'color', [1 1 1]);
axis([0 1 0 1])
set(gca,'FontSize',10)
xlabel('Recall', 'fontsize', 12);
ylabel('Precision', 'fontsize', 12);
title('Accuracy face recognition of each ID', 'fontsize', 14)
grid on;

for i=1:nr_kids
    get_data_kid = data(:,:,i);
    P = get_data_kid(:, 5);
    R = get_data_kid(:, 6);
    
    plot(R, P, 'color', colors(i,:), 'linewidth',2);
end

legend(names, 'location', 'ne');

mean_empty = zeros(thresholds, 6);

% Calculate mean
for i=1:nr_kids
    mean_empty = mean_empty + data(:,:,i);
end

P = mean_empty(:,2)./(mean_empty(:,2)+mean_empty(:,3));
R = mean_empty(:,2)./(mean_empty(:,2)+mean_empty(:,4));

figure(2);
clf;
plot(R, P, 'color', 'r', 'linewidth', 2);
hold on;
axis([0 1 0 1]);
grid on;
set(gcf, 'color', [1 1 1]);
axis([0 1 0 1.05])
set(gca,'FontSize',10)
xlabel('Recall', 'fontsize', 12);
ylabel('Precision', 'fontsize', 12);
title('Average accuracy face recognition', 'fontsize', 14)



%end