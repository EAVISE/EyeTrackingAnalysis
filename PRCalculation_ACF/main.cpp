#include <iostream>
#include "opencv2/opencv.hpp"
#include "OpenXml.h"
#include <boost/bind.hpp>
#include <fstream>
using namespace std;
using namespace cv;
// structures

struct annotationIDStruct
{
    string id;
    int number;
    vector<AnnotationStruct> annotations;
};

struct detectionBB
{
    Rect BB;
    string name;
    float score;
};

struct DetectionsStruct
{
    string path;
    int nrDetections;
    int framenr;
    vector<detectionBB> boxes;
};

vector<annotationIDStruct> parseAnnotations(string path, bool makeSubSets)
{
    string temp = path;
    string pathXmlList = temp.append("XML_files.txt");

    // define ID vector
    vector<annotationIDStruct> annotations_ID;

    ifstream fileList;
    fileList.open(pathXmlList.c_str());

    if(!fileList.is_open())
        cerr << "unable to open XML file list:" << pathXmlList << endl;

    string line;
    vector<string> XMLfiles;
    if (fileList.is_open())
    {
        while ( getline (fileList,line) )
        {
          string temp = path;
          XMLfiles.push_back(temp.append(line));
        }
        fileList.close();
    }
    cout << "there are " << XMLfiles.size() << " xml files in this folder" << endl;

    // parse the XML filesmake
    vector<AnnotationStruct> annotations;

    for(int i=0;i<XMLfiles.size();i++)
    {
        //cout << "index " << i  << "\tXML file = " << XMLfiles[i] << endl;
        OpenXml xml(XMLfiles[i]);
        xml.XmlToData(annotations);
    }

    // show the annotations:
    cout << "there are " << annotations.size() << " annotation files " << endl;
    for(int i=0;i<annotations.size();i++)
    {
        string temp = annotations[i].path;
        //cout << "image " << i << "\t" << temp << endl;

        for(int j=0;j<annotations[i].boxes.size();j++)
        {
            //cout << "\t name = " << annotations[i].boxes[j].name << endl;
            if(annotations_ID.size()==0)
            {
                // if vector is empty, add first item
                annotationIDStruct entry;
                entry.id = annotations[i].boxes[j].name;
                entry.number = 1;
                entry.annotations.push_back(annotations[i]);
                annotations_ID.push_back(entry);
            }

            if(makeSubSets)
            {
                // check if this id is already stored in the vector
                std::vector< annotationIDStruct >::iterator it = std::find_if ( annotations_ID.begin (), annotations_ID.end (),
                boost::bind ( &annotationIDStruct::id, _1 ) == annotations[i].boxes[j].name );
                if(it != annotations_ID.end() )
                {
                    //cout << "ID " << annotations[i].boxes[j].name << " was found in vecor " << endl;
                    (*it).number++;
                    (*it).annotations.push_back(annotations[i]);
                }
                else
                {
                    annotationIDStruct entry;
                    entry.id = annotations[i].boxes[j].name;
                    entry.number = 1;
                    entry.annotations.push_back(annotations[i]);
                    annotations_ID.push_back(entry);
                }
            }
            else
            {
                annotations_ID[0].annotations.push_back(annotations[i]);
            }
        }
    }


    return annotations_ID;
}

vector<DetectionsStruct> parseDetections(string detectionsList)
{
    ifstream DetectionFile;
    DetectionFile.open(detectionsList.c_str());

    string line;
    int frameNr;
    vector<string> tokens;
    vector<DetectionsStruct> detections;

    if (DetectionFile.is_open())
    {
        while ( getline (DetectionFile,line) )
        {
            tokens.clear();
            //cout << "line = " << line << endl;
            std::istringstream ss(line);
            std::string token;

            while(std::getline(ss, token, ',')) {
                tokens.push_back(token);
            }

            if(tokens.size()==6)
            {
                detectionBB tempRect;

                frameNr = atoi(tokens[0].c_str());
                tempRect.BB.x = atof(tokens[1].c_str());
                tempRect.BB.y = atof(tokens[2].c_str());
                tempRect.BB.width = atof(tokens[3].c_str());
                tempRect.BB.height = atof(tokens[4].c_str());
                tempRect.score = atof(tokens[5].c_str());

                // MAKE DETECTIONS VECTOR
                if(detections.size()==0)
                {
                    DetectionsStruct structure;
                    structure.framenr = frameNr;
                    structure.boxes.push_back(tempRect);
                    detections.push_back(structure);
                }
                else
                {
                    // check if this framenr was already stored in this vector
                    std::vector< DetectionsStruct >::iterator it = std::find_if ( detections.begin (), detections.end (),
                    boost::bind ( &DetectionsStruct::framenr, _1 ) == frameNr );
                    if(it != detections.end() )
                    {
                        //cout << "frame number " << frameNr << " was found in vecor " << endl;
                        (*it).boxes.push_back(tempRect);
                    }
                    else
                    {
                        DetectionsStruct structure;
                        structure.framenr = frameNr;
                        structure.boxes.push_back(tempRect);
                        detections.push_back(structure);
                    }
                }
            }
        }
        DetectionFile.close();
    }
    return detections;
}

vector<string> parseImages(string path)
{
    // READ IMAGE LIST
    string temp = path;
    string pathImageList = temp.append("Image_files.txt");

    ifstream ImageList;
    ImageList.open(pathImageList.c_str());

    if(!ImageList.is_open())
        cerr << "unable to open XML file list:" << pathImageList << endl;

    string line2;
    vector<string> Images;
    if (ImageList.is_open())
    {
        while ( getline (ImageList,line2) )
        {
            string temp = path;
            Images.push_back(temp.append(line2));
        }
        ImageList.close();
    }
    cout << "there are " << Images.size() << " images in this folder" << endl;
    return Images;
}

bool CalculateIntersection(Rect GT, Rect Det, float overlapTH)
{
    float iLeft = max(GT.x, Det.x);
    float iRight = min(GT.x+GT.width, Det.x+Det.width);
    float iTop = max(GT.y, Det.y);
    float iBottom = min(GT.y+GT.height, Det.y+Det.height);

    float zero = 0;
    float si = max(zero, (iRight - iLeft)) * max(zero, iBottom - iTop);
    float sa = GT.width * GT.height;
    float sb = Det.width * Det.height;

    float res1 = si/sa;
    float res2 = si/sb;

    if(res1 >= overlapTH && res2 >= overlapTH)
        return true;
    else
        return false;
}


int main(int argc, char* argv[])
{
    cout << "PR calculation of ACF detector" << endl;

    if (argc<5)
    {
        cout << "ERROR. ./PR_ACF <Enter the path to image directory> <Path to detection file> <min detection height> <outputFile>" << endl;
        return -1;
    }

    string path = argv[1];
    string detfile = argv[2];
    string minH = argv[3];
    string outputFile = argv[4];
    int minHeight= atoi(minH.c_str());

    ///////////////// parse the annotations//////////////////////////
    // define ID vector
    vector<annotationIDStruct> annotations_ID;
    annotations_ID = parseAnnotations(path,false);
    cout << "size annations ID vector = " << annotations_ID.size() << endl;
    for(int a=0;a<annotations_ID.size();a++)
    {
        cout << "annotation index = " << a << "\t id = " << annotations_ID[a].id << endl;
    }

    // SELECT ANNOTATION SET;
    vector<AnnotationStruct> annotations = annotations_ID[0].annotations;
    ////////////////// parse the detections///////////////////////////
    vector<DetectionsStruct> detections;
    detections = parseDetections(detfile);
    cout << "size detections vector = " << detections.size() << endl;

    ////////////////// parse image list //////////////////////////////
    vector<string> ImageList;
    ImageList = parseImages(path);
    cout << "size images vector = " << ImageList.size() << endl;

    ///////////////// loop over each image and find corresponding annotations and detections

    float minS = 1000;
    float maxS = -1000;
    // find max and min detection score
    for(int i=0;i<detections.size();i++)
    {
        for(int j=0;j<detections[i].boxes.size();j++)
        {
            if(detections[i].boxes[j].score > maxS)
                maxS = detections[i].boxes[j].score;
            if(detections[i].boxes[j].score < minS)
                minS = detections[i].boxes[j].score;
        }
    }
    float step = (maxS-minS)/50;

    cout << "max score = " << maxS << endl;
    cout << "min score = " << minS << endl;
    cout << "step size = " << step;


    ofstream Result;
    Result.open(outputFile.c_str());
    for(float TH=minS;TH<maxS;TH = TH+step)
    //float TH = 84;
    {
        int TP =0;
        int FP =0;
        int FN =0;
        for(int i=0;i<ImageList.size();i++)
        {
            int frameFP = 0;
            int frameTP = 0;
            int frameFN = 0;

            //cout << "image path =  " << ImageList[i] << endl;
            string substr =  ImageList[i].substr (ImageList[i].size()-9,5);
            int frame = atoi(substr.c_str());

            Mat image = imread(ImageList[i]);

            // find detections for this frame:
            vector<detectionBB> detBoxes;
            std::vector< DetectionsStruct >::iterator it = std::find_if ( detections.begin (), detections.end (),
            boost::bind ( &DetectionsStruct::framenr, _1 ) == frame );
            if(it != detections.end() )
            {
                detBoxes = (*it).boxes;
            }
            //cout << "there are " << detBoxes.size() << " detections for this frame" << endl;


            // find annotations for this frame
            vector<annotationBB> annoBoxes;
            std::vector< AnnotationStruct >::iterator it2 = std::find_if ( annotations.begin (), annotations.end (),
            boost::bind ( &AnnotationStruct::framenr, _1 ) == frame );
            if(it2 != annotations.end() )
            {
                annoBoxes = (*it2).boxes;
            }
            //cout << "there are " << annoBoxes.size() << " annotations for this frame" << endl;

            for(int anno=0;anno<annoBoxes.size();anno++)
            {
                rectangle(image,annoBoxes[anno].BB,Scalar(0,255,0),2);
                bool found = false;

                for(int det=0;det<detBoxes.size();det++)
                {
                   rectangle(image,detBoxes[det].BB,Scalar(255,0,0),2);
                   bool result = CalculateIntersection(annoBoxes[anno].BB,detBoxes[det].BB, 0.5);
                   if(result && detBoxes[det].score > TH)
                   {
                       frameTP++;
                       found = true;
                   }
               }
               // if found is not set, add an extra False Negative
                if(!found && annoBoxes[anno].BB.height>minHeight)
               {
                   frameFN++;
               }
           }

           int detAboveTH = 0;
           for(int k=0;k<detBoxes.size();k++)
           {
               if(detBoxes[k].score > TH)
               {
                   detAboveTH++;
                   rectangle(image,detBoxes[k].BB,Scalar(255,0,0),2);
               }
           }

            frameFP = detAboveTH-frameTP;
/*
            cout << "FP = " << frameFP << endl;
            cout << "TP = " << frameTP << endl;
            cout << "FN = " << frameFN << endl;
*/
//            imshow("image",image);
//            waitKey();

//            char outputPath [200];
//            sprintf(outputPath, "%s%05d%s", "Result/",i,".jpg");
//            imwrite(outputPath,image);


            TP += frameTP;
            FP += frameFP;
            FN += frameFN;
        }

        cout << "total TP =" << TP << endl;
        cout << "total FP =" << FP << endl;
        cout << "total FN =" << FN << endl;

        double recall = (double)TP / (double)(TP+FN);
        double precision = (double)TP / (double)(TP+FP);

        cout << "precision = " << precision << endl;
        cout << "recall = " << recall << endl;

        Result << TH << "\t" << TP << "\t" << FP << "\t" << FN << "\t" << precision << "\t" << recall << endl;
    }

    return 0;
}

