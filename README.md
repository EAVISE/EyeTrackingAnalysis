## 1. Description
Welcome to the project page of Eye Tracking analysis that was developed during the PhD study of Stijn De Beugher. More information on this PhD can be found in his thesis, which is accessible via this link [link](https://lirias.kuleuven.be/handle/123456789/552098).

## 2. Content
On the [wiki page](https://gitlab.com/EAVISE/EyeTrackingAnalysis/wikis/home) of this repository, you'll find all the necessary documentation about this project.

## 3. License
EAVISE, 2017

## 4. Notes
The code is still fully under construction and by no means complete. 
