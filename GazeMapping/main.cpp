#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include <iostream>
#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

#include "../ParsePupilBino/pupilparser.h"
#include "parseDetections.h"
#include "viewtime.h"
#include "loadvariables.h"

using namespace std;
using namespace cv;

//#define Show


// Validate the gaze-position by checking if the gaze cursor falls inside or outside the image
// The input parameters are:
// - int vectorIndex: index in the gaze-vector that we are interested in
// - vector<GazeInfo> gazeVector: the gaze-vector itself
// - Size S: the resolution of the image
// the output value:
// - Point variable containing the x and y position of the gaze cursor for a speci
Point validateGaze(int vectorIndex,vector<GazeInfo> gazeVector, Size S)
{
    Point P1;
    if(vectorIndex !=-1)    // plot gaze cursor
    {
        float normX = gazeVector[vectorIndex].normX;
        float normY = gazeVector[vectorIndex].normY;

        P1.x = S.width*normX;
        P1.y = S.height*(1-normY);

        if(P1.x<0)
            P1.x = 0;

        if(P1.y < 0)
            P1.y = 0;

        if(P1.x>S.width)
            P1.x = S.width;

        if(P1.y>S.height)
            P1.y = S.height;
    }
    return P1;
}

// function to resize a bounding box with a given scale factor
// to increase a detection: use a scale factor larger than 1: e.g. 1.4 increases the rectangle by 40%
// to decrease a detection: use a scale factor smaller than 1.
// input:
// - bb: Rect rectangle
// - scaleFactor: scaling factor
// output:
// - resized rectangle
Rect Resize(Rect bb, float scaleFactor)
{
    int oriWidth = bb.width;
    int oriHeight = bb.height;

    bb.width = bb.width*scaleFactor;
    bb.height = bb.height*scaleFactor;
    if(scaleFactor>1)
    {
        // shift x,y point towards upper left corner
        bb.x = bb.x - (bb.width-oriWidth)/2;
        bb.y = bb.y - (bb.height-oriHeight)/2;
    }
    else
    {
        bb.x = bb.x + (oriWidth-bb.width)/2;
        bb.y = bb.y + (oriHeight-bb.height)/2;
        // shift x,y point towards bottom right corner
    }
    return bb;
}

int main(int argc, char* argv[])
{
    if (argc != 2) {
            // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " <Path to config.ini file>" << std::endl;
            return -1;
    }

    // Read config file (a.k.a. the argument that is passed)
    string ConfigFile = argv[1];
    LoadVariables vars(ConfigFile);

    int minSeqLength = vars.getMinSeqLength();
    string outputPath = vars.getPathOuput();

    // automatically create an output directory in which we store the results
    string command_mkdir = "mkdir -p ";
    command_mkdir.append(outputPath);
    system(command_mkdir.c_str());

    // make the output files:
    // - exposure.txt: txt-file containing information about which detections are visible in which screen
    // - attention.txt txt-file containing information about the visual attention towards a specific object/item
    // - detcounter.txt: txt-file containing information about the number of detections in each frame
    string pathExposure = outputPath;
    string pathAttention = outputPath;
    string pathDetCounter = outputPath;
    pathExposure = pathExposure.append("exposure.txt");
    pathAttention = pathAttention.append("attention.txt");
    pathDetCounter = pathDetCounter.append("detcounter.txt");

    string OOI = vars.getOOI();
    ViewTime viewTimeExposure(pathExposure, OOI,minSeqLength);
    ViewTime viewTimeAttention(pathAttention, OOI,minSeqLength);
    ofstream detcounter;
    detcounter.open(pathDetCounter);

    //open gazepositions.csv file (in export dir of the binocular pupil recording)
    string CSV_Path = vars.getPathGazeData();
    vector<GazeInfo> gazeVector;
    int ret = ParseCSV(CSV_Path, gazeVector );
    cout <<" length of gazeVector = " << gazeVector.size() << endl;
    if(gazeVector.size()==0)
    {
        cout << "no gaze data was found!" << endl;
        return -1;
    }

    // parse the detections
    string DetectionsList = vars.getPathDetections();
    vector<DetectionsStruct> detections;
    ParseDetections dets(DetectionsList, detections);
    if(detections.size()==0)
    {
        cout << "no detections were found!" << endl;
        return -1;
    }
    #ifdef Debug
        cout << "Size detections = " << detections.size() << endl;
    #endif

    // loop over all frames in folder
    string path = vars.getPathFrames();
    string pathImageList = path.append("Image_files.txt");

    ifstream ImageList;
    ImageList.open(pathImageList.c_str());

    if(!ImageList.is_open())
        cerr << "unable to open image-file list:" << pathImageList << endl;

    string line2;
    vector<string> Images;
    if (ImageList.is_open())
    {
        while ( getline (ImageList,line2) )
        {
            string temp = path;
            Images.push_back(temp.append(line2));
        }
        ImageList.close();
    }
    #ifdef Debug
        cout << "there are " << Images.size() << " images in this folder" << endl;
    #endif
    
    // sort the images list:
    std::sort(Images.begin(), Images.end());
    int validDetections =0;

    // loop over all frames in this file
    for(int i=1;i<Images.size();i++)
    {        
        // extract the frame number from the image path
        string temp = Images[i];
        temp.erase (temp.size()-4,4);
        string number = temp.substr (temp.size()-5,5);
        int frame = stoi((number).c_str(),0);

        #ifdef Show
            Mat image = imread(Images[i]);
        #endif

        // find gaze-data for this frame
        cout << "find frame " << frame << "  in the gaze vector" << endl;
        int vectorIndex = findFrame(gazeVector, frame);
        Size s(1280,720);
        Point P1 = validateGaze(vectorIndex,gazeVector, s);
        #ifdef Show
            circle(image,P1,20,Scalar(0,255,255),-1);
        #endif

        // find detections for this frame
        cout << "find frame " << frame << "  in the detections vector" << endl;
        vector<detectionBB> boxes;
        std::vector< DetectionsStruct >::iterator it = std::find_if ( detections.begin (), detections.end (),
        boost::bind ( &DetectionsStruct::framenr, _1 ) == frame );
        if(it != detections.end() )
        {
            boxes = (*it).boxes;
            #ifdef Debug
                cout << "\t this frames contains: "<< boxes.size() << " detections" << endl;
            #endif
        }
        else
        {
            #ifdef Debug
                cout << "\t this frame contains no detections" << endl;
            #endif
        }

        bool attention = false;
        validDetections = 0;

        // loop over all the detections in this frame and check if there is overlap between a detection and the gaze-cursor
        // important! Consider only the detections which have a confidence larger than the TH value
        // if overlap between a detection and the gaze cursor was found in a frame, mark the attention flag
        for(int b=0;b<boxes.size();b++)
        {
            if(boxes[b].score>vars.getDetTH())
            {
                validDetections++;
                // expand rectangle with 40%
                Rect temp = Resize(boxes[b].BB,1.4);
                //rectangle(image,temp,Scalar(0,255,255),1);

                // check if gaze lies inside (extended) rectangle
                if(temp.contains(P1))
                {
                    attention = true;
                }
            }
        }

        #ifdef Debug
            cout << "\t there are " << validDetections << " in this frame" << endl;
        #endif

        detcounter << i << "\t" << validDetections << endl;

        if(validDetections>0)
        {
            viewTimeExposure.add(frame);
        }
        else
        {
            viewTimeExposure.end(0);
        }

        if(attention)
            viewTimeAttention.add(frame);
        else
            viewTimeAttention.end(0);

        #ifdef Show
            imshow("image",image);
            waitKey();
        #endif
    }
    return 0;
}
