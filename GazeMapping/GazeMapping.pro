TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

LIBS += `pkg-config opencv --libs`		# so files
LIBS += -L/usr/lib/x86_64-linux-gnu/ -lxml2

INCLUDEPATH += -I "/usr/local/include/opencv2" 	# headers
INCLUDEPATH += -I "/usr/include/libxml2/"

TARGET = GazeMapping

SOURCES += main.cpp \   
    viewtime.cpp \
    loadvariables.cpp \
    ../ParsePupilBino/pupilparser.cpp \
    parseDetections.cpp


HEADERS += \   
    viewtime.h \
    loadvariables.h \
    ../ParsePupilBino/pupilparser.h \
    parseDetections.h
