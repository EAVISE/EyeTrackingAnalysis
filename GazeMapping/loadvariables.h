#ifndef LOADVARIABLES_H
#define LOADVARIABLES_H

#include <iostream>
#include <QSettings>

using namespace std;

class LoadVariables
{
public:
    LoadVariables(string path);
    string getPathDetections();
    string getPathGazeData();
    string getPathFrames();
    string getPathOuput();
    string getOOI();
    int getMinSeqLength();
    float getDetTH();

private:
    string pathDetections_;
    string pathGazeData_;
    string pathFrames_;
    string pathOuput_;
    string OOI_;
    int minSeqLength_;
    float detTH_;
};

#endif // LOADVARIABLES_H
