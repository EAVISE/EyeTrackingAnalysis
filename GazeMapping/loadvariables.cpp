#include "loadvariables.h"

using namespace std;

LoadVariables::LoadVariables(string path)
{    
    QSettings settings(QString::fromStdString(path), QSettings::IniFormat);

    QString pathDetections = settings.value("PathDetections", "default value if unset").toString();
    QString pathGazeData = settings.value("PathGazeData", "default value if unset").toString();
    QString pathFrames = settings.value("PathFrames", "default value if unset").toString();
    QString pathOuput = settings.value("PathOutput", "default value if unset").toString();
    QString minSeqLength = settings.value("minSeqLength", "default value if unset").toString();
    QString OOI = settings.value("OOI", "default value if unset").toString();
    QString detTH = settings.value("DetTH", "default value if unset").toString();

    pathDetections_ = pathDetections.toStdString();
    pathGazeData_ = pathGazeData.toStdString();
    pathFrames_ = pathFrames.toStdString();
    pathOuput_ = pathOuput.toStdString();
    minSeqLength_ = minSeqLength.toInt();
    OOI_ = OOI.toStdString();
    detTH_ = detTH.toFloat();
   }

string LoadVariables::getPathDetections()
{
    return pathDetections_;
}

string LoadVariables::getPathGazeData()
{
    return pathGazeData_;
}

string LoadVariables::getPathFrames()
{
    return pathFrames_;
}

string LoadVariables::getPathOuput()
{
    return pathOuput_;
}

int LoadVariables::getMinSeqLength()
{
    return minSeqLength_;
}

string LoadVariables::getOOI()
{
    return OOI_;
}

float LoadVariables::getDetTH()
{
    return detTH_;
}
