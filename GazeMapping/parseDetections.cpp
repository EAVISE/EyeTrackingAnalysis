#include "parseDetections.h"
#include <iostream>

using namespace std;
using namespace cv;

ParseDetections::ParseDetections(string path, vector<DetectionsStruct> &detections)
{
    ifstream DetectionFile;
    DetectionFile.open(path.c_str());   

    string line;
    int frameNr;
    vector<string> tokens;

    if (DetectionFile.is_open())
    {
        while ( getline (DetectionFile,line) )
        {
            tokens.clear();
            cout << "line = " << line << endl;
            std::istringstream ss(line);
            std::string token;

            while(std::getline(ss, token, ',')) {
                tokens.push_back(token);
            }

            if(tokens.size()==6)
            {
                detectionBB tempRect;

                frameNr = atoi(tokens[0].c_str());
                tempRect.BB.x = atof(tokens[1].c_str());
                tempRect.BB.y = atof(tokens[2].c_str());
                tempRect.BB.width = atof(tokens[3].c_str());
                tempRect.BB.height = atof(tokens[4].c_str());
                tempRect.score = atof(tokens[5].c_str());

                // MAKE DETECTIONS VECTOR
                if(detections.size()==0)
                {
                    DetectionsStruct structure;
                    structure.framenr = frameNr;
                    structure.boxes.push_back(tempRect);
                    detections.push_back(structure);
                }
                else
                {
                    // check if this framenr was already stored in this vector
                    std::vector< DetectionsStruct >::iterator it = std::find_if ( detections.begin (), detections.end (),
                    boost::bind ( &DetectionsStruct::framenr, _1 ) == frameNr );
                    if(it != detections.end() )
                    {
                        cout << "frame number " << frameNr << " was found in vecor " << endl;
                        (*it).boxes.push_back(tempRect);
                    }
                    else
                    {
                        DetectionsStruct structure;
                        structure.framenr = frameNr;
                        structure.boxes.push_back(tempRect);
                        detections.push_back(structure);
                    }
                }
            }
        }
        DetectionFile.close();
    }
}
