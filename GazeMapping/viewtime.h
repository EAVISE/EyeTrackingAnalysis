#ifndef VIEWTIME_H
#define VIEWTIME_H

#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include "opencv2/opencv.hpp"
#include <boost/lexical_cast.hpp>

using namespace std;

class ViewTime
{
public:
    ViewTime(string outputfile, string classtype, int minSeqLength);
    ~ViewTime(void);
    void add(int frame);
    void end(int counter);

    int seqLength_;
    int first_;
    int last_;
    int minSeqLength_;

    string outputfile_;
    string classtype_;

    ofstream result_;
};

#endif // VIEWTIME_H
