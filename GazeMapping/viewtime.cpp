#include "viewtime.h"

ViewTime::ViewTime(string outputfile, string classtype, int minSeqLength)
{
    // constructor
    outputfile_ = outputfile;
    classtype_ = classtype;
    cout << "classtype = " << classtype << endl;
    result_.open(outputfile_);

    seqLength_ = 0;
    last_ = 0;
    first_ = 0;
    minSeqLength_ = minSeqLength;
}

void ViewTime::add(int frame)
{
    if(seqLength_>=1)            // sequence of detections
    {
        last_ = frame;
        seqLength_++;
    }
    else
    {
        last_ = frame;
        first_ = frame;
        seqLength_ = 1;              // start of new sequence
    }
}

void ViewTime::end(int counter)
{
    if(seqLength_>=minSeqLength_)
    {
        float avgCountPerFrame = (float)counter/(last_-first_+1);
        result_ << classtype_ << "\t" << first_ << "\t" << last_ << "\t" << last_-first_+1 << "\t" << avgCountPerFrame << "\n";
        first_ = 0;              // clear all variables
        last_ = 0;               // clear all variables
        seqLength_ = 0;          // clear all variables

    }
    else    // sequence is not long enough
    {
        cout <<" sequence is not long enough!" << endl;
        first_ = 0;             // clear all variables
        last_ = 0;               // clear all variables
        seqLength_ = 0;          // clear all variables
    }
}

ViewTime::~ViewTime(void) {
    result_.close();
}

