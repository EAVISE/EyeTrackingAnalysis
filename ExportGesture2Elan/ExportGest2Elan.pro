QT       += core

QT       -= gui

TARGET = ExportGest2Elan
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += `pkg-config opencv --libs`
INCLUDEPATH += -I "/usr/local/include/opencv2"


SOURCES += main.cpp \
    makeviewtime.cpp

HEADERS += \
    makeviewtime.h
