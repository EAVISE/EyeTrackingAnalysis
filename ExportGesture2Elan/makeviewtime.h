#ifndef MAKEVIEWTIME_H
#define MAKEVIEWTIME_H

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>
#include <fstream>
using namespace std;

// structs
struct GestureSpace
{
    int spaceID;
    int frameNr;
};

struct Detection{                          // Detection structure
    int item;
    int start;
    int end;
    int length;
};

vector<Detection> MakeViewTime(vector<int> spacevector);


#endif // MAKEVIEWTIME_H
