#include <iostream>

#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>
#include <fstream>
#include "makeviewtime.h"

//#define ViewTimePath "/home/sdb/Dropbox/CodeDoctoraat/GestureDetection/Programma/Output/Sigma_9.txt"
//#define GestureSpaces "/home/sdb/Dropbox/CodeDoctoraat/Results/Output_SaGA/GestureSpace.txt"
//#define GestureDirection "/home/sdb/Dropbox/CodeDoctoraat/Results/Output_NeuroPeirce/GestureDirection.txt"
//#define VideoPath "/media/sdb/DATAPART2/NeuroPeirce/Block A_1.mp4"
//#define VideoPath "/media/sdb/DATAPART2/SaGA/SaGA-V07/V07.mp4"

//#define ViewTimePath "/media/sdb/DATAPART2/recordings/2014_08_28/002/Sigma_9.txt"
//#define GestureSpaces "/media/sdb/DATAPART2/recordings/2014_08_28/002/GestureSpace.txt"
//#define GestureDirection "/media/sdb/DATAPART2/recordings/2014_08_28/002/GestureDirection.txt"
//#define VideoPath "/media/sdb/DATAPART2/recordings/2014_08_28/002/after.avi"

#define ViewTimePath "/home/sdb/EyeTrackingAnalysis/GestureAnalysis/GesturePhases.txt"
#define GestureSpaces "/home/sdb/EyeTrackingAnalysis/GestureAnalysis/GestureSpace.txt"
#define GestureDirection "/home/sdb/EyeTrackingAnalysis/GestureAnalysis/GestureDirection.txt"
#define VideoPath "/media/sdb/DATAPART2/SaGA/SaGA-V07/V07.mp4"

#define FPS 25
#define gapTH 5
#define minTime 7
#define OffSet 0 // express in frames

using namespace std;
using namespace cv;

//-------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------//
struct TimeSlot {                           // timeslot structure
    int id;
    double value;
    int framenr;
};
//-----------------------------------------------------------------------------------


//-------------------------------------------------------------------------------//
struct Annotation {                           // timeslot structure
    string annotationClass;
    int StartTimeSlot;
    int EndTimeSlot;
};
//-----------------------------------------------------------------------------------

// find smallest start value in vector of detections
int findMin(vector<Detection> DetVector)
{
    int min = 99999;
    int minIndex = 99999;
    for(int i=0;i<DetVector.size();i++)
    {
        if(DetVector[i].start < min)
        {
            min = DetVector[i].start;
            minIndex = i;
        }
    }
    return minIndex;
}
//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// write ELAN Header
void WriteHeader(ofstream& File)
{
    File << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
    File << "<ANNOTATION_DOCUMENT AUTHOR=\"\" DATE=\"2015-05-20T14:50:36+01:00\" FORMAT=\"2.8\" VERSION=\"2.8\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.mpi.nl/tools/elan/EAFv2.8.xsd\">" << endl;
    File << "\t<HEADER MEDIA_FILE=\"\" TIME_UNITS=\"milliseconds\">" << endl;
    File << "\t\t<MEDIA_DESCRIPTOR MEDIA_URL=\"file://" << VideoPath << "\" MIME_TYPE=\"video/*\" RELATIVE_MEDIA_URL=\"./Block A_1.mp4\"/>" << endl;
    File << "\t\t<PROPERTY NAME=\"URN\">urn:nl-mpi-tools-elan-eaf:ebe089d9-07d9-46a7-bf4c-08745ea6652f</PROPERTY>" << endl;
    File << "\t\t<PROPERTY NAME=\"lastUsedAnnotationId\">4</PROPERTY>" << endl;
    File << "\t</HEADER>" << endl;
}
//-----------------------------------------------------------------------------------
// write ELAN TIMESLOTS
void WriteTimeSlots(ofstream& File, vector<TimeSlot> TimeSlots)
{
    File << "\t<TIME_ORDER>" << endl;

    for(int i=0;i<TimeSlots.size();i++)
    {
         int value = (int)TimeSlots[i].value;
         File << "\t\t<TIME_SLOT TIME_SLOT_ID=\"ts" << TimeSlots[i].id+1 <<"\" TIME_VALUE=\"" << value << "\"/>" << endl;
    }

    File << "\t</TIME_ORDER>" << endl;
}
//-----------------------------------------------------------------------------------
// write ELAN TIER
void WriteTiers(ofstream& File, vector<Annotation> Annotations, string tierName)
{
    File << "\t<TIER LINGUISTIC_TYPE_REF=\"default-lt\" TIER_ID=\"default\"/>" << endl;
    for(int i=0;i<Annotations.size();i++)
    {
        File << "\t<TIER ANNOTATOR=\"Stijn\" LINGUISTIC_TYPE_REF=\"default-lt\" PARTICIPANT=\"Raphael\" TIER_ID=\"" << tierName << "\">" << endl;
        File <<"\t\t<ANNOTATION>"<<endl;
        File << "\t\t\t<ALIGNABLE_ANNOTATION ANNOTATION_ID=\" a" << i<< "\" TIME_SLOT_REF1=\"ts" << Annotations[i].StartTimeSlot+1 << "\" TIME_SLOT_REF2=\"ts" << Annotations[i].EndTimeSlot+1 << "\">" << endl;
        File <<"\t\t\t\t<ANNOTATION_VALUE>" << Annotations[i].annotationClass << "</ANNOTATION_VALUE>"<<endl;
        //File <<"\t\t\t\t<ANNOTATION_VALUE>" << "0" << "</ANNOTATION_VALUE>"<<endl;
        File << "\t\t\t</ALIGNABLE_ANNOTATION>" << endl;
        File << "\t\t</ANNOTATION>" << endl;
        File << "\t</TIER>" <<endl;
    }
}
//-----------------------------------------------------------------------------------
//Write ELAN FOOTER
void WriteFooter(ofstream& File)
{
    File << "\t<LINGUISTIC_TYPE GRAPHIC_REFERENCES=\"false\" LINGUISTIC_TYPE_ID=\"default-lt\" TIME_ALIGNABLE=\"true\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Time subdivision of parent annotation's time interval, no time gaps allowed within this interval\" STEREOTYPE=\"Time_Subdivision\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Symbolic subdivision of a parent annotation. Annotations refering to the same parent are ordered\" STEREOTYPE=\"Symbolic_Subdivision\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"1-1 association with a parent annotation\" STEREOTYPE=\"Symbolic_Association\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Time alignable annotations within the parent annotation's time interval, gaps are allowed\" STEREOTYPE=\"Included_In\"/>" << endl;
    File << "</ANNOTATION_DOCUMENT>" << endl;
}

//-----------------------------------------------------------------------------------
// Find a timeslot that belongs to a certain frame number
// in no valid timeslot was found, return -1
int FindTimeSlot(vector<TimeSlot> TimeVector, int frame)
{
    for(int i=0;i<TimeVector.size();i++)
    {
        if(frame==TimeVector[i].framenr)
            return i;
    }
    return -1;
}

// Find the gesture spaces that belong to a certain gesture phase
vector<Detection>findSpacesInGestures(vector<Detection> sortVector, vector<Detection>viewTime)
{
/*    
    for(int i=0;i<viewTime.size();i++)
    {
        cout << "VT "<< i << "viewtime start = " << viewTime[i].start << " - viewtime end = " << viewTime[i].end << endl;
    }
*/
    vector<Detection>finalGestureSpace;
    Detection detectionItem;
    // find all gesture spaces that belong to the gesture phases
    for(int i=0;i<sortVector.size();i++)
    {
        cout << " --> final vector" <<  sortVector[i].item << ":\t" << sortVector[i].start << "-" << sortVector[i].end << "\t" << sortVector[i].length << endl;

        // find start
        int target = sortVector[i].start;
        int indexStart,indexEnd;
        for(int j=0;j<viewTime.size();j++)
        {
            if(viewTime[j].start <= target && target <viewTime[j].end)
            {
                indexStart = j;
            }
        }
        cout << "\t viewtime index start = " << indexStart << endl;

        // find end
        target = sortVector[i].end;
        for(int j=0;j<viewTime.size();j++)
        {
            if(viewTime[j].start <= target && target <viewTime[j].end)
            {
                indexEnd = j;
            }
        }

        cout << "\t viewtime index end = " << indexEnd << endl;

        if(indexStart ==0 && indexEnd ==0)
            cout << "could not found correct indexes... " << endl;

        // show individual annotation values:
        for(int j=indexStart;j<=indexEnd;j++)
        {
            cout << "\tVT-start = " << viewTime[j].start  << " | Sort-start = " << sortVector[i].start << endl;
            if(viewTime[j].start < sortVector[i].start)
            {
                detectionItem.start = sortVector[i].start;
                cout << "\t\t change start value" << endl;
            }
            else
            {
                detectionItem.start =viewTime[j].start;
            }

            if(viewTime[j].end > sortVector[i].end)
            {
                detectionItem.end = sortVector[i].end;
            }
            else
            {
                detectionItem.end =viewTime[j].end;
            }

            cout << "\t final Start = " << detectionItem.start << endl;
            cout << "\t final end = " << detectionItem.end << endl;

            detectionItem.item =viewTime[j].item;
            detectionItem.length = detectionItem.end - detectionItem.start;
            finalGestureSpace.push_back(detectionItem);
        }
    }
    return finalGestureSpace;
}

void MakeTimeSlots(vector<Detection>finalGestureSpace,vector<TimeSlot>& TimeSlots, double frametime)
{
    // get ID of last item in TimeSlot vector
    int id = TimeSlots[TimeSlots.size()-1].id+1;
    // timeslots for left gesture space
    for(int i=0;i<finalGestureSpace.size();i++)
    {
        //cout << "gesture space: " << i << ": " << "start = " << finalGestureSpace[i].start << " | end = " << finalGestureSpace[i].end << endl;
        int index = FindTimeSlot(TimeSlots,finalGestureSpace[i].start);
        if(index==-1)
        {
            // add new entry
            TimeSlot TimeEntry;
            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(finalGestureSpace[i].start);
            TimeEntry.framenr = finalGestureSpace[i].start;
            TimeSlots.push_back(TimeEntry);
            id++;
        }

        index = FindTimeSlot(TimeSlots,finalGestureSpace[i].end);
        if(index==-1)
        {
            // add new entry
            TimeSlot TimeEntry;
            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(finalGestureSpace[i].end);
            TimeEntry.framenr = finalGestureSpace[i].end;
            TimeSlots.push_back(TimeEntry);
            id++;
        }
    }
}

string convertDirectionID(int id)
{
    string type;
    if(id==1)
        type="L";
    else if(id==2)
        type="R";
    else if(id==3)
        type="U";
    else if(id==4)
        type="D";
    else if(id==5)
        type = "N";
    else if(id==5)
        type = "nan";
    return type;
}

string convertSpaceID(int id)
{
    string type;
    if(id==1)
        type="CC";
    else if(id==2)
        type="C";
    else if(id==10 || id==3) // label 3 was used in experiments NP/SaGA
        type="P";
    else if(id==11)
       	type="P_UR";
	else if(id==12)
        type="P_R";
	else if(id==13)
       	type="P_LR";
    else if(id==14)
        type="P_Lo";
	else if(id==15)
       	type="P_LL";
	else if(id==16)
        type="P_Le";
	else if(id==17)
       	type="P_UL";
	else if(id==18)
        type="P_U";
    else if(id==20 || id==4) // label 4 was used in experiments NP/SaGA
       	type="EP";
	else if(id==21)
        type="EP_UR";
	else if(id==22)
       	type="EP_R";
    else if(id==23)
        type="EP_LR";
    else if(id==24)
       	type="EP_Lo";
    else if(id==25)
        type="EP_LL";
	else if(id==26)
       	type="EP_Le";
	else if(id==27)
        type="EP_UL";
	else if(id==28)
       	type="EP_I";
	else
        type = "nan";

    return type;
}

int main()
{  
    cout << "Automatic ELAN-export algorithm V4 (gesture space and gesture direction included)" << endl;
    for(int TH=9;TH<=9;TH++)
    {
        char Path[200];        
        sprintf(Path,"%s",ViewTimePath);
        //sprintf(Path, "%s%d%s","/home/sdb/Dropbox/CodeDoctoraat/Results/Output_NeuroPeirce/Sigma_", TH, ".txt");
        //sprintf(Path, "%s%d%s","/home/sdb/Dropbox/CodeDoctoraat/Results/Output_SaGA/Sigma_", TH, ".txt");

        cout << "Path = " << Path << endl;

        char PathSpace[200];
        sprintf(PathSpace,"%s",GestureSpaces);
        cout << "PathSpace = " << PathSpace << endl;

        char PathDirection[200];
        sprintf(PathDirection,"%s",GestureDirection);
        cout << "PathDirection = " << PathDirection << endl;
        //-----------------------------------------------------------------------------------------------//
        //1) read GestureSpace.txt + store gesture spaces in left and right vector
        string FileNameSpace = PathSpace;
        string LineSpace;
        ifstream ResultSpace;
        ResultSpace.open(FileNameSpace.c_str());
        int right,left;
        vector<int> rightSpace;
        vector<int> leftSpace;

        if(ResultSpace.is_open())
            cout << "file is open" << endl;
        else
        {
            cout << "file is not open" << endl;
            return -1;
        }

        while(!ResultSpace.eof())
        {
            getline(ResultSpace, LineSpace);
            sscanf(LineSpace.c_str(),"%d%d",&right, &left);
            rightSpace.push_back(right);
            leftSpace.push_back(left);
        }
        cout << "size left space = " << leftSpace.size() << endl;
        cout << "size right space = " << rightSpace.size() << endl;
        //-----------------------------------------------------------------------------------------------//
        // 2) convert gesturespace vector to viewtime files for both left and right hand
        vector<Detection> ViewTime_leftSpace;
        vector<Detection> ViewTime_rightSpace;
        cout <<" viewtime left space = " << endl;
        ViewTime_leftSpace = MakeViewTime(leftSpace);
        cout <<" viewtime right space = " << endl;
        ViewTime_rightSpace = MakeViewTime(rightSpace);
        //-----------------------------------------------------------------------------------------------//
        // 2b read GestureDirection.txt + store gesture directions in left and right vector
        string FileNameDir = PathDirection;
        string LineDir;
        ifstream ResultDir;
        ResultDir.open(FileNameDir.c_str());
        vector<int> rightDir;
        vector<int> leftDir;

        if(ResultDir.is_open())
            cout << "file gesture direction is open" << endl;
        else
        {
            cout << "file gesture direction is not open" << endl;
            return -1;
        }
        while(!ResultDir.eof())
        {
            getline(ResultDir, LineDir);
            sscanf(LineDir.c_str(),"%d%d",&right, &left);
            rightDir.push_back(right);
            leftDir.push_back(left);
        }
        cout << "size left Dir = " << leftDir.size() << endl;
        cout << "size right Dir = " << rightDir.size() << endl;

        //-----------------------------------------------------------------------------------------------//
        // 2c) convert gesturedirection vector to viewtime files for both left and right hand
        vector<Detection> ViewTime_leftDir;
        vector<Detection> ViewTime_rightDir;
        cout <<" viewtime left direction = " << endl;
        ViewTime_leftDir = MakeViewTime(leftDir);
        cout <<" viewtime right direction = " << endl;
        ViewTime_rightDir = MakeViewTime(rightDir);

        vector<Detection>tempLeft;
        for(int i=0;i<ViewTime_leftDir.size();i++)
        {
            if(ViewTime_leftDir[i].length<3)
            {
                tempLeft[tempLeft.size()-1].end = ViewTime_leftDir[i].end;
            }
            else
            {
                tempLeft.push_back(ViewTime_leftDir[i]);
            }
        }
/*
        for(int i=0;i<tempLeft.size();i++)
        {
            cout << tempLeft[i].start << " | " << tempLeft[i].end << " | size = " << tempLeft[i].end - tempLeft[i].start << " | class = " << tempLeft[i].item << endl;
        }
*/

        vector<Detection>tempRight;
        for(int i=0;i<ViewTime_rightDir.size();i++)
        {
            if(ViewTime_rightDir[i].length<3)
            {
                tempRight[tempRight.size()-1].end = ViewTime_rightDir[i].end;
            }
            else
            {
                tempRight.push_back(ViewTime_rightDir[i]);
            }
        }

        ViewTime_leftDir = tempLeft;
        ViewTime_rightDir = tempRight;

        //-----------------------------------------------------------------------------------------------//
        // 3) read viewtime file + store in vector
        //string FileName = ViewTimePath;
        string FileName = Path;
        string Line;
        int item,start,end,length;
        ifstream Result (FileName.c_str());
        vector<Detection> DetVector;
        int lastframenr = 0;

        if(!Result.is_open())
        {
            cout << "could not open viewtime file gesturephases: " << FileName << endl;
            return -1;
        }

        while(!Result.eof())
        {
            Detection det;
            item = 0;
            start = 0;
            end = 0;
            length = 0;
            getline(Result, Line);
            sscanf(Line.c_str(),"%d%d%d%d",&item,&start,&end,&length);

            det.item = item;
            det.start = start+OffSet;
            det.end = end+OffSet;
            det.length = length;

            if(end+OffSet>lastframenr)
                lastframenr = det.end;

            if(start>0&&end>0)
            {
                DetVector.push_back(det);                
            }
        }
        cout << "last frame nr = " << lastframenr << endl;
        //-----------------------------------------------------------------------------------------------//
        // 4) merge left and right detections and store them into Detvector2
        vector<int>frames;

        for(int i=0;i<=lastframenr;i++)
        {
            frames.push_back(0);
        }

        for(int i=0;i<DetVector.size();i++)
        {
            int first = DetVector[i].start;
            for(int j=first;j<(DetVector[i].end);j++)
            {
                frames[j] = 1;
            }
        }

        vector<Detection> DetVector2;
        bool lock = false;
        Detection tempdet;
        for(int i=0;i<frames.size();i++)
        {
            if(!lock && frames[i]==1)
            {
                lock=true;
                tempdet.start = i;
            }

            if(lock && frames[i]==0)
            {
                lock = false;
                tempdet.end = i;
                tempdet.length = tempdet.end - tempdet.start;
                DetVector2.push_back(tempdet);
            }
        }

        cout << "size DetVector2 = " << DetVector2.size() <<endl;

        //-----------------------------------------------------------------------------------------------//
        // 5) sort vector
        DetVector = DetVector2;
        vector<Detection> SortVector;
        while(DetVector.size()>0)
        {
            int minIndex = findMin(DetVector);

            if(SortVector.size()>0)
            {
                if( (SortVector[SortVector.size()-1].item  == DetVector[minIndex].item ) && (DetVector[minIndex].start < SortVector[SortVector.size()-1].end))
                {
                    SortVector[SortVector.size()-1].end = DetVector[minIndex].end;
                }
                else
                {
                    SortVector.push_back(DetVector[minIndex]);
                }
            }
            else
            {
                SortVector.push_back(DetVector[minIndex]);
            }

            DetVector.erase(DetVector.begin()+minIndex);
        }

		for(int i=0;i<SortVector.size();i++)
		{
			cout << " ------> S = " << SortVector[i].start << " | E = " << SortVector[i].end << endl;
		}
        cout << "Size Sortvector = " << SortVector.size() << endl;
        //-----------------------------------------------------------------------------------------------//
        //6) cluster neighbouring & overlapping detections + remove detections that are too short (<MinTime)
         vector<Detection> SortVector3;
         SortVector3.clear();
        for(int i=0;i<SortVector.size();i++)
        {
            //cout << "start = " << SortVector[i].start << " | end = " << SortVector[i].end << " | length = " << SortVector[i].end-SortVector[i].start << endl;
            //cout << "\ti = " << i << "| sortvector size = " << SortVector.size() << endl;
            if(i<SortVector.size())
            {
                if( abs(SortVector[i].end - SortVector[i+1].start)<gapTH)
                {
                    cout << "\tstart gap between current and previous = " << SortVector[i].start - SortVector[i+1].start << endl;
                    cout << "\tend gap between current and previous = " << SortVector[i].end - SortVector[i+1].end << endl;
                    int newStart = min( SortVector[i].start, SortVector[i+1].start);
                    int newEnd = max( SortVector[i].end, SortVector[i+1].end);
                    cout << "\t-->newstart = " << newStart << " new end = " << newEnd << endl;
                    Detection Temp;
                    Temp.start = newStart;
                    Temp.end = newEnd;
                    Temp.length = newEnd-newStart;
                    if(Temp.length>minTime)
                    {
                        SortVector3.push_back(Temp);
                    }
                    i++;
                }
                else
                {
                    if(SortVector[i].length>minTime)
                    {
                        SortVector3.push_back(SortVector[i]);
                    }
                }
            }
        }

        cout << "size sortvector 3 = " << SortVector3.size() << endl;

        //-----------------------------------------------------------------------------------------------//
        // 7) find for each gesturephase, the corresponding gesture spaces for both left and right hand
        vector<Detection>finalGestureSpaceLeft = findSpacesInGestures(SortVector3,ViewTime_leftSpace);
        vector<Detection>finalGestureSpaceRight = findSpacesInGestures(SortVector3,ViewTime_rightSpace);
        cout << "size finalGestureSpaceLeft = " << finalGestureSpaceLeft.size() << endl;
        cout << "size finalGestureSpaceRight = " << finalGestureSpaceRight.size() << endl;
        //-----------------------------------------------------------------------------------------------//
        // 7b) find for each gesturephase, the corresponding gesture directions for both left and right hand
        vector<Detection>finalGestureDirectionLeft = findSpacesInGestures(SortVector3,ViewTime_leftDir);
        vector<Detection>finalGestureDirectionRight = findSpacesInGestures(SortVector3,ViewTime_rightDir);
        cout << "size finalGestureDirectionLeft = " << finalGestureDirectionLeft.size() << endl;
        cout << "size finalGestureDirectionRight = " << finalGestureDirectionRight.size() << endl;
        //-----------------------------------------------------------------------------------------------//
        // 8) convert start & end time of the gesturephases to time slots
        double frametime = ((double)1/FPS)*1000;
        vector<TimeSlot> TimeSlots;
        vector<Annotation> Annotations;
        cout << "frametime = " << frametime << endl;
        int id = 0;
        for(int i=0;i<SortVector3.size();i++)
        {
            TimeSlot TimeEntry;
            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(SortVector3[i].start);//+83;
            TimeEntry.framenr = SortVector3[i].start;
            TimeSlots.push_back(TimeEntry);
            id++;

            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(SortVector3[i].end);//+83;
            TimeEntry.framenr = SortVector3[i].end;
            TimeSlots.push_back(TimeEntry);
            id++;
            cout << "TimeSlot Phase: start frame = " << SortVector3[i].start << " corresponding time = " << TimeEntry.value  << " fps = " << 1000/frametime<< endl;
        }
        //-----------------------------------------------------------------------------------------------//
        // 9) make timeslots of gestureSpaces
        MakeTimeSlots(finalGestureSpaceLeft,TimeSlots, frametime);
        MakeTimeSlots(finalGestureSpaceRight,TimeSlots,frametime);
        //-----------------------------------------------------------------------------------------------//
        // 9b) make timeslots of gestureDirection
        MakeTimeSlots(finalGestureDirectionLeft,TimeSlots,frametime);
        MakeTimeSlots(finalGestureDirectionRight,TimeSlots,frametime);
        //-----------------------------------------------------------------------------------------------//
#ifdef Debug
        // 9b) give overview of timeslots:
        for(int i=0;i<TimeSlots.size();i++)
        {
            cout << "Timeslot gesture space" << TimeSlots[i].id << "| " << TimeSlots[i].value << " | " << TimeSlots[i].framenr << endl;
        }
#endif

        //-----------------------------------------------------------------------------------------------//
        // 10) make annotations vector of the gesture phases
        for(int i=0;i<SortVector3.size();i++)
        {
            int index1 = FindTimeSlot(TimeSlots,SortVector3[i].start);
            int index2 = FindTimeSlot(TimeSlots,SortVector3[i].end);
            Annotation AnnotationItem;

            ostringstream convert;   // stream used for the conversion
            convert << i;
            AnnotationItem.annotationClass = convert.str();
            AnnotationItem.StartTimeSlot = index1;
            AnnotationItem.EndTimeSlot = index2;
            Annotations.push_back(AnnotationItem);
            cout << "annotation phase " << i << "slot start = " << index1 << " | slot end = " << index2 << endl;
        }

        //-----------------------------------------------------------------------------------------------//
        // 11) make annotations vector of gesturespaces of left and right hand
        vector<Annotation>AnnotationsSpaceLeft;
        for(int i=0;i<finalGestureSpaceLeft.size();i++)
        {
            int index1 = FindTimeSlot(TimeSlots,finalGestureSpaceLeft[i].start);
            int index2 = FindTimeSlot(TimeSlots,finalGestureSpaceLeft[i].end);
            Annotation AnnotationItem;
            AnnotationItem.StartTimeSlot = index1;
            AnnotationItem.EndTimeSlot = index2;
            AnnotationItem.annotationClass = convertSpaceID(finalGestureSpaceLeft[i].item);
            AnnotationsSpaceLeft.push_back(AnnotationItem);
        }

        vector<Annotation>AnnotationsSpaceRight;
        for(int i=0;i<finalGestureSpaceRight.size();i++)
        {
            int index1 = FindTimeSlot(TimeSlots,finalGestureSpaceRight[i].start);
            int index2 = FindTimeSlot(TimeSlots,finalGestureSpaceRight[i].end);
            Annotation AnnotationItem;
            AnnotationItem.StartTimeSlot = index1;
            AnnotationItem.EndTimeSlot = index2;
            AnnotationItem.annotationClass = convertSpaceID(finalGestureSpaceRight[i].item);
            AnnotationsSpaceRight.push_back(AnnotationItem);
        }
        //-----------------------------------------------------------------------------------------------//
        // 11b) make annotations vector of gestureDetections of left and right hand
        vector<Annotation>AnnotationsDirectionLeft;
        for(int i=0;i<finalGestureDirectionLeft.size();i++)
        {
            int index1 = FindTimeSlot(TimeSlots,finalGestureDirectionLeft[i].start);
            int index2 = FindTimeSlot(TimeSlots,finalGestureDirectionLeft[i].end);
            Annotation AnnotationItem;
            AnnotationItem.StartTimeSlot = index1;
            AnnotationItem.EndTimeSlot = index2;
            AnnotationItem.annotationClass = convertDirectionID(finalGestureDirectionLeft[i].item);
            AnnotationsDirectionLeft.push_back(AnnotationItem);
        }

        vector<Annotation>AnnotationsDirectionRight;
        for(int i=0;i<finalGestureDirectionRight.size();i++)
        {
            int index1 = FindTimeSlot(TimeSlots,finalGestureDirectionRight[i].start);
            int index2 = FindTimeSlot(TimeSlots,finalGestureDirectionRight[i].end);
            Annotation AnnotationItem;
            AnnotationItem.StartTimeSlot = index1;
            AnnotationItem.EndTimeSlot = index2;
            AnnotationItem.annotationClass = convertDirectionID(finalGestureDirectionRight[i].item);
            AnnotationsDirectionRight.push_back(AnnotationItem);
        }

        //-----------------------------------------------------------------------------------------------//
        // 12) Write to ELAN file:
        char XML_File[200];
        sprintf(XML_File,"%s%d%s","Result",TH,".eaf");
        ofstream XML;
        XML.open(XML_File);

        WriteHeader(XML);
        WriteTimeSlots(XML,TimeSlots);
        WriteTiers(XML,Annotations, "GesturePhase");
        WriteTiers(XML,AnnotationsSpaceLeft, "GesturSpaceLeft");
        WriteTiers(XML,AnnotationsSpaceRight, "GestureSpaceRight");
        WriteTiers(XML,AnnotationsDirectionLeft, "GestureDirLeft");
        WriteTiers(XML,AnnotationsDirectionRight, "GestureDirRight");
        WriteFooter(XML);        
        XML.close();

        //-----------------------------------------------------------------------------------------------//
        // 13) write output to .txt for jaccard calculation
        //ofstream DetectionFile;        
        //char DetPath[200];
        //sprintf(DetPath,"%s%d%s","Detections",TH,".txt");
        //DetectionFile.open(DetPath);

        ofstream GesturePhases;
        char PhasePath[200];
        sprintf(PhasePath,"%s%d%s","Phases",TH,".txt");
        GesturePhases.open(PhasePath);

        for(int i=0;i<SortVector3.size();i++)
        {
            cout << SortVector3[i].item << ":\t" << SortVector3[i].start << "-" << SortVector3[i].end << "\t" << SortVector3[i].length << endl;
            //DetectionFile << "1\tRapaheal\t" << SortVector3[i].start*frametime << "\t" << SortVector3[i].end*frametime << "\t" << SortVector3[i].length*frametime << "\t0" << endl;
            GesturePhases << SortVector3[i].start << "\t" << SortVector3[i].end << "\t" << SortVector3[i].length << endl;
        }
        //DetectionFile.close();
        GesturePhases.close();
    }

    return 0;
}

