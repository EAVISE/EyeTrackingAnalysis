#include "makeviewtime.h"

using namespace std;

vector<Detection> MakeViewTime(vector<int> spacevector)
{
    vector<Detection>viewtimeVector;
    Detection viewItem;
    viewItem.start = 0;
    viewItem.item = spacevector[0];
    bool set = false;

    // start @ index 1
    for(int i=1;i<spacevector.size();i++)
    {
        if(spacevector[i] != viewItem.item)
        {
            viewItem.end = i;
            viewItem.length = viewItem.end - viewItem.start;
            viewtimeVector.push_back(viewItem);
            set = true;

            viewItem.start = i;
            viewItem.item = spacevector[i];
        }
        else
        {
            set = false;
        }
    }

    if(set==false)
    {
        viewItem.end = spacevector.size()-1;
        viewItem.length = viewItem.end - viewItem.start;
        viewtimeVector.push_back(viewItem);
    }
/*
    for(int i=0;i<viewtimeVector.size();i++)
    {
        cout << "\t" << viewtimeVector[i].start << " " << viewtimeVector[i].end << " item = " << viewtimeVector[i].item << "size = " << viewtimeVector[i].length << endl;
    }
*/
    return viewtimeVector;
}
