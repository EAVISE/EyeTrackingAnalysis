#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iomanip>
#include <dirent.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>
#include "opencv2/ml/ml.hpp"
#include <opencv2/gpu/gpu.hpp>
#include <boost/lexical_cast.hpp>

#include "loadvariables.h"

using namespace std;
using namespace cv;

struct func_params_ {
  Point Punt1;
  Point Punt2;
  bool drawing_box;
  bool PauseVideo;   
  CvRect box;
};

void my_mouse_callback(int event, int x, int y, int flags, void* param);

double GetFrameRate(char VideoFileName[200]);

#endif // IMAGEPROCESSING_H
