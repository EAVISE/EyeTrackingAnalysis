#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <dirent.h>

#include "opencv2/opencv.hpp"
#include "opencv2/features2d.hpp"

#include "Matching.h"

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
	if (argc < 2) {
            // Tell the user how to run the program
            std::cerr << "Usage: " << argv[0] << " <Path to image directory > " << std::endl;
            return -1;
    }
    
   	imageFeature *FOIStruct;
  	imageFeature *VideoFrameStruct;
  	Ptr<BRISK> detector = BRISK::create();
      	
    // extract features from OOI
    char imagePath_OOI[200];
    sprintf(imagePath_OOI, "%s", "/home/sdb/Desktop/promo2.png");    
  	
  	cout << "path = " << imagePath_OOI << endl;  	
    FOIStruct = CalculateFeatures(imagePath_OOI,1,detector,0.05);
    cout << "#features FOI = " << FOIStruct->kp.size() << endl;
  	
  	ofstream Result;
  	Result.open("result.txt");
  	  	
    // loop over all extracted images    
    for(int i=1;i<3000;i++)
    {
        char imagePath[200];
        sprintf(imagePath, "%s%05d%s", argv[1], i, ".png");
		cout << " Processing frame: " << imagePath << endl;
        VideoFrameStruct = CalculateFeatures(imagePath,1,detector,0);
               
        if(!FOIStruct->image.empty()&&!FOIStruct->features.empty()&&!VideoFrameStruct->image.empty()&&!VideoFrameStruct->features.empty())
        {
            float score;
            Rect BB = matching2Images(FOIStruct,VideoFrameStruct,score);
        	cout << "\t score" << score << "\n";

            Mat image = VideoFrameStruct->image.clone();

            rectangle(image,BB,Scalar(255,0,255),2);

            char OutputFileName[200];
            sprintf(OutputFileName,"%s%d%s","Detections/",i,".jpg");
            imwrite(OutputFileName,image);
            
            // score = 999 means that not valid detection recangle was found            
            if(score!=999)
                Result <<   i << ", " << ", " << BB.x << ", " << BB.y << ", " << BB.width << ", " << BB.height << ", " << score << endl;
        }
        cout << "----------------------------------------" << endl;
    }    

    return 0;
}
