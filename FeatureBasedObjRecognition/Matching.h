#ifndef MATCHING_H
#define MATCHING_H

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iomanip>
#include <dirent.h>

#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>


#define DefConfidence 0.99
#define DefDistance 3.0

using namespace std;
using namespace cv;


//-------------------------------------------------------------------------------//
struct imageFeature {                           // ImageFeature structure
    vector <KeyPoint> kp;                       // Keypoints
    Mat features;                               // Descriptor
    Mat image;                                  // image
};
//-----------------------------------------------------------------------------------

int ratioTest(vector<vector<DMatch> > &matches, float ratio);

imageFeature* CalculateFeatures(char FileName[100], int scale, Ptr<BRISK> detector, float border);

void symmetryTest(const vector<vector<DMatch> >& matches1,const vector<vector<DMatch> >& matches2,vector<DMatch>& symMatches);

Mat ransacTest(const vector<DMatch>& matches,const vector<KeyPoint>& keypoints1,const vector<KeyPoint>& keypoints2,vector<DMatch>& outMatches);

int test(BRISK BRISKD);

Rect matching2Images(imageFeature *FOIStruct, imageFeature *VideoFrameStruct, float &score);

#endif // MATCHING_H
