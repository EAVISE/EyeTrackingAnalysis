#include "Matching.h"

using namespace std;
using namespace cv;

#define CVCUDA cv::cuda


//#define showFeatures
#define defratio 0.84
#define ScoreThresHold 0.057
//#define EnableOverview

//====================================================================================
// This function detects ORB keypoints in an image
// When the ORB keypoints are detected, their corresponsing descriptors were computed
// The ouput of this function is a structure which contains the following fields:
//      - vector of keypoints
//      - Descriptors
//      - Image itself in an Mat object
//====================================================================================
imageFeature* CalculateFeatures(char FileName[100],int scale,Ptr<BRISK> detector, float border )
{        
    imageFeature *Im = new imageFeature;
    //Ptr<ORB> detector = ORB::create(500,1.2,25,23,0,2,ORB::HARRIS_SCORE,23);  
    //Ptr<AKAZE> detector = AKAZE::create();
    
    Mat image1 = imread(FileName);

    if(image1.empty() || (image1.cols==2 && image1.rows==2))
        return Im;

	int top,bottom,left,right;
         /// Initialize arguments for the filter
    top = (int) (border*image1.rows); bottom = (int) (border*image1.rows);
    left = (int) (border*image1.cols); right = (int) (border*image1.cols);
  	  		
 	copyMakeBorder(image1, image1, top, bottom, left, right, BORDER_CONSTANT, Scalar(0,0,0) );

    Im->image = image1;

    CV_Assert(!image1.empty() && image1.depth() == CV_8U);
    if( image1.channels() > 1 )
        cvtColor(image1, image1, COLOR_BGR2GRAY);

    Mat image; // to contain resized image
	if(scale!=1)
	{
    	resize(image1,image,Size(image1.cols*scale,image1.rows*scale));
	}
	else
	{
		image = image1;
	}
    
   	detector->detectAndCompute( image, Mat(), Im->kp, Im->features);
    
    return Im;          // return image structure;
}

//-----------------------------------------------------------------------------------------

//====================================================================================
// Clear matches for which NN ratio is > than threshold
// NN ratio is defined: best distance / second best distance
// ratio must be in the range between 0 - 0.99
// A large value of ratio means a large number of removed matches
// This function returns the number of removed points
// (corresponding entries being cleared,i.e. size will be 0)
//====================================================================================
int ratioTest(vector<vector<DMatch> > &matches, float ratio)
{
    int removed=0;
    // for all matches
    for (vector<vector<DMatch> >::iterator matchIterator= matches.begin(); matchIterator!= matches.end(); ++matchIterator)
    {
        // if 2 NN has been identified
        if (matchIterator->size() > 1)
        {
            if ((*matchIterator)[0].distance / (*matchIterator)[1].distance > ratio)
            {
                matchIterator->clear(); // remove match
                removed++;
            }
        }
        else
        { // does not have 2 neighbours
            matchIterator->clear(); // remove match
            removed++;
        }
    }
return removed;
}

//-----------------------------------------------------------------------------------------

//====================================================================================
// This function extracts the matches that are in agreement with bots sets
// This is the symmetrical matching scheme imposing that, for a match pair to be accepted,
// both points must be the best matching feature of the other
//====================================================================================
// Insert symmetrical matches in symMatches vector
void symmetryTest(const vector<vector<DMatch> >& matches1,const vector<vector<DMatch> >& matches2,vector<DMatch>& symMatches)
{
    // for all matches image 1 -> image 2
    for (vector<vector<DMatch> >::const_iterator matchIterator1= matches1.begin(); matchIterator1!= matches1.end(); ++matchIterator1)
    {
        // ignore deleted matches
        if (matchIterator1->size() < 2)
            continue;
        // for all matches image 2 -> image 1
        for (vector<vector<DMatch> >::const_iterator matchIterator2= matches2.begin();matchIterator2!= matches2.end();++matchIterator2)
        {
            // ignore deleted matches
            if (matchIterator2->size() < 2)
                continue;
            // Match symmetry test
            if ((*matchIterator1)[0].queryIdx ==(*matchIterator2)[0].trainIdx &&(*matchIterator2)[0].queryIdx ==(*matchIterator1)[0].trainIdx)
            {
                // add symmetrical match
                symMatches.push_back(DMatch((*matchIterator1)[0].queryIdx,(*matchIterator1)[0].trainIdx,(*matchIterator1)[0].distance));
                break; // next match in image 1 -> image 2
            }
        }
    }
}
//----------------------------------------------------------------------------------------------------------//

//====================================================================================
// This function is an additional filtering test that uses a fundamental matrix in order
// to reject matches that do not obey the epipolar contraint.
//====================================================================================
Mat ransacTest(const vector<DMatch>& matches,const vector<KeyPoint>& keypoints1,const vector<KeyPoint>& keypoints2,vector<DMatch>& outMatches)
{
    outMatches.clear();
    
    double confidence = DefConfidence;
    double distance = DefDistance;

    vector<int> queryIdxs( matches.size() ), trainIdxs( matches.size() );
    for( size_t i = 0; i < matches.size(); i++ )
    {
        queryIdxs[i] = matches[i].queryIdx;
        trainIdxs[i] = matches[i].trainIdx;
    }
    vector<Point2f> points1;
    vector<Point2f> points2;
    KeyPoint::convert(keypoints1, points1, queryIdxs);
    KeyPoint::convert(keypoints2, points2, trainIdxs);

    // Compute F matrix using RANSAC
    vector<uchar> inliers(points1.size(),0);
    Mat fundemental= findFundamentalMat(Mat(points1),Mat(points2),inliers,CV_FM_RANSAC,distance,confidence);

    // extract the surviving (inliers) matches
    vector<uchar>::const_iterator itIn= inliers.begin();
    vector<DMatch>::const_iterator itM= matches.begin();
    // for all matches
    for ( ;itIn!= inliers.end(); ++itIn, ++itM)
    {
        if (*itIn)
        { // it is a valid match
            outMatches.push_back(*itM);
        }
    }
    
    bool refineF = true;
    if (refineF) 
    {
    	// The F matrix will be recomputed with
    	// all accepted matches
    	// Convert keypoints into Point2f
    	// for final F computation
    	points1.clear();
    	points2.clear();
    	for (std::vector<cv::DMatch>::const_iterator it= outMatches.begin();it!= outMatches.end(); ++it) 
    	{
    		// Get the position of left keypoints
    		float x= keypoints1[it->queryIdx].pt.x;
    		float y= keypoints1[it->queryIdx].pt.y;
    		points1.push_back(cv::Point2f(x,y));
    		// Get the position of right keypoints
    		x= keypoints2[it->trainIdx].pt.x;
    		y= keypoints2[it->trainIdx].pt.y;
    		points2.push_back(cv::Point2f(x,y));
    	}
	
		// Compute 8-point F from all accepted matches
    	if (points1.size()>0&&points2.size()>0)
    	{
    		fundemental= cv::findFundamentalMat(cv::Mat(points1),cv::Mat(points2), // matches
                               CV_FM_8POINT); // 8-point method
    	}
    }
    
    
    return fundemental;
}
//----------------------------------------------------------------------------------------------------------//

//====================================================================================
// Matching//
//====================================================================================
Rect matching2Images(imageFeature *FOIStruct,imageFeature *VideoFrameStruct,float &score)
{ 
    //BruteForceMatcher<HammingLUT> matcher;
    BFMatcher matcher(NORM_L2);
    vector<vector<DMatch> > matches12, matches21;
    vector<DMatch> symMatches,matches;
    //float score = 0;
    float TH = ScoreThresHold;
    float ratio = defratio;
    float dist = 0;
    float surface = 0;
    char OutputFileName[100];
    Rect BBrect;
/*
    CVCUDA::GpuMat FOIDescriptorsGpu(FOIStruct->features);
	CVCUDA::GpuMat VideoDescriptorsGpu(VideoFrameStruct->features);

	cv::Ptr<cv::cuda::DescriptorMatcher> gpuMatcher;
	gpuMatcher = cv::cuda::DescriptorMatcher::createBFMatcher(cv::NORM_HAMMING);
	gpuMatcher->knnMatch(FOIDescriptorsGpu, VideoDescriptorsGpu, matches12, 2);
	gpuMatcher->knnMatch(VideoDescriptorsGpu, FOIDescriptorsGpu, matches21, 2);
*/  
    matcher.knnMatch(FOIStruct->features,VideoFrameStruct->features,matches12,2);
    matcher.knnMatch(VideoFrameStruct->features,FOIStruct->features,matches21,2);
    #ifdef EnableOverview
    Overview << "\tsize Matches12: " << matches12.size() << "\n";
    Overview << "\tsize Matches21: " << matches21.size() << "\n";
    #endif

    int removed= ratioTest(matches12,ratio);
    #ifdef EnableOverview
    Overview << "\tsize Matches12 after ratio test: " << matches12.size()-removed << "\n";
    #endif
    removed= ratioTest(matches21,ratio);
    #ifdef EnableOverview
    Overview << "\tsize Matches21 after ratio test: " << matches21.size()-removed << "\n";
    #endif

    symmetryTest(matches12,matches21,symMatches);
    #ifdef EnableOverview
    Overview << "\tsize Matches after symmetry test: " << symMatches.size() << "\n";
    #endif

    cout << "\tsize Matches after symmetry test: " << symMatches.size() << endl;
    
    score = symMatches.size();
    matches = symMatches;

    int matchesTH = 10;
    if(symMatches.size()<=matchesTH)
    {
        matches = symMatches;
        score = 999;
        #ifdef EnableOverview
        Overview << "\tNOT ENOUGH MATCHES FOR RANSAC \n";
        #endif
    }
    else
    {
        Mat fundemental= ransacTest(symMatches,FOIStruct->kp, VideoFrameStruct->kp, matches);    // return the found fundemental matrix
        #ifdef EnableOverview
        Overview << "\tsize Matches after RANSAC: " << matches.size() << "\n";
        #endif
        cout << "\tsize Matches after RANSAC: " << matches.size() << "\n";

        if(matches.size()>0)
        {
            for(int i=0;i<matches.size();i++)
            {
                dist = dist + matches.at(i).distance;
                surface = surface + FOIStruct->kp.at(matches.at(i).queryIdx).size + VideoFrameStruct->kp.at(matches.at(i).trainIdx).size;
            }
            dist = dist / matches.size();
            score = dist / surface;
        }
        else
        {
            score = 999;
        }
    }

	// if score is good enough, locate the object in the image and return a rectangle in which the object is found
    if(score<1)
    {
    	 //-- Localize the object           
    	std::vector<Point2f> obj;
    	std::vector<Point2f> scene;
		for( int i = 0; i < matches.size(); i++ )
    	{
    		//-- Get the keypoints from the good matches
        	obj.push_back( FOIStruct->kp[ matches[i].queryIdx ].pt );
        	scene.push_back( VideoFrameStruct->kp[matches[i].trainIdx ].pt );
    	}
    
    	cv::Mat arrayRansac;
    	std::vector<uchar> inliers(obj.size(),0);
    	Mat H = findHomography( obj, scene, CV_RANSAC,3,inliers);
    
    	//-- Get the corners from the image_1 ( the object to be "detected" )
    	std::vector<Point2f> obj_corners(4);
    	obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( FOIStruct->image.cols, 0 );
    	obj_corners[2] = cvPoint( FOIStruct->image.cols, FOIStruct->image.rows ); obj_corners[3] = cvPoint( 0, FOIStruct->image.rows );
    	std::vector<Point2f> scene_corners(4);
    
    	perspectiveTransform( obj_corners, scene_corners, H);

        // define a rectangle around the Object of Interest
        BBrect.x = scene_corners[0].x;
        BBrect.y = scene_corners[0].y;
        BBrect.width =max(abs(scene_corners[0].x - scene_corners[1].x), abs(scene_corners[3].x - scene_corners[2].x));
        BBrect.height =max(abs(scene_corners[0].y - scene_corners[3].y) , abs(scene_corners[1].y - scene_corners[2].y));       
    }
    return BBrect;


}

