#ifndef MATCHINGTHREAD_H
#define MATCHINGTHREAD_H

#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include <QThread>
#include "loadvariables.h"

class MatchingThread : public QThread
{
    Q_OBJECT
public:
    MatchingThread(QObject *parent = 0);    
    ~MatchingThread();

    void matching(int NumberOfQueryObjects, int MaxFOI, Parameters *ParamNew);

signals:
    void updateProgBar(int progbar);

protected:
    void run();
    
public slots:

private:
    QMutex mutex;
    QWaitCondition condition;   
    int NumberOfQueryObjects;
    int MaxFOI;
    bool restart;
    bool abort;
    Parameters *Param;
};

#endif // MATCHINGTHREAD_H
