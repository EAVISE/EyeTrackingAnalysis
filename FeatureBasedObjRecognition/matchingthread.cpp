#include "matchingthread.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>
#include "opencv2/ml/ml.hpp"
#include <opencv2/gpu/gpu.hpp>

#include <boost/lexical_cast.hpp>
#include <Matching.h>

#include <iostream>
#include "mainwindow.h"
#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

MatchingThread::MatchingThread(QObject *parent) :
    QThread(parent)
{
    // constructor
}

MatchingThread::~MatchingThread()
{
    // destructor
}

//====================================================================================
// This function replaces a part a string
// Input is String itself, string to replace, string a replacement
//====================================================================================
void ReplaceStringInPlace(std::string& subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

void MatchingThread::matching(int NumberOfQueryObjects, int MaxFOI, Parameters *ParamNew)
{
    QMutexLocker locker(&mutex);    
    this->NumberOfQueryObjects = NumberOfQueryObjects;
    this->MaxFOI = MaxFOI;
    this->Param = ParamNew;

    if (!isRunning())
    {
         start(TimeCriticalPriority);
    }
    else
    {
        restart = true;
        condition.wakeOne();
    }    
}

void MatchingThread::run()
{
    clock_t startT, endT;
    startT = clock();

    mutex.lock();    
    int NumberOfQueryObjects = this->NumberOfQueryObjects;
    int MaxFOI = this->MaxFOI;    
    Parameters *Parameter = this->Param;
    mutex.unlock();

    char CroppedVideoFrame[200];    
    char ResultFileName[200];
    char CroppedFOI[200];
    char FOIpath[200];
    char FrameFolder[200];
    int nFeatures = 500;
    float scaleFactor = 1.2f;
    int nlevels = 25;
    int edgeThreshold = 23;
    int firstlevel = 0;
    int patchSize = 23;
    int ImageUpscale = 1;

    int progbar=0;
    int stepProgressBar = (NumberOfQueryObjects)/100;

    float score;
    float dist=0;
    float surface=0;


    sprintf(FrameFolder,"%s%s",Parameter->WorkFolder_,"/output/imgCrop-");
    cout << "Frame folder Object Detection = " << FrameFolder << endl;

    sprintf(FOIpath,"%s%s", Parameter->WorkFolder_,"/Train/TrainImg_");
    cout << "Folder FOI = " << FOIpath << endl;

    ImageFeature *VideoFrameStruct;
    ImageFeature *FOIStruct;

    Ptr<FeatureDetector> detector;
    detector = new ORB(nFeatures,scaleFactor,nlevels,edgeThreshold,firstlevel,2,ORB::HARRIS_SCORE,patchSize);    
    DescriptorExtractor * extractor = new ORB();


    for(int queryID=0;queryID<NumberOfQueryObjects;queryID++)          // outer for loop
    {
        sprintf(CroppedVideoFrame,"%s%05d%s", FrameFolder,queryID,".ppm");
        cout << "filename = " << CroppedVideoFrame << endl;
        VideoFrameStruct = CalculateFeatures(CroppedVideoFrame,detector,extractor,ImageUpscale);
        cout << " features are calculated..." << endl;
        if(queryID%stepProgressBar == 1)
        {
            progbar++;
            emit updateProgBar(progbar);
        }

        //cout << "image size = " << VideoFrameStruct->image.size() << " | features size = " << VideoFrameStruct->features.size() << endl;
        //cout << "image empty = " << VideoFrameStruct->image.empty() << " features empty = " << VideoFrameStruct->features.empty() << endl;
        if(!VideoFrameStruct->image.empty()&&!VideoFrameStruct->features.empty())
        {
            for(int NrofFOI = 0;NrofFOI<MaxFOI;NrofFOI++)
            {
                score = 0;
                surface = 0;
                dist = 0;

                // add path to workding dir
                sprintf(ResultFileName,"%s%s%05d%s",Parameter->WorkFolder_,"/ObjectDet/TrainImg_",NrofFOI,".txt");
                ofstream Result (ResultFileName,ios::app);
                Result << queryID << ": " ;

                sprintf(CroppedFOI,"%s%05d%s", FOIpath,NrofFOI,".ppm");
                cout << "FOI path = " << FOIpath << endl;
                cout << "FOI size = " << FOIStruct->image.size << endl;

                FOIStruct = CalculateFeatures(CroppedFOI,detector,extractor,ImageUpscale);
                cout << " FOI features = " << FOIStruct->features.size() << endl;

                //drawKeypoints(FOIStruct->image,FOIStruct->kp,ShowKeypoints,Scalar(255,0,0),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
                //imwrite("KP.png", FOIStruct->image);
                //char KPimage[200];
                //sprintf(KPimage, "%s%05d%s", "Matches/KP_",queryID,".ppm");
                //imwrite(KPimage,FOIStruct->image);

                if(!FOIStruct->image.empty()&&!FOIStruct->features.empty())
                {
                    score = matching2Images(FOIStruct,VideoFrameStruct, NrofFOI,queryID);
                    Result << score << "\n";
                }
                else
                {
                    cout << "FOI " << NrofFOI << "was empty" << endl;
                }
                delete(FOIStruct);
            }
        }
        else
        {
            cout << "VideoFrame " << queryID << " was empty" << endl;
            for(int NrofFOI = 0;NrofFOI<MaxFOI;NrofFOI++)
            {
                sprintf(ResultFileName,"%s%s%05d%s",Parameter->WorkFolder_,"/ObjectDet/TrainImg_",NrofFOI,".txt");
                ofstream Result (ResultFileName,ios::app);
                Result << queryID << ": 999 \n";
            }
        }
        delete(VideoFrameStruct);
    }
    delete(extractor);
    detector.release();

    cout << "Calculation in thread is finished!" << endl;
    endT = clock();
    cout << "Time required for execution: " << (double)(endT-startT)/CLOCKS_PER_SEC << " seconds." << "\n";
}

