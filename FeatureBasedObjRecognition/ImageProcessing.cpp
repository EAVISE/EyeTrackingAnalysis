#include "ImageProcessing.h"
#include "loadvariables.h"

using namespace std;
using namespace cv;

//====================================================================================
//
//
//====================================================================================
// Implement mouse callback
//void my_mouse_callback( int event, int x, int y, int flags, void* param ){
void my_mouse_callback( int event, int x, int y, int flags, void* param ){

    func_params_* ParamMouse = (func_params_*) param;
    switch( event ){
        case CV_EVENT_MBUTTONDOWN:
            ParamMouse->PauseVideo = true;
            break;

        case CV_EVENT_MOUSEMOVE:
            if(ParamMouse->drawing_box ){
                ParamMouse->box.width = x-ParamMouse->box.x;
                ParamMouse->box.height = y-ParamMouse->box.y;
            }
            break;

        case CV_EVENT_LBUTTONDOWN:
            ParamMouse->drawing_box = true;
            ParamMouse->box = cvRect( x, y, 0, 0 );
            ParamMouse->Punt1.y = y;
            ParamMouse->Punt1.x = x;
            break;

        case CV_EVENT_LBUTTONUP:
            ParamMouse->drawing_box = false;
            ParamMouse->PauseVideo = false;
            ParamMouse->Punt2.x = x;
            ParamMouse->Punt2.y = y;           
            break;
    }
}
//--------------------------------------------------------------------------------

double GetFrameRate(char VideoFileName[200])
{
    VideoCapture capture(VideoFileName);
    double rate= capture.get(CV_CAP_PROP_FPS);
    return rate;
}
