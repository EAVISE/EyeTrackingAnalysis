% Geschreven door: Stijn De Beugher (uitbreiding thesis Kevin Deleus)
% Master Elektronica-ICT
% 2014-2015

% Input = Data: De data uit het XML-bestand

% Output = - DataArr: An array containing all the data as described below
%          - Len: length of this array
%          - Nr: number of first frame

% Column 1: frame number
% Column 2: relative X-position of right hand
% Column 3: relative Y-position of right hand
% Column 4: relative X-position of left hand
% Column 5: relative Y-position of left hand

% Column 6: x-position of center torso
% Column 7: y-position of center torso
% Column 8: x-position of center face
% Column 9: y-position of center face

% Column 10: x-pos of upper left corner of torso detection        
% Column 11: y-pos of upper left corner of torso detection

% Column 12: width of torso detection        
% Column 13: height of torso detection
        
% Column 14: width of face detection                

% column 15: Raw x-posoiton of right hand
% column 16: Raw y-position of right hand

% column 17: Raw x-posoiton of left hand
% column 18: Raw y-position of left hand

function [Coord,Len,Nr] = convertData(Data,s, Path)
   
    % Nummer van de eerste frame opzoeken
    Nr = s.Saves.Frame{1}.Number.Text;
    
    % Bepalen hoeveel frames er in het databestand zitten
    Len = length(s.Saves.Frame); 
    
    widths = [];   
    filteredWidth = [];
    N = 11;
    temp = [];
    
    RawLeft = [];
    RawRight = [];
    RawTorso = [];
    RawCenter = [];
    RawFace = [];
    
    displaceLeft = [];
    displaceRight = [];
    % De nodige data in een array stoppen
    startframe = 1;
    for i = startframe:Len        
        Nr = s.Saves.Frame{i}.Number.Text;        
        Coord(i,1) = str2num(s.Saves.Frame{i}.Number.Text);

        Body       = s.Saves.Frame{i}.UpperBody.Rect;           % Coordinaten/dimensies van het lichaam opslaan
        XBody      = str2num(Body.x.Text);
        YBody      = str2num(Body.y.Text);
        WBody      = str2num(Body.Width.Text);
        HBody      = str2num(Body.Height.Text);        
        
        Face        = s.Saves.Frame{i}.UpperBody.Face.Rect; 
        WFace       = str2num(Face.Width.Text);
        XFace       = str2num(Face.x.Text);
        YFace       = str2num(Face.y.Text);       
        
        widths = [widths; WFace WBody];
        last = 0;            
        
        % median over ub width
        if(size(temp, 1)) < N
            width = WBody;
            temp = [temp; WBody];
        else
            temp = [temp; WBody];
            temp(1) = [];
            width = median(temp);
        end        
        WBody = width;                
        filteredWidth = [filteredWidth; width];
              
       
        % Calculate center of body
        Xcenter     = XBody + WBody/2;
        Ycenter     = YBody + HBody/2;        
        
        RHand      = s.Saves.Frame{i}.UpperBody.Hand(2);        % Coordinaten van de rechterhand opslaan
        LHand      = s.Saves.Frame{i}.UpperBody.Hand(1);        % Coordinaten van de linkerhand opslaan   
        
        RJoint = s.Saves.Frame{i}.UpperBody.Joint(2);
        LJoint = s.Saves.Frame{i}.UpperBody.Joint(1);
        
        X_Right_Center = (str2num(RHand{1}.Point.x.Text));%+str2num(RJoint{1}.Point.x.Text))/2;
        Y_Right_Center = (str2num(RHand{1}.Point.y.Text));%+str2num(RJoint{1}.Point.y.Text))/2;
        
        X_Left_Center = (str2num(LHand{1}.Point.x.Text));%+str2num(LJoint{1}.Point.x.Text))/2;
        Y_Left_Center = (str2num(LHand{1}.Point.y.Text));%+str2num(LJoint{1}.Point.y.Text))/2;

        %remove outliers of the hand positions. If the hand coordinates
        %between two consecutive frames is larger than a predefined
        %threshold (175), then we discard this outlier position and replace
        %it with an averaged coordinate.

        displaceTH = 175;
        if((i>1)&&(i<Len))        
            LHand_Prev      = s.Saves.Frame{i-1}.UpperBody.Hand(1);        % Coordinaten van de linkerhand opslaan
            LJoint_Prev = s.Saves.Frame{i-1}.UpperBody.Joint(1);
            X_Left_Center_Prev = (str2num(LHand_Prev{1}.Point.x.Text));%+str2num(LJoint_Prev{1}.Point.x.Text))/2;
            Y_Left_Center_Prev = (str2num(LHand_Prev{1}.Point.y.Text));%+str2num(LJoint_Prev{1}.Point.y.Text))/2;


            LHand_Next = s.Saves.Frame{i+1}.UpperBody.Hand(1);        % Coordinaten van de linkerhand opslaan
            LJoint_Next	= s.Saves.Frame{i+1}.UpperBody.Joint(1);
            X_Left_Center_Next = (str2num(LHand_Next{1}.Point.x.Text));%+str2num(LJoint_Next{1}.Point.x.Text))/2;
            Y_Left_Center_Next = (str2num(LHand_Next{1}.Point.y.Text));%+str2num(LJoint_Next{1}.Point.y.Text))/2;

            distL_Prev = sqrt((X_Left_Center_Prev-X_Left_Center)^2 + (Y_Left_Center_Prev-Y_Left_Center)^2);
            distL_Next = sqrt((X_Left_Center_Next-X_Left_Center)^2 + (Y_Left_Center_Next-Y_Left_Center)^2);
            %displaceLeft = [displaceLeft; distL_Prev distL_Next];
            if(distL_Prev > displaceTH && distL_Next > displaceTH)
            % replace current left pos with averaged pos
                X_Left_Center = (str2num(LHand_Next{1}.Point.x.Text));% + str2num(LHand_Prev{1}.Point.x.Text))/2;
                Y_Left_Center = (str2num(LHand_Next{1}.Point.y.Text));% + str2num(LHand_Prev{1}.Point.y.Text))/2;
                 displaceLeft = [displaceLeft; distL_Prev distL_Next 1];
            else
                 displaceLeft = [displaceLeft; distL_Prev distL_Next 0];
            end

            %remove outliers right hand
            RHand_Prev  = s.Saves.Frame{i-1}.UpperBody.Hand(2);        % Coordinaten van de linkerhand opslaan
            RJoint_Prev = s.Saves.Frame{i-1}.UpperBody.Joint(2);
            X_Right_Center_Prev = (str2num(RHand_Prev{1}.Point.x.Text));%+str2num(RJoint_Prev{1}.Point.x.Text))/2;
            Y_Right_Center_Prev = (str2num(RHand_Prev{1}.Point.y.Text));%+str2num(RJoint_Prev{1}.Point.y.Text))/2;

            RHand_Next  = s.Saves.Frame{i+1}.UpperBody.Hand(2);        % Coordinaten van de linkerhand opslaan
            RJoint_Next	= s.Saves.Frame{i+1}.UpperBody.Joint(2);
            X_Right_Center_Next = (str2num(RHand_Next{1}.Point.x.Text));%%+str2num(RJoint_Next{1}.Point.x.Text))/2;
            Y_Right_Center_Next = (str2num(RHand_Next{1}.Point.y.Text));%+str2num(RJoint_Next{1}.Point.y.Text))/2;

            distR_Prev = sqrt((X_Right_Center_Prev-X_Right_Center)^2 + (Y_Right_Center_Prev-Y_Right_Center)^2);
            distR_Next = sqrt((X_Right_Center_Next-X_Right_Center)^2 + (Y_Right_Center_Next-Y_Right_Center)^2);
            %displaceRight = [displaceRight; distR_Prev distR_Next];

            if(distR_Prev > displaceTH && distR_Next > displaceTH)
            % replace current right pos with averaged pos
                X_Right_Center = (str2num(RHand_Next{1}.Point.x.Text));% + str2num(RHand_Prev{1}.Point.x.Text))/2;
                Y_Right_Center = (str2num(RHand_Next{1}.Point.y.Text));% + str2num(RHand_Prev{1}.Point.y.Text))/2;
                 displaceRight = [displaceRight; distR_Prev distR_Next 1];
            else
                displaceRight = [displaceRight; distR_Prev distR_Next 0];
            end
        end 
                
        RawRight = [RawRight; X_Right_Center Y_Right_Center];
        RawLeft = [RawLeft; X_Left_Center Y_Left_Center];    
        
        RawTorso = [RawTorso;XBody YBody WBody HBody];
        RawCenter = [RawCenter; Xcenter Ycenter];
        RawFace = [RawFace; XFace YFace];        
        
        % Calculation of hand position
        ScaleFactor = WBody/100;        
        X_Right_Rel = (str2num(RHand{1}.Point.x.Text) -Xcenter)/ScaleFactor;
        X_Left_Rel = (str2num(LHand{1}.Point.x.Text) - Xcenter)/ScaleFactor;
        Y_Right_Rel = (Ycenter -str2num(RHand{1}.Point.y.Text))/ScaleFactor;
        Y_Left_Rel =  (Ycenter - str2num(LHand{1}.Point.y.Text))/ScaleFactor;
        
        %new calc !! ONLY WHEN JOINT = WRIST !!
        % Alternative hand position calculation that could be used when the
        % joint corresponds always the the wrist. Then we calculate the
        % center between fingertips and wrist, resulting in a coordinate
        % that overlaps with the center of the hand
        
        %ScaleFactor = WBody/100;        
        %X_Right_Rel = round((X_Right_Center -Xcenter) / ScaleFactor);
        %X_Left_Rel = round((X_Left_Center - Xcenter) / ScaleFactor);
        %Y_Right_Rel = round((Ycenter - Y_Right_Center) / ScaleFactor);
        %Y_Left_Rel =  round((Ycenter - Y_Left_Center) / ScaleFactor);
        
       % store all the relevant data into a large dataformat 
        Coord(i,2) = round(X_Right_Rel); %Right X
        Coord(i,3) = round(Y_Right_Rel); %Right Y
        
        Coord(i,4) = round(X_Left_Rel); %Left X
        Coord(i,5) = round(Y_Left_Rel); %Left Y 
        
        Coord(i,6) = Xcenter;   %center-X torso
        Coord(i,7) = Ycenter;   %center-Y torso
        
        Coord(i,8) = XFace;   %center-X face
        Coord(i,9) = YFace;   %center-Y face
        
        Coord(i,10) = XBody;
        Coord(i,11) = YBody;
        
        Coord(i,12) = WBody;
        Coord(i,13) = HBody;
        
        Coord(i,14) = WFace;    
        
        Coord (i,15) = X_Right_Center;
        Coord (i,16) = Y_Right_Center;
        
        Coord (i,17) = X_Left_Center;
        Coord (i,18) = Y_Left_Center;        
    end    
end
