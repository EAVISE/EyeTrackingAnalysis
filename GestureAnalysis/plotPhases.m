function plotPhases()

close all;

%NeuroPeirce 
Data = xmlread('/media/sdb/DATAPART2/SaGA/SaGA-V07/FinalDet13000.xml');
Path = '/media/sdb/DATAPART2/SaGA/SaGA-V07/frames';
PathFile = '/home/sdb/EyeTrackingAnalysis/ExportGesture2Elan/build/Phases9.txt'

% De extensie van de frame ingeven.
Ext = '.png';

unix('mkdir -p GesturePhases')

try
    load('s.mat');
catch
    [ s ] = xml2struct(Data);
    save('s.mat', 's');
end

% convert raw data
[DataArr,Len,Nr] = convertData(Data,s, Path);

% read txt file of final gesture phase
 [start stop length] = textread(PathFile, ...
 '%d %d %d');


LeftHand  = [];
RightHand  = [];
LeftHand = [LeftHand; DataArr(:,17) DataArr(:,18)];
RightHand = [RightHand; DataArr(:,15) DataArr(:,16)];

Len = size(RightHand,1)
for i=1:Len-1
    RightHand(i,3) = sqrt((RightHand(i,1)-RightHand(i+1,1))^2 + (RightHand(i,2)-RightHand(i+1,2))^2);
    if(RightHand(i,3)>200)
        RightHand(i,1) = RightHand(i+1,1);
        RightHand(i,2) = RightHand(i+1,2);
        RightHand(i,3) = sqrt((RightHand(i,1)-RightHand(i+1,1))^2 + (RightHand(i,2)-RightHand(i+1,2))^2);
    end
end


Len = size(LeftHand,1)
for i=2:Len
    LeftHand(i,3) = sqrt((LeftHand(i,1)-LeftHand(i-1,1))^2 + (LeftHand(i,2)-LeftHand(i-1,2))^2);
    if(LeftHand(i,3)>200)
        LeftHand(i,1) = LeftHand(i+1,1);
        LeftHand(i,2) = LeftHand(i+1,2);
        LeftHand(i,3) = sqrt((LeftHand(i,1)-LeftHand(i+1,1))^2 + (LeftHand(i,2)-LeftHand(i+1,2))^2);
    end
end

DataArr(:,15) = RightHand(:,1)
DataArr(:,16) = RightHand(:,2)
DataArr(:,17) = LeftHand(:,1)
DataArr(:,18) = LeftHand(:,2)


PhaseLen = size(start,1)

for j=1:PhaseLen
%for j=67:67
    startindex = start(j)
    stopindex = stop(j)

    title = [Path '/' sprintf('%05d', startindex) '.png']
    A = imread (title);
    figure(1),clf;
    imshow(A);
    hold on;
    
    PhaseRight = [];
    PhaseLeft = [];
 
    THDisplace = 50;
 
    PhaseRight = [PhaseRight; DataArr(startindex,15) DataArr(startindex,16) 5];
    PhaseLeft = [PhaseLeft; DataArr(startindex,17) DataArr(startindex,18) 5];
 
    for i=startindex:stopindex
        plot(DataArr(i,15),DataArr(i,16),'ob','MarkerSize',10);         % RIGHT hand = blue
        plot(DataArr(i,17),DataArr(i,18),'or','MarkerSize',10);         % LEFT hand = red

        lastRight =  size(PhaseRight,1);
        lastLeft =  size(PhaseLeft,1);

        diffRight = sqrt((DataArr(i,15)-PhaseRight(lastRight,1))^2 + (DataArr(i,16)-PhaseRight(lastRight,2))^2)
        diffLeft = sqrt((DataArr(i,17)-PhaseLeft(lastLeft,1))^2 + (DataArr(i,18)-PhaseLeft(lastLeft,2))^2)  

        if(diffRight>THDisplace)
            PhaseRight = [PhaseRight; DataArr(i,15) DataArr(i,16) 5];
        else
            PhaseRight(lastRight,3) = PhaseRight(lastRight,3)+1;
        end  

        if(diffLeft>THDisplace)
            PhaseLeft = [PhaseLeft; DataArr(i,17) DataArr(i,18) 5];
        else
            PhaseLeft(lastLeft,3) = PhaseLeft(lastLeft,3)+1;
        end  
    end         
  

    figure(2),clf;
    imshow(A);
    hold on;
    
    % plot dots
    Len = size(PhaseRight,1);
    for i=1:Len
         plot(PhaseRight(i,1),PhaseRight(i,2),'ob','MarkerSize',PhaseRight(i,3));         % RIGHT hand = blue
    end

    Len = size(PhaseLeft,1);
    for i=1:Len
         plot(PhaseLeft(i,1),PhaseLeft(i,2),'or','MarkerSize',PhaseLeft(i,3));         % RIGHT hand = blue
    end

    % plot lines
    Len = size(PhaseRight,1);
    for i=1:Len-1
        X = [PhaseRight(i,1) PhaseRight(i+1,1)];
        Y = [PhaseRight(i,2) PhaseRight(i+1,2)];
        plot(X,Y,'-b', 'LineWidth',3);         % RIGHT hand = blue
     
        first = [PhaseRight(i,1) PhaseRight(i,2)];
        last = [PhaseRight(i+1,1) PhaseRight(i+1,2)];
        arrow(first,last, 'EdgeColor','b','FaceColor','b', 'Length',20);
    end

    Len = size(PhaseLeft,1);
    for i=1:Len-1
        X = [PhaseLeft(i,1) PhaseLeft(i+1,1)];
        Y = [PhaseLeft(i,2) PhaseLeft(i+1,2)];
        plot(X,Y,'-r', 'LineWidth',3);         % LEFT hand = red
     
        first = [PhaseLeft(i,1) PhaseLeft(i,2)];
        last = [PhaseLeft(i+1,1) PhaseLeft(i+1,2)];
        arrow(first,last, 'EdgeColor','r','FaceColor','r', 'Length',20);
    end
    
    f = getframe(gca);
    im = frame2im(f);

    title = ['GesturePhases/' sprintf('%05d', j) '.png'];
    imwrite(im,title);        
    
    %pause;
end

end

