%function plot gesture space

% output of this function: a .txt file with a line for each frame
% each line looks like this: [GestureSpace Right Hand] [GestureSpace Left Hand]
% The possible values:
%   1  = Center-Center

%   2  = Center

%   11 = Periphery Upper Right
%   12 = Periphery Right
%   13 = Periphery Lower Right
%   14 = Periphery Lower
%   15 = Periphery Lower Left
%   16 = Periphery Left
%   17 = Periphery Upper Left
%   18 = Periphery Upper

%   21 = Extreme Periphery Upper Right
%   22 = Extreme Periphery Right
%   23 = Extreme Periphery Lower Right
%   24 = Extreme Periphery Lower
%   25 = Extreme Periphery Lower Left
%   26 = Extreme Periphery Left
%   27 = Extreme Periphery Upper Left
%   28 = Extreme Periphery Upper

function [GestureSpace] = PlotGestureSpace(DataArr,Path)

Drawing =0;
Save = 0;

%-------------------------------------------------------------------------
% Calculate average width of torso
TorsoW = DataArr(:,12);
avgW = mean(TorsoW)
%-------------------------------------------------------------------------
% Calculate average witdh of face
FaceW = DataArr(:,14);
avgFW = mean(FaceW);

%-------------------------------------------------------------------------
% Calculate average Y-pos of torso 
YTorso = DataArr(:,7);
avgCY = mean(YTorso);
%-------------------------------------------------------------------------

Len = size(DataArr,1);
GestureSpaceTempRight = [];
GestureSpaceTempLeft = [];

% Loop over all frames and calculate for each frame the various sectors of
% the MC Neill gesture space
for i = 1:Len
    clf
    frameNr = i;
    title = [Path '/' sprintf('%05d', frameNr) '.png'];
    A = imread (title);
    if(Drawing)
        figure(2);
        imshow(A);
        hold on;
    end
    
    FaceX = DataArr(i,8);
    FaceY = DataArr(:,9);        
       
    avgFX = FaceX;
    avgFY = mean(FaceY);
    DeltaY = 50;
    
    var1 = avgFY+0.25*avgFW;
    var2 = avgFY+avgFW;    
    var4 = avgCY+DeltaY*0.5;
    var5 = avgCY+DeltaY+0.50*avgFW; 
    var6 = avgCY-avgFW;
    var7 = avgCY;
    
    varA = avgFX+avgFW/2-avgW/2-avgFW;
    varB = avgFX+avgFW/2-avgW/2;    
    varD = avgFX+avgFW/2+avgW/2;
    varE = avgFX+avgFW/2+avgW/2+avgFW;
    varF = avgFX+avgFW/2 - avgW*0.33;
    varG = avgFX+avgFW/2 + avgW*0.33;

    A1x = varA;
    A1y = var1;
    
    E1x = varE;
    E1y = var1;
 
    A5x = varA;
    A5y = var5;
 
    E5x = varE;
    E5y = var5;
 
    B2x = varB;
    B2y = var2;
 
    D2x = varD;
    D2y = var2;
 
    B4x = varB;
    B4y = var4;
 
    D4x = varD;
    D4y = var4;
     
    A2x = varA;
    A2y = var2;
 
    A4x = varA;
    A4y = var4;

    E2x = varE;
    E2y = var2;
 
    E4x = varE;
    E4y = var4;
 
    B1x = varB;
    B1y = var1;
 
    B5x = varB;
    B5y = var5;
 
    D1x = varD;
    D1y = var1;
 
    D5x = varD;
    D5y = var5;
 
    F7x = varF;
    F7y = var7;
 
    G7x = varG;
    G7y = var7;
 
    F6x = varF;
    F6y = var6;
 
    G6x = varG;
    G6y = var6;
 
    B7x = varB;
    B7y = var7;
 
    A7x = varA;
    A7y =  var7;

    A6x = varA;
    A6y = var6;
 
    B6x = varB;
    B6y = var6;
 
    F1x = varF;
    F1y = var1;
      
    F2x = varF;
    F2y = var2;
 
    G1x = varG;
    G1y = var1;

    G2x = varG;
    G2y = var2;
 
    D6x = varD;
    D6y = var6;
 
    D7x = varD;
    D7y = var7;
 
    E6x = varE;
    E6y = var6;

    E7x = varE;
    E7y = var7;
 
    F4x = varF;
    F4y = var4;
 
    F5x = varF;
    F5y = var5;

    G4x = varG;
    G4y = var4;

    G5x = varG;
    G5y = var5;    
    
    % if drawing is set to 1, draw the gesture space
    if(Drawing)
        plot(A1x,A1y,'*b','MarkerSize',10);         % corner A1
        plot(E1x,E1y,'*b','MarkerSize',10);         % corner E1
        plot(A5x,A5y,'*b','MarkerSize',10);         % corner A5
        plot(E5x,E5y,'*b','MarkerSize',10);         % corner E5
        plot(B2x,B2y,'*g','MarkerSize',10);         % corner B2
        plot(D2x,D2y,'*g','MarkerSize',10);         % corner D2
        plot(B4x,B4y,'*g','MarkerSize',10);         % corner B4
        plot(D4x,D4y,'*g','MarkerSize',10);         % corner D4
        plot(A2x,A2y,'*b','MarkerSize',10);         % corner A2
        plot(A4x,A4y,'*b','MarkerSize',10);         % corner A4
        plot(E2x,E2y,'*b','MarkerSize',10);         % corner E2
        plot(E4x,E4y,'*b','MarkerSize',10);         % corner E4
        plot(B1x,B1y,'*b','MarkerSize',10);         % corner B1
        plot(B5x,B5y,'*b','MarkerSize',10);         % corner B5
        plot(D1x,D1y,'*b','MarkerSize',10);         % corner D1
        plot(D5x,D5y,'*b','MarkerSize',10);         % corner D5
        plot(F7x,F7y,'*r','MarkerSize',10);         % corner F7
        plot(G7x,G7y,'*r','MarkerSize',10);         % corner G7
        plot(F6x,F6y,'*r','MarkerSize',10);         % corner F6
        plot(G6x,G6y,'*r','MarkerSize',10);         % corner G6
        plot(B7x,B7y,'*g','MarkerSize',10);         % corner B7
        plot(A7x,A7y,'*b','MarkerSize',10);         % corner A7
        plot(A6x,A6y,'*b','MarkerSize',10);         % corner A6
        plot(B6x,B6y,'*g','MarkerSize',10);         % corner B6
        plot(F1x,F1y,'*b','MarkerSize',10);         % corner F1
        plot(F2x,F2y,'*g','MarkerSize',10);         % corner F2
        plot(G1x,G1y,'*b','MarkerSize',10);         % corner G1
        plot(G2x,G2y,'*g','MarkerSize',10);         % corner G2
        plot(D6x,D6y,'*g','MarkerSize',10);         % corner D6
        plot(D7x,D7y,'*g','MarkerSize',10);         % corner D7
        plot(E6x,E6y,'*b','MarkerSize',10);         % corner E6
        plot(E7x,E7y,'*b','MarkerSize',10);         % corner E7
        plot(F4x,F4y,'*b','MarkerSize',10);         % corner F4
        plot(F5x,F5y,'*b','MarkerSize',10);         % corner F5
        plot(G4x,G4y,'*b','MarkerSize',10);         % corner G4
        plot(G5x,G5y,'*b','MarkerSize',10);         % corner G5
    end
    %-------------------------------------------------------------------------
    
    if(Drawing)
        x = [A1x E1x];
        y = [A1y E1y];
        plot(x,y, 'b');

        x = [A1x A5x];
        y = [A1y A5y];
        plot(x,y, 'b');

        x = [A5x E5x];
        y = [A5y E5y];
        plot(x,y, 'b');

        x = [E1x E5x];
        y = [E1y E5y];
        plot(x,y, 'b');

        x = [B2x D2x];
        y = [B2y D2y];
        plot(x,y, 'g');

        x = [B2x B4x];
        y = [B2y B4y];
        plot(x,y, 'g');

        x = [B4x D4x];
        y = [B4y D4y];
        plot(x,y, 'g');

        x = [D2x D4x];
        y = [D2y D4y];
        plot(x,y, 'g');

        x = [0 A2x];
        y = [A2y A2y];
        plot(x,y, 'b-.');

        x = [0 A4x];
        y = [A4y A4y];
        plot(x,y, 'b-.');

        x = [E2x 1280];
        y = [E2y E2y];
        plot(x,y, 'b-.');

        x = [E4x 1280];
        y = [E4y E4y];
        plot(x,y, 'b-.');

        x = [B1x B1x];
        y = [0 B1y];
        plot(x,y, 'b-.');

        x = [D1x D1x];
        y = [0 D1y];
        plot(x,y, 'b-.');

        x = [B5x B5x];
        y = [720 B5y];
        plot(x,y, 'b-.');

        x = [D5x D5x];
        y = [720 D5y];
        plot(x,y, 'b-.');

        x = [F6x G6x];
        y = [F6y G6y];
        plot(x,y, 'r');

        x = [F6x F7x];
        y = [F6y F7y];
        plot(x,y, 'r');

        x = [G7x F7x];
        y = [G7y F7y];
        plot(x,y, 'r');

        x = [G6x G7x];
        y = [G6y G7y];
        plot(x,y, 'r');

        x = [B7x A7x];
        y = [B7y A7y];
        plot(x,y, 'b-.');

        x = [B6x A6x];
        y = [B6y A6y];
        plot(x,y, 'b-.');

        x = [F1x F2x];
        y = [F1y F2y];
        plot(x,y, 'b-.');

        x = [G1x G2x];
        y = [G1y G2y];
        plot(x,y, 'b-.');

        x = [D6x E6x];
        y = [D6y E6y];
        plot(x,y, 'b-.');

        x = [D7x E7x];
        y = [D7y E7y];
        plot(x,y, 'b-.');

        x = [F4x F5x];
        y = [F4y F5y];
        plot(x,y, 'b-.');

        x = [G4x G5x];
        y = [G4y G5y];
        plot(x,y, 'b-.');
    end


% text(53,23,['EXTREME PERIPHERY'], 'FontSize', 15, 'color', [0 0 1]);
% text(376,166,['PERIPHERY'], 'FontSize', 15, 'color', [0 0 1]);
% text(486,244,['CENTER'], 'FontSize', 15, 'color', [0 1 0]);
% text(534,310,['CENTER-CENTER'], 'FontSize', 15, 'color', [1 0 0]);
% 
% text(194,126,['upper right'], 'FontSize', 15, 'color', [0 0 1]);
% text(194,346,['right'], 'FontSize', 15, 'color', [0 0 1]);
% text(194,574,['lower right'], 'FontSize', 15, 'color', [0 0 1]);
% 
% text(1153,126,['upper left'], 'FontSize', 15, 'color', [0 0 1]);
% text(1153,346,['left'], 'FontSize', 15, 'color', [0 0 1]);
% text(1153,574,['lower left'], 'FontSize', 15, 'color', [0 0 1]);
% 
% text(586,25,['upper'], 'FontSize', 15, 'color', [0 0 1]);
% text(586,666,['lower'], 'FontSize', 15, 'color', [0 0 1]);
%-----------------------------------------------------------------------
% check hand positions

    %-------------------------------------------------------------------%
    % Right Hand
    %-------------------------------------------------------------------%    
    RightX = (DataArr(i,15));
    RightY = (DataArr(i,16));
    
    if(Drawing)
        plot(RightX,RightY, 'or','MarkerSize',10);
    end

    if(RightX >= F6x && RightX <= G7x) && (RightY >= F6y && RightY <=G7y)                                                                           % Center-center OK = 1
        GestureSpaceTempRight = [GestureSpaceTempRight; 1];           
    elseif(RightX >=B2x && RightX <= D4x) && (RightY>= B2y && RightY <=D4y)                                                                         % Center       OK = 2
        GestureSpaceTempRight = [GestureSpaceTempRight; 2];
	%% sub sectors periphery
	elseif(RightX > A1x && RightX <= F2x) && (RightY>=A1y) && (RightY<=F2y) || (RightX > A2x && RightX <= B6x) && (RightY>=A2y) && (RightY<=B6y)
	GestureSpaceTempRight = [GestureSpaceTempRight; 11];                                                                                            % upper right           
	elseif(RightX > A6x && RightX <= B7x) && (RightY>=A6y) && (RightY<=B7y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 12];                                                                                        % right           
	elseif(RightX > A7x && RightX <= B4x) && (RightY>=A7y) && (RightY<=B4y) || (RightX > A4x && RightX <= F5x) && (RightY>=A4y) && (RightY<=F5y	)
		GestureSpaceTempRight = [GestureSpaceTempRight; 13];                                                                                        % lower right           
	elseif(RightX > F4x && RightX <= G5x) && (RightY>=F4y) && (RightY<=G5y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 14];                                                                                        % lower
	elseif(RightX > G4x && RightX <= E5x) && (RightY>=G4y) && (RightY<=E5y) || (RightX > D7x && RightX <= E4x) && (RightY>=D7y) && (RightY<=E4y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 15];                                                                                        % lower left           
	elseif(RightX > D6x && RightX <= E7x) && (RightY>=D6y) && (RightY<=E7y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 16];                                                                                        % left
	elseif(RightX > D2x && RightX <= E6x) && (RightY>=D2y) && (RightY<=E6y) || (RightX > G1x && RightX <= E2x) && (RightY>=G1y) && (RightY<=E2y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 17];                                                                                        % upper left           
	elseif(RightX > F1x && RightX <= G2x) && (RightY>=F1y) && (RightY<=G2y)
		GestureSpaceTempRight = [GestureSpaceTempRight; 18];                                                                                        % upper    		
        
    %% sub sectors extreme periphery
    elseif(RightX > 0 && RightX <= B1x) && (RightY>=0) && (RightY<=B1y) || (RightX > 0 && RightX <= A2x) && (RightY>=A1y) && (RightY<=A2y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 21];                                                                                        % upper right           
    elseif(RightX > 0 && RightX <= A4x) && (RightY>=A2y) && (RightY<=A4y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 22];                                                                                        % right           
    elseif(RightX > 0 && RightX <= A5x) && (RightY>=A4y) && (RightY<=A5y) || (RightX > 0 && RightX <= B5x) && (RightY>=A5y) && (RightY<=720)
        GestureSpaceTempRight = [GestureSpaceTempRight; 23];                                                                                        % lower right           
    elseif(RightX > B5x && RightX <= D5x) && (RightY>=D5y) && (RightY<=720)
        GestureSpaceTempRight = [GestureSpaceTempRight; 24];                                                                                        % lower
    elseif(RightX > D5x && RightX <= 1280) && (RightY>=D5y) && (RightY<=720) || (RightX > E4x && RightX <= 1280) && (RightY>=E4y) && (RightY<=E5y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 25];                                                                                        % lower left           
    elseif(RightX > E2x && RightX <= 1280) && (RightY>=E2y) && (RightY<=E4y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 26];                                                                                        % left
    elseif(RightX > E1x && RightX <= 1280) && (RightY>=E1y) && (RightY<=E2y) || (RightX > D1x && RightX <= 1280) && (RightY>=0) && (RightY<=E1y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 27];                                                                                        % upper left           
    elseif(RightX > B1x && RightX <= D1x) && (RightY>=0) && (RightY<=D1y)
        GestureSpaceTempRight = [GestureSpaceTempRight; 28];                                                                                        % upper
    end    
       
    %-------------------------------------------------------------------%
    % Left Hand
    %-------------------------------------------------------------------%        
    LeftX = (DataArr(i,17));
    LeftY = (DataArr(i,18));   
    
    if(Drawing)
        plot(LeftX,LeftY, 'og','MarkerSize',10);
    end
    
    if(LeftX >= F6x && LeftX <= G7x) && (LeftY >= F6y && LeftY <=G7y)                                                                           % Center-center OK = 1
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 1];           
    elseif(LeftX >=B2x && LeftX <= D4x) && (LeftY>= B2y && LeftY <=D4y)                                                                         % CenterX       OK = 2
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 2];   
	%% sub sectors periphery
	elseif(LeftX > A1x && LeftX <= F2x) && (LeftY>=A1y) && (LeftY<=F2y) || (LeftX > A2x && LeftX <= B6x) && (LeftY>=A2y) && (LeftY<=B6y)
	GestureSpaceTempLeft = [GestureSpaceTempLeft; 11];                                                                                        % upper right           
	elseif(LeftX > A6x && LeftX <= B7x) && (LeftY>=A6y) && (LeftY<=B7y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 12];                                                                                % right           
	elseif(LeftX > A7x && LeftX <= B4x) && (LeftY>=A7y) && (LeftY<=B4y) || (LeftX > A4x && LeftX <= F5x) && (LeftY>=A4y) && (LeftY<=F5y	)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 13];                                                                                % lower right           
	elseif(LeftX > F4x && LeftX <= G5x) && (LeftY>=F4y) && (LeftY<=G5y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 14];                                                                                % lower
	elseif(LeftX > G4x && LeftX <= E5x) && (LeftY>=G4y) && (LeftY<=E5y) || (LeftX > D7x && LeftX <= E4x) && (LeftY>=D7y) && (LeftY<=E4y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 15];                                                                                % lower left           
	elseif(LeftX > D6x && LeftX <= E7x) && (LeftY>=D6y) && (LeftY<=E7y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 16];                                                                                % left
	elseif(LeftX > D2x && LeftX <= E6x) && (LeftY>=D2y) && (LeftY<=E6y) || (LeftX > G1x && LeftX <= E2x) && (LeftY>=G1y) && (LeftY<=E2y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 17];                                                                                % upper left           
	elseif(LeftX > F1x && LeftX <= G2x) && (LeftY>=F1y) && (LeftY<=G2y)
		GestureSpaceTempLeft = [GestureSpaceTempLeft; 18];       
     
    	    %% sub sectors extreme periphery
     elseif(LeftX > 0 && LeftX <= B1x) && (LeftY>=0) && (LeftY<=B1y) || (LeftX > 0 && LeftX <= A2x) && (LeftY>=A1y) && (LeftY<=A2y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 21];                                                                                        % upper right           
    elseif(LeftX > 0 && LeftX <= A4x) && (LeftY>=A2y) && (LeftY<=A4y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 22];                                                                                        % right           
    elseif(LeftX > 0 && LeftX <= A5x) && (LeftY>=A4y) && (LeftY<=A5y) || (LeftX > 0 && LeftX <= B5x) && (LeftY>=A5y) && (LeftY<=720)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 23];                                                                                        % lower right           
    elseif(LeftX > B5x && LeftX <= D5x) && (LeftY>=D5y) && (LeftY<=720)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 24];                                                                                        % lower
    elseif(LeftX > D5x && LeftX <= 1280) && (LeftY>=D5y) && (LeftY<=720) || (LeftX > E4x && LeftX <= 1280) && (LeftY>=E4y) && (LeftY<=E5y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 25];                                                                                        % lower left           
    elseif(LeftX > E2x && LeftX <= 1280) && (LeftY>=E2y) && (LeftY<=E4y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 26];                                                                                        % left
    elseif(LeftX > E1x && LeftX <= 1280) && (LeftY>=E1y) && (LeftY<=E2y) || (LeftX > D1x && LeftX <= 1280) && (LeftY>=0) && (LeftY<=E1y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 27];                                                                                        % upper left           
    elseif(LeftX > B1x && LeftX <= D1x) && (LeftY>=0) && (LeftY<=D1y)
        GestureSpaceTempLeft = [GestureSpaceTempLeft; 28];                                                                                        % upper
    end    

%-----------------------------------------------------------------------------------
    if(Save)
    f = getframe(gca);
    im = frame2im(f);
        
    title = ['GestSpaceFig/' sprintf('%05d', i) '.jpg']
    imwrite(im,title);    
   end
end   

GestureSpace =  [GestureSpaceTempRight GestureSpaceTempLeft];
title = ['GestureSpace.txt'];
dlmwrite(title,GestureSpace,'delimiter',' ');

end
