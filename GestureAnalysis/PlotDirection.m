% output of this function: a .txt file with a line for each frame
% each line looks like this: [Direction Right Hand] [Direction Left Hand]
% The possible values:
%   1 = Left
%   2 = Right
%   3 = Up
%   4 = Down
%   5 = No movement

function [Direction] = PlotDirection(DataArr,Path)

Direction = [];

Len = size(DataArr,1);
RightX = (DataArr(:,15));
RightY = (DataArr(:,16));

LeftX = (DataArr(:,17));
LeftY = (DataArr(:,18));

DisplaceLeft = [];
DisplaceRight = [];
DisplaceLeft = [DisplaceLeft; 0 0];
DisplaceRight = [DisplaceRight; 0 0];
for i = 1:Len-1   
    displaceRight_X = RightX(i,1)-RightX(i+1,1);
    displaceRight_Y = RightY(i,1)-RightY(i+1,1);
    
    displaceLeft_X = LeftX(i,1)-LeftX(i+1,1);
    displaceLeft_Y = LeftY(i,1)-LeftY(i+1,1);
    
    DisplaceLeft = [DisplaceLeft; displaceLeft_X displaceLeft_Y];
    DisplaceRight = [DisplaceRight; displaceRight_X displaceRight_Y];
end

v = ones(5,1)/5;
DisplaceLeft(:,3) = conv(DisplaceLeft(:,1),v,'same');
DisplaceLeft(:,4) = conv(DisplaceLeft(:,2),v,'same');

DisplaceRight(:,3) = conv(DisplaceRight(:,1),v,'same');
DisplaceRight(:,4) = conv(DisplaceRight(:,2),v,'same');

Len = size(DisplaceLeft(:,1));
DirectionTH = 5;
for i=1:Len
    if(abs(DisplaceLeft(i,3))>=abs(DisplaceLeft(i,4)))   %change in X direction is larger than Y direction --> left/right
        if(abs(DisplaceLeft(i,3))<DirectionTH)
            DisplaceLeft(i,5) = 5;
        elseif(DisplaceLeft(i,3))>=0
           DisplaceLeft(i,5)=1;
       else
           DisplaceLeft(i,5)=2;
       end
    else                                                %change in y direction is larger than X direction --> up/down
         if(abs(DisplaceLeft(i,4))<DirectionTH)
             DisplaceLeft(i,5) = 5;             
         elseif(DisplaceLeft(i,4))>0
           DisplaceLeft(i,5)=3;
       else
           DisplaceLeft(i,5)=4;
       end
    end        
    
    if(abs(DisplaceRight(i,3))>=abs(DisplaceRight(i,4)))   %change in X direction is larger than Y direction --> left/right
        if(abs(DisplaceRight(i,3))<DirectionTH)
            DisplaceRight(i,5) = 5; % No movement      
        elseif(DisplaceRight(i,3))>=0
           DisplaceRight(i,5)=1;    % Movement to the left
       else
           DisplaceRight(i,5)=2;    % movement to the right
       end
    else                                                %change in y direction is larger than X direction --> up/down
        if(abs(DisplaceRight(i,4))<DirectionTH)
             DisplaceRight(i,5) = 5;          
        elseif(DisplaceRight(i,4))>0
           DisplaceRight(i,5)=3;
       else
           DisplaceRight(i,5)=4;
       end
    end  
end

GestureDir =  [DisplaceRight(:,5) DisplaceLeft(:,5)];
title = ['GestureDirection.txt'];
dlmwrite(title,GestureDir,'delimiter',' ');

end
