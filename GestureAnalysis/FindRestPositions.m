function [RestLeft, RestRight, SigmaLeft, SigmaRight] = FindRestPositions(RightHand,LeftHand)
%FindRestPositions
%This function tries to find the rest position of both left and right hand
%The rest position can be seen as the position where the hands are the most
%frequent

RestLeft = [];
RestRight = [];
SigmaLeft = [];
SigmaRight = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RIGHT HAND %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%find min and max positions of the right hand
Xmax = abs(max(RightHand(:,1)));
Ymax = abs(max(RightHand(:,2)));
Xmin = abs(min(RightHand(:,1)));
Ymin = abs(min(RightHand(:,2))); 

%find the range of the right hand in both x and y direction
RangeX = round(Xmax + Xmin + 10);
RangeY = round(Ymax + Ymin + 1);

%Create an empty map that has the size of the X and Y range
heatmap = zeros(RangeY, RangeX); 

%Find minima in Right-Array in order to calculate the offset
Xmin = abs(min(RightHand(:,1)));
Ymin = abs(min(RightHand(:,2))); 

% remove the offset on each hand position
RightHand2(:, 1) = RightHand(:,1) + Xmin+1;
RightHand2(:, 2) = RightHand(:,2) + Ymin+1;

% mark each position of the right hand on the heat map 
for i = 1:size(RightHand2,1)     
     heatmap(RightHand2(i,2),RightHand2(i,1))=heatmap(RightHand2(i,2),RightHand2(i,1))+1;     
end

% flip the map over the X-axis in order to make it human readable + show the map
heatmap = flipud(heatmap);
figure(4)
imshow(heatmap);

%Smooth map using a gaussian filter + flip again over the x-axis
radius=30;
gauss=fspecial('gaussian', [radius+1 radius+1], radius/6);
heatmap_smoothed=filter2(gauss,heatmap);
heatmap_smoothed = flipud(heatmap_smoothed);

% show a coloured heat map
cmap = colormap(jet);
heatmap_colored = grs2rgb(mat2gray(heatmap_smoothed)+0.01,cmap);
figure(5)
imshow(heatmap_colored)

%find location of local maxima
[maximum, loc] = max(heatmap_smoothed(:));
[y x] = ind2sub(size(heatmap_smoothed),loc);

y = y-Ymin-1;
x = x-Xmin-1;
RestRight = [RestRight; x y]

%find sigma
maximum = maximum/2;
[rows,cols,vals] = find(heatmap_smoothed >maximum);
minRow = min(rows)
maxRow = max(rows)
maxCol = max(cols)
minCol = min(cols)

 figure(6)
 imshow(heatmap_colored);
 hold on
 plot(minCol,minRow,'*k','MarkerSize',10);
 plot(maxCol,maxRow,'*k','MarkerSize',10);
%SigmaRight = sqrt((minCol-maxCol)^2 + (minRow-maxRow)^2)
SigmaRight_col = sqrt((minCol-maxCol)^2)
SigmaRight_row = sqrt((minRow-maxRow)^2)
SigmaRight = [SigmaRight; SigmaRight_col SigmaRight_row];
%SigmaRight = min(SigmaRight_col,SigmaRight_row);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LEFT HAND %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%find min and max positions of the left hand
Xmax = abs(max(LeftHand(:,1)));
Ymax = abs(max(LeftHand(:,2)));
Xmin = abs(min(LeftHand(:,1)));
Ymin = abs(min(LeftHand(:,2))); 

%find the range of the left hand in both x and y direction
RangeX = Xmax + Xmin + 1;
RangeY = Ymax + Ymin + 1;

%Create an empty map that has the size of the X and Y range
heatmap = zeros(RangeY, RangeX); 

%Find minima in Right-Array in order to calculate the offset
Xmin = abs(min(LeftHand(:,1)));
Ymin = abs(min(LeftHand(:,2))); 

% remove the offset on each hand position
LeftHand2(:, 1) = LeftHand(:,1) + Xmin+1;
LeftHand2(:, 2) = LeftHand(:,2) + Ymin+1;

% mark each position of the right hand on the heat map 
for i = 1:size(LeftHand2,1)     
     heatmap(LeftHand2(i,2),LeftHand2(i,1))=heatmap(LeftHand2(i,2),LeftHand2(i,1))+1;     
end

% flip the map over the X-axis in order to make it human readable + show the map
heatmap = flipud(heatmap);
figure(7)
imshow(heatmap);

%Smooth map using a gaussian filter + flip again over the x-axis
radius=30;
gauss=fspecial('gaussian', [radius+1 radius+1], radius/6);
heatmap_smoothed=filter2(gauss,heatmap);
heatmap_smoothed = flipud(heatmap_smoothed);

% show a coloured heat map
cmap = colormap(jet);
heatmap_colored = grs2rgb(mat2gray(heatmap_smoothed)+0.01,cmap);
figure(8)
imshow(heatmap_colored)

%find location of local maxima
[loc, loc] = max(heatmap_smoothed(:));
[y x] = ind2sub(size(heatmap_smoothed),loc);

x_max = x;
y_max = y;

y = y-Ymin-1;
x = x-Xmin-1;
RestLeft = [RestLeft; x y]

%find sigma
maximum = maximum/2;
[rows,cols,vals] = find(heatmap_smoothed >maximum);
minRow = min(rows)
maxRow = max(rows)
maxCol = max(cols)
minCol = min(cols)

 figure(9)
 imshow(heatmap_colored);
 hold on
 plot(minCol,minRow,'*k','MarkerSize',10);
 plot(maxCol,maxRow,'*k','MarkerSize',10);
%plot(x_max,y_max,'*k','MarkerSize',10); 

 
%SigmaLeft = sqrt((minCol-maxCol)^2 + (minRow-maxRow)^2);
SigmaLeft_col = sqrt((minCol-maxCol)^2);
SigmaLeft_row = sqrt((minRow-maxRow)^2);
SigmaLeft = [SigmaLeft; SigmaLeft_col SigmaLeft_row];
%SigmaLeft = min(SigmaLeft_row,SigmaLeft_col);
end

