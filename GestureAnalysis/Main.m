clear all;
close all;

% Path to XML file containing person / face / hand detections
%Data = xmlread('/home/sdb/Desktop/temp/FinalDet.xml');
Data = xmlread('/media/sdb/DATAPART2/SaGA/SaGA-V07/FinalDet13000.xml');

% Path to a folder containing the frames 
%Path = '/home/sdb/Desktop/temp/';
Path = '/media/sdb/DATAPART2/SaGA/SaGA-V07/frames';

% frame extension
Ext = '.png';

% Optimal TH factor that is used to weight the sigma values
TH=0.9;

%--------------------------------------------------------------------------
%  load an XML file containing the person, face and hand detections that
%  was created using our hand detection. In order to save time, you could store
%  the data file into a variable called s.mat once it is loaded. Make sure
%  that you remove existing s.mat variable when starting processing a new
%  recording.

try
    load('s.mat');
catch
    [ s ] = xml2struct(Data);
    save('s.mat', 's');
end
%-------------------------------------------------------------------------

% Interpret the XML data 
[DataArr,Len,Nr] = convertData(Data,s, Path);

% Plot all the hand postions:
% left hand positions are drawn in green
% right hand positions are drawn in red
figure(1);
hold on;
plot(0,0,'.b','MarkerSize',10);        
plot(DataArr(:,2),DataArr(:,3),'.g','MarkerSize',10); %Left
plot(DataArr(:,4),DataArr(:,5),'.r','MarkerSize',10); %Right
hold off;

LeftHand = [];
RightHand = [];
LeftHand = [LeftHand;DataArr(:,2) DataArr(:,3)];
RightHand = [RightHand;DataArr(:,4) DataArr(:,5)];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate rest positions
[RestLeft, RestRight, SigmaLeft, SigmaRight] = FindRestPositions(RightHand,LeftHand);

SigmaLeft = TH*SigmaLeft;
SigmaRight = TH*SigmaRight;

figure(1)
hold on
plot(RestRight(1,1),RestRight(1,2),'*k','MarkerSize',10);
plot(RestLeft(1,1),RestLeft(1,2),'*k','MarkerSize',10);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%calculate distance between each hand position and the respective position
DeltaRight = []
DeltaLeft = [];

gestureFlagLeft = [];
gestureFlagRight = [];

Distances = [];

% calculate displacement of each hand w.r.t rest position of both left and
% right hand
% if the displacement of either left or right hand is larger than the
% threshold (Sigma), set the correspondig flag in the gestureflag array
for i=1:Len     % start at index 2 --> deltaX = Xt - X(t-1)    
    %D_Left = sqrt((LeftHand(i,2)-RestLeft(1,2))^2 + (LeftHand(i,1)-RestLeft(1,1))^2);    
    %DeltaLeft = [DeltaLeft; D_Left];
    D_LeftRow = sqrt((LeftHand(i,2)-RestLeft(1,2))^2);    %Y
    D_LeftCol = sqrt((LeftHand(i,1)-RestLeft(1,1))^2);    %X
    distLeft = sqrt(D_LeftRow^2+D_LeftCol^2);
    DeltaLeft = [DeltaLeft; D_LeftCol D_LeftRow];    

    %D_Right = sqrt((RightHand(i,2)-RestRight(1,2))^2 + (RightHand(i,1)-RestRight(1,1))^2);       
    %DeltaRight = [DeltaRight; D_Right];
    D_RightRow = sqrt((RightHand(i,2)-RestRight(1,2))^2);    %Y
    D_RightCol = sqrt((RightHand(i,1)-RestRight(1,1))^2);    %X
    distRight = sqrt(D_RightRow^2+D_RightCol^2);
    
    Distances = [Distances; distRight distLeft];
    DeltaRight = [DeltaRight; D_RightCol D_RightRow];    
end

%smooth the results in order to remove spikes and gaps
%DeltaRight = smooth(DeltaRight);
%DeltaLeft = smooth(DeltaLeft);
for i=1:Len    
    %if(DeltaLeft(i)>SigmaLeft)
    if((DeltaLeft(i,1)>SigmaLeft(1,1)*TH) || (DeltaLeft(i,2)>SigmaLeft(1,2)*TH))
          flagLeft = 1;
     else
         flagLeft = 0;       
     end
     gestureFlagLeft = [gestureFlagLeft; flagLeft];     
     
     %if((DeltaRight(i,1)>SigmaRight(1,1)*TH) || (DeltaRight(i,2)>SigmaRight(1,2)*TH))
     if((DeltaRight(i,1)>SigmaRight(1,1)*TH))% || (DeltaRight(i,2)>SigmaRight(1,2)*TH))
        flagRight = 1;
     else
         flagRight = 0;       
     end
     gestureFlagRight = [gestureFlagRight; flagRight];    
end

% loop over the gesture flag arrays and identify start and end position of
% each gesture
GesturesPhases = [];
gesturephaseLeft = 0;
gesturephaseRight = 0;
for i=1:Len     % start at index 2 --> deltaX = Xt - X(t-1)    
    if(gestureFlagLeft(i,1)==1 && gesturephaseLeft==0)
       startLeft = i; 
       gesturephaseLeft = 1;
    end   
       
    if(gestureFlagLeft(i,1)==0 && gesturephaseLeft==1)
        stopLeft = i;
        gesturephaseLeft = 0;   
        length = stopLeft - startLeft;
        GesturesPhases = [GesturesPhases;0 startLeft stopLeft length];     % gesture of left hand has index 0
    end
    
    if(gestureFlagRight(i,1)==1 && gesturephaseRight==0)
       startRight = i; 
       gesturephaseRight = 1;
    end   
       
    if(gestureFlagRight(i,1)==0 && gesturephaseRight==1)
        stopRight = i;
        gesturephaseRight = 0;    
        length = stopRight - startRight;
        GesturesPhases = [GesturesPhases;1 startRight stopRight length];   % gesture of right hand has index 1
    end
end

Len2 = size(GesturesPhases,1);
% plot displacement of right hand as bar graph 
% marh displacement threshold (sigma) using a red line
% mark begin and end of each gesture phase using a blue line
figure(2)
hold on;
%title('Displacement of x-position of right hand','fontsize', 12);
bar(DeltaRight(:,1))
%plot sigma Right TH
%x = [0 Len];
%y = [SigmaRight(1,1)*TH SigmaRight(1,1)*TH];
%plot(x,y, 'r','LineWidth',2);
%set(gcf, 'color', [1 1 1]);
%set(gca,'FontSize',10)
%xlabel('Frame number', 'fontsize', 12);
%ylabel('displacement in pixel', 'fontsize', 12);
%axis([0 10639 0 100]);

for i=1:20
    if(GesturesPhases(i,1)==1 & GesturesPhases(i,4) > 1)
        x = [GesturesPhases(i,2) GesturesPhases(i,2)];
        y = [0 300];
        plot(x,y);
        
        x = [GesturesPhases(i,3) GesturesPhases(i,3)];
        plot(x,y);
    end    
end

% plot displacement of left hand as bar graph 
% marh displacement threshold (sigma) using a red line
% mark begin and end of each gesture phase using a blue line
figure(3)
hold on;
%title('Displacement of y-position of left hand''fontsize', 14)
bar(DeltaLeft(:,2))
%plot sigma Left TH
%x = [0 Len];
%y = [SigmaLeft(1,1)*TH SigmaLeft(1,1)*TH];
%plot(x,y,'r');

for i=1:Len2
    if(GesturesPhases(i,1)==0 & GesturesPhases(i,4) > 1)
        x = [GesturesPhases(i,2) GesturesPhases(i,2)];
        y = [0 300];
        plot(x,y);
        
        x = [GesturesPhases(i,3) GesturesPhases(i,3)];
        plot(x,y);
    end    
end

% Write gesture phases to txt file:
title = ['GesturePhases.txt']
dlmwrite(title,GesturesPhases,'delimiter',' ');
dlmwrite('distances.txt',Distances,'delimiter',' ');

% Perform the analysis of the usage of the gesture space
[GestureSpace] = PlotGestureSpace(DataArr,Path);                        % GestureSpace(:,1)   = Right hand
                                                                        % GestureSpace(:,2)   = Left hand                                                                        
% Perform the analysis of the directionality of the gestures                                                                        
[Direction] = PlotDirection(DataArr,Path);

% visualize the gesture segments into a single image per gesture phase
% plotPhases(); 


