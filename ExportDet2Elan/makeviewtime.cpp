#include "makeviewtime.h"

using namespace std;

// find smallest start value in vector of detections
int findMin(vector<DetectionStructure> DetVector)
{
    int min = 99999;
    int minIndex = 99999;
    for(int i=0;i<DetVector.size();i++)
    {
        if(DetVector[i].start < min)
        {
            min = DetVector[i].start;
            minIndex = i;
        }
    }
    return minIndex;
}

//-----------------------------------------------------------------------------------

// write ELAN Header
void WriteHeader(ofstream& File)
{
    File << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
    File << "<ANNOTATION_DOCUMENT AUTHOR=\"\" DATE=\"2015-05-20T14:50:36+01:00\" FORMAT=\"2.8\" VERSION=\"2.8\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"http://www.mpi.nl/tools/elan/EAFv2.8.xsd\">" << endl;
    File << "\t<HEADER MEDIA_FILE=\"\" TIME_UNITS=\"milliseconds\">" << endl;
    File << "\t\t<MEDIA_DESCRIPTOR MEDIA_URL=\"file:///out.mp4\" MIME_TYPE=\"video/*\" RELATIVE_MEDIA_URL=\"./out.mp4\"/>" << endl;
    File << "\t\t<PROPERTY NAME=\"URN\">urn:nl-mpi-tools-elan-eaf:ebe089d9-07d9-46a7-bf4c-08745ea6652f</PROPERTY>" << endl;
    File << "\t\t<PROPERTY NAME=\"lastUsedAnnotationId\">4</PROPERTY>" << endl;
    File << "\t</HEADER>" << endl;
}

//-----------------------------------------------------------------------------------

// write ELAN TIMESLOTS
void WriteTimeSlots(ofstream& File, vector<TimeSlot> TimeSlots)
{
    File << "\t<TIME_ORDER>" << endl;

    for(int i=0;i<TimeSlots.size();i++)
    {
         int value = (int)TimeSlots[i].value;
         File << "\t\t<TIME_SLOT TIME_SLOT_ID=\"ts" << TimeSlots[i].id+1 <<"\" TIME_VALUE=\"" << value << "\"/>" << endl;
    }

    File << "\t</TIME_ORDER>" << endl;
}

//-----------------------------------------------------------------------------------
// write ELAN TIER
void WriteTiers(ofstream& File, vector<Annotation> Annotations, string tierName)
{
    File << "\t<TIER LINGUISTIC_TYPE_REF=\"default-lt\" TIER_ID=\"default\"/>" << endl;
    for(int i=0;i<Annotations.size();i++)
    {
        File << "\t<TIER ANNOTATOR=\"Stijn\" LINGUISTIC_TYPE_REF=\"default-lt\" PARTICIPANT=\"Raphael\" TIER_ID=\"" << tierName << "\">" << endl;
        File <<"\t\t<ANNOTATION>"<<endl;
        File << "\t\t\t<ALIGNABLE_ANNOTATION ANNOTATION_ID=\" a" << i<< "\" TIME_SLOT_REF1=\"ts" << Annotations[i].StartTimeSlot+1 << "\" TIME_SLOT_REF2=\"ts" << Annotations[i].EndTimeSlot+1 << "\">" << endl;
        File <<"\t\t\t\t<ANNOTATION_VALUE>" << Annotations[i].annotationClass << "</ANNOTATION_VALUE>"<<endl;
        //File <<"\t\t\t\t<ANNOTATION_VALUE>" << "0" << "</ANNOTATION_VALUE>"<<endl;
        File << "\t\t\t</ALIGNABLE_ANNOTATION>" << endl;
        File << "\t\t</ANNOTATION>" << endl;
        File << "\t</TIER>" <<endl;
    }
}

//-----------------------------------------------------------------------------------
//Write ELAN FOOTER
void WriteFooter(ofstream& File)
{
    File << "\t<LINGUISTIC_TYPE GRAPHIC_REFERENCES=\"false\" LINGUISTIC_TYPE_ID=\"default-lt\" TIME_ALIGNABLE=\"true\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Time subdivision of parent annotation's time interval, no time gaps allowed within this interval\" STEREOTYPE=\"Time_Subdivision\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Symbolic subdivision of a parent annotation. Annotations refering to the same parent are ordered\" STEREOTYPE=\"Symbolic_Subdivision\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"1-1 association with a parent annotation\" STEREOTYPE=\"Symbolic_Association\"/>" << endl;
    File << "\t<CONSTRAINT DESCRIPTION=\"Time alignable annotations within the parent annotation's time interval, gaps are allowed\" STEREOTYPE=\"Included_In\"/>" << endl;
    File << "</ANNOTATION_DOCUMENT>" << endl;
}

//-----------------------------------------------------------------------------------
// Find a timeslot that belongs to a certain frame number
// in no valid timeslot was found, return -1
int FindTimeSlot(vector<TimeSlot> TimeVector, int frame)
{
    for(int i=0;i<TimeVector.size();i++)
    {
        if(frame==TimeVector[i].framenr)
            return i;
    }
    return -1;
}

// Find the gesture spaces that belong to a certain gesture phase
vector<DetectionStructure>findDetsInExposure(vector<DetectionStructure> sortVector, vector<DetectionStructure>viewTime)
{
    vector<DetectionStructure>finalDetCounts;
    DetectionStructure detectionItem;
    // find all gesture spaces that belong to the gesture phases
    for(int i=0;i<sortVector.size();i++)
    {
        // find start
        int target = sortVector[i].start;
        int indexStart,indexEnd;
        for(int j=0;j<viewTime.size();j++)
        {
            if(viewTime[j].start <= target && target <viewTime[j].end)
            {
                indexStart = j;
            }
        }

        // find end
        target = sortVector[i].end;
        for(int j=0;j<viewTime.size();j++)
        {
            if(viewTime[j].start <= target && target <viewTime[j].end)
            {
                indexEnd = j;
            }
        }

        if(indexStart ==0 && indexEnd ==0)
            cout << "could not found correct indexes... " << endl;

        // show individual annotation values:
        for(int j=indexStart;j<=indexEnd;j++)
        {
            if(viewTime[j].start < sortVector[i].start)
            {
                detectionItem.start = sortVector[i].start;
            }
            else
            {
                detectionItem.start =viewTime[j].start;
            }

            if(viewTime[j].end > sortVector[i].end)
            {
                detectionItem.end = sortVector[i].end;
            }
            else
            {
                detectionItem.end =viewTime[j].end;
            }

            detectionItem.item =viewTime[j].item;
            detectionItem.length = detectionItem.end - detectionItem.start;
            finalDetCounts.push_back(detectionItem);
        }
    }
    return finalDetCounts;
}

void MakeTimeSlots(vector<DetectionStructure>finalGestureSpace,vector<TimeSlot>& TimeSlots, double frametime)
{
    // get ID of last item in TimeSlot vector
    int id =0;
    if(TimeSlots.size()==0)
        id = 0;
    else
        id = TimeSlots[TimeSlots.size()-1].id+1;

    // timeslots for left gesture space
    for(int i=0;i<finalGestureSpace.size();i++)
    {
        //cout << "gesture space: " << i << ": " << "start = " << finalGestureSpace[i].start << " | end = " << finalGestureSpace[i].end << endl;
        int index = FindTimeSlot(TimeSlots,finalGestureSpace[i].start);

        if(index==-1)
        {
            // add new entry
            TimeSlot TimeEntry;
            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(finalGestureSpace[i].start);

            TimeEntry.framenr = finalGestureSpace[i].start;
            TimeSlots.push_back(TimeEntry);
            id++;
		}

        index = FindTimeSlot(TimeSlots,finalGestureSpace[i].end);
        if(index==-1)
        {
            // add new entry
            TimeSlot TimeEntry;
            TimeEntry.id = id;
            TimeEntry.value = (double)frametime*(finalGestureSpace[i].end);
            TimeEntry.framenr = finalGestureSpace[i].end;
            TimeSlots.push_back(TimeEntry);
            id++;
        }
    }
}

void FileParser(vector<DetectionStructure>&detectionVector,string path)
{
    ifstream viewTime;
    string Line;
    char item[50];
    int start,end,length;

    viewTime.open(path);
    if(!viewTime.is_open())
        return;

    while(!viewTime.eof())
    {
        DetectionStructure det;
        start = 0;
        end = 0;
        length = 0;
        getline(viewTime, Line);
        sscanf(Line.c_str(),"%s%d%d%d",&item,&start,&end,&length);

        det.item = string(item);
        det.start = start;
        det.end = end;
        det.length = length;

        if(start>0&&end>0)
        {
            detectionVector.push_back(det);
        }
    }
}

vector<DetectionStructure> SortFunction(vector<DetectionStructure>DetVector)
{
    vector<DetectionStructure>SortVector;
    while(DetVector.size()>0)
    {
        int minIndex = findMin(DetVector);
        if(SortVector.size()>0)
        {
            if( (SortVector[SortVector.size()-1].item  == DetVector[minIndex].item ) && (DetVector[minIndex].start < SortVector[SortVector.size()-1].end))
            {
                SortVector[SortVector.size()-1].end = DetVector[minIndex].end;
            }
            else
            {
                SortVector.push_back(DetVector[minIndex]);
            }
        }
        else
        {
            SortVector.push_back(DetVector[minIndex]);
        }

        DetVector.erase(DetVector.begin()+minIndex);
    }  
    return SortVector;
}

vector<DetectionStructure> SmoothFunction(vector<DetectionStructure>sortVector, int minTime, int maxGap)
{
    vector<DetectionStructure>smoothVector;
    int newStart= 999999;
    int newEnd = -1;

    for(int i=0;i<sortVector.size();i++)
    {
        if(i<sortVector.size())
        {
            //cout << "smoothing i = " << i << "\tstart = " << sortVector[i].start << "\tend = " << sortVector[i].end << "\titem = " << sortVector[i].item << "\tlength" << sortVector[i].length << endl;
            int jump = (sortVector[i+1].start- sortVector[i].end );
            if(jump<maxGap)
            {
                if(sortVector[i].start < newStart)
                    newStart = sortVector[i].start;
                if(sortVector[i+1].end > newEnd )
                    newEnd = sortVector[i+1].end;
            }
            else
            {
                int finalStart, finalEnd;
                if(newStart!=999999)
                    finalStart = newStart;
                else
                    finalStart = sortVector[i].start;

                if(newEnd>-1)
                    finalEnd = newEnd;
                else
                    finalEnd = sortVector[i].end;

                DetectionStructure Temp;
                Temp.item = sortVector[i].item;
                Temp.start = finalStart;
                Temp.end = finalEnd;
                Temp.length = finalEnd-finalStart;
                if(Temp.length>minTime)
                {
                    smoothVector.push_back(Temp);
                }

                newStart = 999999;
                newEnd = -1;
            }
        }
    }

    /*

    for(int i=0;i<sortVector.size();i++)
    {
        cout << "sorting i = " << i << " start = " << sortVector[i].start << " end = " << sortVector[i].end << " item = " << sortVector[i].item << endl;
        if(i<sortVector.size())
        {
            cout << "\t gap = " << abs(sortVector[i].end - sortVector[i+1].start) << endl;
            if( abs(sortVector[i].end - sortVector[i+1].start)<maxGap && sortVector[i].item == sortVector[i+1].item)
            {                
                int newStart = min( sortVector[i].start, sortVector[i+1].start);
                int newEnd = max( sortVector[i].end, sortVector[i+1].end);
                cout << "new start = " << newStart << " new end  = " << newEnd << endl;
                DetectionStructure Temp;
                Temp.item = sortVector[i].item;
                Temp.start = newStart;
                Temp.end = newEnd;
                Temp.length = newEnd-newStart;
                if(Temp.length>minTime)
                {
                    smoothVector.push_back(Temp);
                }
                i++;
            }
            else
            {
                if(sortVector[i].length>minTime)
                {
                    smoothVector.push_back(sortVector[i]);
                }
            }
        }
    }
    */
    return smoothVector;
}

vector<DetectionStructure> MakeViewTime(vector<NrOfDets> detVector)
{
    vector<DetectionStructure>viewtimeVector;
    DetectionStructure viewItem;
    viewItem.start = detVector[0].frameCounter;
    viewItem.item = detVector[0].detCounter;
    bool set = false;

    // start @ index 1
    for(int i=1;i<detVector.size();i++)
    {
        string nrOfDets = std::to_string(detVector[i].detCounter);
        if(nrOfDets != viewItem.item)
        {
            viewItem.end = detVector[i].frameCounter;
            viewItem.length = viewItem.end - viewItem.start;
            viewtimeVector.push_back(viewItem);
            set = true;

            viewItem.start = detVector[i].frameCounter;
            viewItem.item = nrOfDets;
        }
        else
        {
            set = false;
        }
    }

    if(set==false)
    {
        viewItem.end = detVector[detVector.size()-1].frameCounter;
        viewItem.length = viewItem.end - viewItem.start;
        viewtimeVector.push_back(viewItem);
    }
/*
    for(int i=0;i<viewtimeVector.size();i++)
    {
        cout << "\t" << viewtimeVector[i].start << " " << viewtimeVector[i].end << "\titem = " << viewtimeVector[i].item << "\tsize = " << viewtimeVector[i].length << endl;
    }
*/
    return viewtimeVector;
}

vector<Annotation> MakeAnnoVector(vector<DetectionStructure> inputVector, vector<TimeSlot> TimeSlots)
{
    vector<Annotation> annotationVector;
    for(int i=0;i<inputVector.size();i++)
    {
        int index1 = FindTimeSlot(TimeSlots,inputVector[i].start);
        int index2 = FindTimeSlot(TimeSlots,inputVector[i].end);
        Annotation annotationItem;

        annotationItem.annotationClass = inputVector[i].item;
        annotationItem.StartTimeSlot = index1;
        annotationItem.EndTimeSlot = index2;
        annotationVector.push_back(annotationItem);
    }
    return annotationVector;
}
