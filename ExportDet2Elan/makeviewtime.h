#ifndef MAKEVIEWTIME_H
#define MAKEVIEWTIME_H

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

//-------------------------------------------------------------------------------//
struct DetectionStructure {                          // Detection structure
    string item;
    int start;
    int end;
    int length;
};
//-------------------------------------------------------------------------------//
struct TimeSlot {                           // timeslot structure
    int id;
    double value;
    int framenr;
};
//-------------------------------------------------------------------------------//
struct Annotation {                           // timeslot structure
    string annotationClass;
    int StartTimeSlot;
    int EndTimeSlot;
};
//-----------------------------------------------------------------------------------
struct NrOfDets {
    int frameCounter;
    int detCounter;
};
//-----------------------------------------------------------------------------------

int findMin(vector<DetectionStructure> DetVector);
void WriteHeader(ofstream& File);
void WriteTimeSlots(ofstream& File, vector<TimeSlot> TimeSlots);
void WriteTiers(ofstream& File, vector<Annotation> Annotations, string tierName);
void WriteFooter(ofstream& File);
int FindTimeSlot(vector<TimeSlot> TimeVector, int frame);
vector<DetectionStructure>findDetsInExposure(vector<DetectionStructure> sortVector, vector<DetectionStructure>viewTime);
void MakeTimeSlots(vector<DetectionStructure>finalGestureSpace,vector<TimeSlot>& TimeSlots, double frametime);
void FileParser(vector<DetectionStructure>&detectionVector,string path);
vector<DetectionStructure> SortFunction(vector<DetectionStructure>DetVector);
vector<DetectionStructure> SmoothFunction(vector<DetectionStructure>sortVector, int minTime, int maxGap);
vector<DetectionStructure> MakeViewTime(vector<NrOfDets> detVector);
vector<Annotation> MakeAnnoVector(vector<DetectionStructure> inputVector, vector<TimeSlot> TimeSlots);




#endif // MAKEVIEWTIME_H
