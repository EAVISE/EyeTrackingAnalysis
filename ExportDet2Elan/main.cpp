#include <iostream>
#include <fstream>
#include "makeviewtime.h"

using namespace std;

#define minTime 3
#define maxGap 12

int main(int argc, char* argv[])
{
    cout << "Automatic ELAN-export algorithm including additional annotation information" << endl;
    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " <Path to result folder 1>< Path to result folder 2> < Path to result folder n> < Path to output folder>" << std::endl;
        return -1;
    }

    vector<DetectionStructure> vectorExposure;
    vector<DetectionStructure> vectorAttention;
    vector<DetectionStructure> vectorCounter;

    for(int i=1;i<argc-1;i++)
    {
        cout << "=======================================================" << endl;
        cout << "path = " << argv[i] << endl;
        string basePath = argv[i];
        string exposurePath = basePath;
        string attentionPath = basePath;
        string detCounterPath = basePath;

        exposurePath = exposurePath.append("exposure.txt");
        attentionPath = attentionPath.append("attention.txt");
        detCounterPath = detCounterPath.append("detcounter.txt");

        //1) read detCounter.txt + store number of detections for each frame in a vector of structs
        ifstream NrofDets;
        string Line;
        vector<NrOfDets>detVector;
        NrofDets.open(detCounterPath);

        if(!NrofDets.is_open())
        {
             cout << "detCounter.txt is NOT open" << endl;
             return -1;
        }
        else
		{
			while(!NrofDets.eof())
			{
				NrOfDets detStruct;
				getline(NrofDets, Line);
				sscanf(Line.c_str(),"%d%d",&detStruct.frameCounter, &detStruct.detCounter);
				detVector.push_back(detStruct);
			}
			cout << "detVector size = " << detVector.size() << endl;
		}

        //2) convert detVector to Viewtime vector
        vector<DetectionStructure>ViewTimeDetCount = MakeViewTime(detVector);

        //3) read Viewtime of exposure file + read attention file and store then into the same vector
        vector<DetectionStructure> detectionVectorE;
        vector<DetectionStructure> detectionVectorA;
        FileParser(detectionVectorE,exposurePath);
        FileParser(detectionVectorA,attentionPath);
        cout << "size detectionVector of exposure = " << detectionVectorE.size() << endl;
        cout << "size detectionVector of attention = " << detectionVectorA.size() << endl;

        //3) sort the vector
        vector<DetectionStructure> sortVectorE = SortFunction(detectionVectorE);
        vector<DetectionStructure> sortVectorA = SortFunction(detectionVectorA);

        //4) temporal smoothing
        vector<DetectionStructure> smoothVectorE = SmoothFunction(sortVectorE,minTime,maxGap);
        vector<DetectionStructure> smoothVectorA = SmoothFunction(sortVectorA,minTime,maxGap);

        for(int i=0;i<smoothVectorE.size();i++)
        {
            cout << "smooth  i = " << i << "\t" << smoothVectorE[i].item << "\tstart:" << smoothVectorE[i].start << "\tstop:" << smoothVectorE[i].end << "\tlength = " << smoothVectorE[i].length << endl;
        }

        //5)  find for each exposure segment the corresponding number of exposure items
        vector<DetectionStructure> DetectionCounter = findDetsInExposure(smoothVectorE,ViewTimeDetCount);

        // concatenate with existing vectors
        vectorExposure.insert(vectorExposure.end(),smoothVectorE.begin(),smoothVectorE.end());
        vectorAttention.insert(vectorAttention.end(),smoothVectorA.begin(),smoothVectorA.end());
        vectorCounter.insert(vectorCounter.end(),DetectionCounter.begin(),DetectionCounter.end());
    }

    vector<DetectionStructure> vectorCounter2 = SortFunction(vectorCounter);
 
    //6) make timeslots
    double frametime = ((double)1/30)*1000;
    vector<TimeSlot> TimeSlots;
    MakeTimeSlots(vectorExposure,TimeSlots, frametime);
    MakeTimeSlots(vectorAttention,TimeSlots, frametime);
    MakeTimeSlots(vectorCounter2,TimeSlots, frametime);
    
   
    //7) make annotations vector
    vector<Annotation> Annotations_exposure = MakeAnnoVector(vectorExposure, TimeSlots);
    vector<Annotation> Annotations_attention = MakeAnnoVector(vectorAttention,  TimeSlots);
    vector<Annotation> Annotations_detCounter = MakeAnnoVector(vectorCounter2, TimeSlots);

    cout << "vector exposure = " << Annotations_exposure.size() << endl;
    cout << "vector attention = " << Annotations_attention.size() << endl;
    cout << "vector detcounter = " << Annotations_detCounter.size() << endl;
    
    string annotationClass;
    int StartTimeSlot;
    int EndTimeSlot;
    
    // 8) Write to ELAN file:
    char XML_File[200];
    sprintf(XML_File,"%s%s",argv[argc-1],"/Result.eaf");
    ofstream XML;
    XML.open(XML_File);

    WriteHeader(XML);
    WriteTimeSlots(XML,TimeSlots);
    WriteTiers(XML,Annotations_exposure, "exposure");
    WriteTiers(XML,Annotations_detCounter, "detCount");
    WriteTiers(XML,Annotations_attention, "attention");
    WriteFooter(XML);

    XML.close();

    return 0;
}
