#ifndef PUPILPARSER
#define PUPILPARSER

#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

struct GazeInfo {                          // Detection structure
    int frame;
    float confidence;
    float normX;
    float normY;
};

int ParseCSV(std::string path, std::vector<GazeInfo> &gazeVector);
int findFrame(std::vector<GazeInfo> gazeVector, int frame);



#endif // PUPILPARSER

