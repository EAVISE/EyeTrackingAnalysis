#include "pupilparser.h"


using namespace std;
using namespace cv;

int findFrame(vector<GazeInfo> gazeVector, int frame)
{
    for(int i=0;i<gazeVector.size();i++)
    {
        if(gazeVector[i].frame == frame)
            return i;
    }
    return -1;
}

int ParseCSV(string path, vector<GazeInfo> &gazeVector)
{
    // Open CSV file
    ifstream GazeCoFile;
    GazeCoFile.open(path.c_str());
    cout << "Read gaze co file: " << path << endl;
    if(!GazeCoFile.is_open())
    {
        cout << "could not open gaze co file" << endl;
        return -1;
    }

    // Read the CSV file line-by-line
    string Line;
    int frame;
    int lastframe = -1;
    float confidence,normX,normY,timestamp;

    while(!GazeCoFile.eof())
    {
        getline( GazeCoFile, Line);
        //cout << "line : " << Line << endl;
        std::istringstream s(Line);
        std::string field;
        int lineCounter = 0;
        while (getline(s, field,','))
        {
            std::istringstream ss( field );
            switch(lineCounter) {
            case 0:
                ss >> timestamp;
                break;
            case 1:
                ss >> frame;
                break;
            case 2:
                ss >> confidence;
                break;
            case 3:
                ss >> normX;
                break;
            case 4:
                ss >> normY;
                break;

            default:
                break;
            }

            lineCounter++;
        }
        cout << "-->frame = " << frame << " | confidence = " << confidence << " | normX = " << normX << " | normY = " << normY << endl;
        if(frame==lastframe)
        {
            // average results
            gazeVector.back().normX = (gazeVector.back().normX+normX)/2;
            gazeVector.back().normY = (gazeVector.back().normY+normY)/2;
        }
        else
        {
            // make new entry
            GazeInfo newEntry;
            newEntry.frame = frame;
            newEntry.confidence = confidence;
            newEntry.normX = normX;
            newEntry.normY = normY;
            gazeVector.push_back(newEntry);
        }
        lastframe = frame;
    }


}
