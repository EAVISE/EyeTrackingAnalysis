#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include <iostream>
#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

#include "pupilparser.h"

using namespace std;
using namespace cv;

#define Write

int getdir (string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp = opendir(dir.c_str())) == NULL)
    {
        cerr << "unable to open directory" << endl;
    return -1;
    }

    while ((dirp = readdir(dp)) != NULL)
    {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc < 4) {
            // Tell the user how to run the program
            std::cerr << "Usage: " << argv[0] << " <Path to gazepositions.csv file> <Path to frames folder> <Path to output folder> " << std::endl;
            return -1;
    	}    

    string CSV_Path;
    CSV_Path = argv[1];

    //open gazepositions.csv file (in export dir of the binocular pupil recording)
    vector<GazeInfo> gazeVector;
    int ret = ParseCSV(CSV_Path, gazeVector );
    cout <<" lengt of gazeVector = " << gazeVector.size() << endl;

    // loop over all frames in frame folder and plot (if available the gaze data)
    string FrameFolder;
    FrameFolder = argv[2];
    
    // output folder
    string outputFolder;
    outputFolder = argv[3];

    vector<string> files = vector<string>();
    getdir(FrameFolder,files);
    for (unsigned int i = 0;i < files.size();i++)
    {        
		// remove file type (.jpg or .png)
		files[i].erase(files[i].size() - 4);

		istringstream buffer(files[i]);
		int value;
		buffer >> value;
		cout << "--> " << value << endl;

		// try to open this image
		Mat image;
		char ImagePath[200];
		sprintf(ImagePath, "%s%05d%s", FrameFolder.c_str(), value, ".png");
		cout << "path = " << ImagePath << endl;
		image = imread(ImagePath);

		if(image.empty())
			cerr << "unable to open image: " << ImagePath << endl;
		else
		{
			// find gaze info for this frame
			int vectorIndex = findFrame(gazeVector,value);

			if(vectorIndex !=-1)    // plot gaze cursor
			{
				float normX = gazeVector[vectorIndex].normX;
				float normY = gazeVector[vectorIndex].normY;

				Point P1;
				P1.x = image.cols*normX;
				P1.y = image.rows*(1-normY);

				if(P1.x<0)
					P1.x = 0;

				if(P1.y < 0)
					P1.y = 0;

				if(P1.x>image.cols)
					P1.x = image.cols;

				if(P1.y>image.rows)
					P1.y = image.rows;

				circle(image,P1,20,Scalar(0,0,255),-1);
			}				
			#ifdef Show
			imshow("window",image);
			waitKey();
			#endif
			
			#ifdef Write				
			char outputpath[200];
			sprintf(outputpath, "%s%05d%s", outputFolder.c_str(),value,".png");
			imwrite(outputpath,image);
			#endif
		}        
	}
}
