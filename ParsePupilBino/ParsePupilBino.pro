TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

LIBS += `pkg-config opencv --libs`		# so files

INCLUDEPATH += -I "/usr/local/include/opencv2" 	# headers

TARGET = ParsePupilBino

SOURCES += main.cpp \
    pupilparser.cpp

HEADERS += \
    pupilparser.h

