TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle

LIBS += `pkg-config opencv --libs`		# so files

INCLUDEPATH += -I "/usr/local/include/opencv2" 	# headers

TARGET = ParseTobii

SOURCES += main.cpp \
    Tobiiparser.cpp

HEADERS += \
    Tobiiparser.h \

