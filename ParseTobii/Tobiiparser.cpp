#include "Tobiiparser.h"
#include "QString"
#include <boost/algorithm/string.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace cv;
using boost::lexical_cast;

int convert2Int(string input)
{
	string tempS = "";
	int value = -1;
	for(std::string::size_type i = 0; i < input.size(); ++i) 
	{	
		if(i%2!=0)
		{
			tempS.push_back(input[i]);							
		}
	}	 
	value = atoi(tempS.c_str());	
	return value;
}

float convert2Float(string input)
{
	string tempS = "";
	float value = -1;
	for(std::string::size_type i = 0; i < input.size(); ++i) 
	{	
		if(i%2!=0)
		{
			tempS.push_back(input[i]);							
		}
	}	 
	value = atof(tempS.c_str());	
	return value;
}


int findTimestamp(float timeCounter, std::vector<GazeInfo> gazeVector)
{
    float minDist = 9999;
    int minIndex = -1;
    for(int i=0;i<gazeVector.size();i++)
    {
        float dist=abs(timeCounter - gazeVector[i].timestamp);
        //cout << "dist = " << dist << endl;
        if(dist<minDist)
        {
            minDist = dist;
            minIndex = i;
        }
    }
    return minIndex;
}


// This functions parses the actual TSV file. The first argument contians the path to the TSV file, the second argument
// contains a pointer to a datastructure in which the gaze and timestamp data is stored
int ParseTSV(string path, vector<GazeInfo> &gazeVector)
{
    // Open TSV file
    ifstream GazeCoFile;
    GazeCoFile.open(path.c_str());
    cout << "Read gaze co file: " << path << endl;
    if(!GazeCoFile.is_open())
    {
        cout << "could not open gaze co file" << endl;
        return -1;
    }

    // Read the TSV file line-by-line. Each line contains information about a frame that was captured, however, not all frames contain valid gaze data!
    // The following fields of each line are relevant:
        // field 8 = timestamp value
        // field 9 = x-position of gaze cursor
        // field 10 = y-position of gaze cursor
    // once a field was interpret, an addition convert function was required since the data into the string is stored in a strange way:
    // e.g. temp = [ 3 3 3 ] --> example of how value 333 is stored into the string, thanks to our convert function convert2Float and convert2Int,
    // we are able to transform these structures into actual values.
    string Line;       
    float timestamp;
    string temp;
	int normX,normY;	
	
	while(!GazeCoFile.eof())
    {
	    getline( GazeCoFile, Line);
		std::istringstream s(Line);
		std::string field;
    	int lineCounter = 0;    	
    	normX = -1;
   		normY = -1;
        while (getline(s, field,'\t'))
       	{    	
    		if(field.size()>0)
            {
                std::istringstream ss(field);                
                
	    		switch(lineCounter)
    	        {
				case 8:									
					ss >> temp;		
					timestamp = convert2Float(temp);					
                	break;	            
	            case 9:
    	            ss >> temp;
					normX = convert2Int(temp);					
                    break;
            	case 10:
                	ss >> temp;
					normY = convert2Int(temp);					
                	break;
	            default:
    	            break;
                }                
            }
            lineCounter++;            
        }    
        //cout << "timestamp = " << timestamp << " normX = " << normX << " normY = " << normY << endl;
        if(normX !=-1 && normY!=-1)
        {
            // make new entry
            GazeInfo newEntry;
            newEntry.timestamp = timestamp;
            newEntry.normX = normX;
            newEntry.normY = normY;
            gazeVector.push_back(newEntry);
        }
	}
}
