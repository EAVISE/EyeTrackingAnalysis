#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include <iostream>
#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

#include "Tobiiparser.h"

using namespace std;
using namespace cv;

//#define Write
#define Show

int main(int argc, char* argv[])
{
    if (argc < 3) {
            // Tell the user how to run the program
            std::cerr << "Usage: " << argv[0] << " <Path to gazepositions.csv file> <Path to frames folder> <Path to output folder> " << std::endl;
            return -1;
    	}    

    // standard framerate of the Tobii Glasses2 eye-trackers
    int fps = 25;

    string TSV_Path;
    TSV_Path = argv[1];

    //open gazepositions.csv file (in export dir of the binocular pupil recording)
    vector<GazeInfo> gazeVector;
    int ret = ParseCSV(TSV_Path, gazeVector );
    cout <<" lengt of gazeVector = " << gazeVector.size() << endl;

    // loop over all frames in frame folder and plot (if available the gaze data)
    string FrameFolder;
    FrameFolder = argv[2];

    // output folder
    string outputFolder;
    outputFolder = argv[3];

    // define that time per frame in milliseconds, since this is used in the timestamp
    // in case of the Tobii glasses2, the framerate is set to 25
    float timePerFrame = float(1000/fps);
    cout << " time per frame = " << timePerFrame << endl;

    // create a window to show the image
    namedWindow("window", WINDOW_AUTOSIZE);

    // in this snippet, we just run over the first 1000 frames of a recording
    for (unsigned int i = 0;i <1000;i++)
    {       
        Mat image;
        char ImagePath[200];
        sprintf(ImagePath, "%s%05d%s", FrameFolder.c_str(), i, ".png");

        // try opening this image:
        image = imread(ImagePath);

        if(image.empty())
            cerr << "unable to open image: " << ImagePath << endl;
        else
        {
            // find corresponding gaze data for this frame, using the timestamp information
            // We convert the framenumber into a timestamp value by multiplying the framenumber with the "timePerFrame value"
            int FOI = i;
            float timeCounter = (float)timePerFrame*FOI;

            int vectorIndex = findTimestamp(timeCounter, gazeVector);

            // only when an index was found, we plot the gaze cursor
            if(vectorIndex !=-1)    // plot gaze cursor
            {
                float normX = gazeVector[vectorIndex].normX;
                float normY = gazeVector[vectorIndex].normY;                

                Point P1;
                P1.x = normX;
                P1.y = normY;

                // check and correct if gaze-cursor is positioned outside the image
                if(P1.x<0)
                    P1.x = 0;

                if(P1.y < 0)
                    P1.y = 0;

                if(P1.x>image.cols)
                    P1.x = image.cols;

                if(P1.y>image.rows)
                    P1.y = image.rows;

                circle(image,P1,20,Scalar(0,0,255),-1);
            } 
#ifdef Show
            imshow("window",image);
            waitKey();
#endif
         }

#ifdef Write
            if(!image.empty())
            {                
				char outputpath[200];
                sprintf(outputpath, "%s%05d%s", outputFolder.c_str(),i,".png");
				imwrite(outputpath,image);
			}
#endif
    }
}
