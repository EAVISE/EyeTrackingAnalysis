#ifndef PUPILPARSER
#define PUPILPARSER

#include <iostream>
#include <sstream>

#include <string>
#include <fstream>
#include <stdio.h>

#include <dirent.h>

#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

struct GazeInfo {                          // Detection structure
    float timestamp;
    float normX;
    float normY;
};

int ParseTSV(std::string path, std::vector<GazeInfo> &gazeVector);
int findTimestamp(float timeCounter, std::vector<GazeInfo> gazeVector);
int convert2Int(string input);
float convert2Float(string input);

#endif // PUPILPARSER

