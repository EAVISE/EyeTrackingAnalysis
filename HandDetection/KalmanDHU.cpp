#include "KalmanDHU.h"
#include <fstream>
#include <iostream>

KalmanDHU::KalmanDHU()
{
    //ctor
    initKalman = true;
    KF = KalmanFilter(4,2,0);///
    measurement = Mat_<float>(2,1);
    measurement.setTo(Scalar(0));
}

KalmanDHU::~KalmanDHU()
{
    //dtor
}

void KalmanDHU::Init(Point startPoint)
{
    KF.transitionMatrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    KF.statePre.at<float>(0) = startPoint.x;
    KF.statePre.at<float>(1) = startPoint.y;
    KF.statePre.at<float>(2) = 0;
    KF.statePre.at<float>(3) = 0;
    setIdentity(KF.measurementMatrix);
    setIdentity(KF.processNoiseCov, Scalar::all(1e-3)); //
    setIdentity(KF.measurementNoiseCov, Scalar::all(0.005)); //
    setIdentity(KF.errorCovPost, Scalar::all(.1));

    initKalman = false;

    Mat estimated;
    for(int j=0;j<5;j++)
    {
        prediction = KF.predict();
        measurement(0) = startPoint.x;
        measurement(1) = startPoint.y;
        estimated = KF.correct(measurement);
    }
}

///Predict the next kalman values that are tracked
void KalmanDHU::predictKalman()
{
    prediction = KF.predict();
    pre_X = prediction.at<float>(0);
    pre_Y = prediction.at<float>(1);
}

///Correct Kalman
void KalmanDHU::correctKalman(Point input)
{
    measurement(0) = input.x;
    measurement(1) = input.y;

    Mat estimated = KF.correct(measurement);//

    corr_X = estimated.at<float>(0);
    corr_Y = estimated.at<float>(1);    
}

///GETTERS AND SETTERS
float KalmanDHU::getPreX()
{
    return pre_X;
}
float KalmanDHU::getPreY()
{
    return pre_Y;
}

Point KalmanDHU::getPreXY()
{
    return Point(pre_X,pre_Y);
}

float KalmanDHU::getCorrX()
{
    return corr_X;
}
float KalmanDHU::getCorrY()
{
    return corr_Y;
}

Point KalmanDHU::getCorrXY()
{
    return Point(corr_X,corr_Y);
}

bool KalmanDHU::GetInit()
{
    return this->initKalman;
}
