#ifndef HAND_H
#define HAND_H

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
//#include <opencv2/gpu/gpu.hpp>

#include "opencv2/opencv.hpp"


#include <time.h>

using namespace cv;
using namespace std;

class Hand
{

public:
    Hand();
    void SetHand(int x, int y);
    void SetHand(Point P);
    Point GetHand();
    bool CheckDisplacement(Point Prediction, int TH);
    int GetX();
    int GetY();
    void GetCandidates(Point2f prediction, vector<Vec4i> Det);

    Vec4i GetBest();
    double GetBestDist();

    Vec4i GetSecBest();
    double GetSecBestDist();

    bool NotEmpty();

    void SetManIndex(int index);
    int GetManIndex();

    int GetBestIndex();
    int GetSecBestIndex();

    double GetUsedDist();
    void SetUsedDist(double dist);

    int getPrediction();
    void setPrediction(int pred);
    void increasePrediction();
    void decreasePrediction();
    void setValid(int valid);
    int getValid();
    void increaseValid();
    void decreaseValid();


private:
    Point HandCo_;
    double minDist_;
    double secminDist_;
    double usedDist_;

    Vec4i best_;
    Vec4i secbest_;

    int bestIndex_;
    int secbestIndex_;

    bool findMin(double A,double B);

    int ManIndex;

    int prediction_;
    int valid_;
};

#endif // HAND_H
