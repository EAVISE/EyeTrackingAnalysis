#ifndef DETECTOR_VJ_H
#define DETECTOR_VJ_H

#include "Detector.h"
#include <string>
#include "opencv2/opencv.hpp"

#include <stdio.h>
#include <iostream>
#include "detectionDHU.h"

using namespace std;
using namespace cv;

class Detector_VJ : public Detector
{
public:
    /** Default constructor */
    Detector_VJ();
    /** Default destructor */
    virtual ~Detector_VJ();
    int loadCascades(String,String);

    std::vector<DHU::Detection> Detect(cv::Mat im);    

protected:
    CascadeClassifier face_cascade;
    CascadeClassifier profileleft_cascade;
private:
};

#endif // DETECTOR_VJ_H
