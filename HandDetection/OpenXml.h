#ifndef OPENXML_H
#define OPENXML_H

#include <iostream>
#include <string>     // std::string, std::to_string
#include <stdio.h>
#include <QXmlStreamReader>
#include <QFile>

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
//#include <opencv2/gpu/gpu.hpp>
#include "opencv2/opencv.hpp"


using namespace std;
using namespace cv;

class OpenXml
{
    public:
        OpenXml(string xmlFile);
        void XmlToData();

        string filenameAvi;
        vector<int> frameNr;
        vector<vector<Point> > hand;
        vector<vector<Point> > left_hand;
        vector<vector<Point> > right_hand;
        vector<vector<Vec4i> > arm;
        vector<vector<double> > handscore;
        vector<vector<double> > armscore;
        vector<Rect> face;
        vector<vector<Rect> > upperBody;
        vector<double> runtime;
        vector<vector<float> > facescore;
        vector<int> unused;


    private:
        QXmlStreamReader* xml;

        Rect readRect();
        RotatedRect readHand();
        Point readPoint();
        Vec4i readLine();
        double readHandScore();
        double score;
};

#endif // OPENXML_H
