#include "probcheck.h"

ProbCheck::ProbCheck()
{
    // constructor
    this->probabilty_ = 0;
    this->CenterElbow_R_ = Point2f(115.9,88.1);
    this->CenterElbow_L_ = Point2f(102.3,88.1);

    this->CenterWrist_R_= Point2f(224.94,149.25);     // right wrist with respect to shoulder
    this->CenterWrist_L_= Point2f(184.48,149.25);     // left wrist with respect to shoulder
    this->index_L_ = 0;
    this->index_R_ = 0;
    this->Average_L_ = 10;
    this->Average_R_ = 10;
    this->AvgSize = 3;
}

void::ProbCheck::UpdateMap(Mat &Map, Point NewPoint)
{
    // size of gaussian window
    int sizeNew = 45;
    int center = sizeNew/2;
    Mat New = Mat::zeros(sizeNew, sizeNew, CV_64F);

    // make value of center pixels = 1
    New.at<double>(center,center) = 1.0;
    New.at<double>(center,center-1) = 1.0;
    New.at<double>(center-1,center) = 1.0;
    New.at<double>(center+1,center) = 1.0;
    New.at<double>(center,center+1) = 1.0;

    // do gaussian blurring
    GaussianBlur( New, New, Size( sizeNew, sizeNew ), 0, 0 );
    double min, max;
    cv::minMaxLoc(New, &min, &max);

    // scale values up to 1
    New = New*(1/max);

    // add this window to the heatmap
    Rect ROI(NewPoint.x-center, NewPoint.y-center, New.cols, New.rows);
    Mat Temp = Map(ROI).clone();
    Mat Result = Temp+New;
    Mat imgPanelRoi(Map, ROI);
    Result.copyTo(imgPanelRoi);

    cout << "probability map was updated... " << endl;
}

int ProbCheck::Init(string path)
{
    // Read Probability map of elbow positions:
    MatlabIO matio;
    bool ok = matio.open(path.c_str(), "r");
    if(!ok)
        return -1;

    // read all of the variables in the file
    vector<MatlabIOContainer> variables;
    variables = matio.read();
    cout << "variables size = " << variables.size() << endl;

    // read data    
    for (unsigned int n = 0; n < variables.size(); ++n)
    {
        if (variables[n].name().compare("ProbElbowLeft") == 0) {
            this->ProbElbow_Left_ = variables[n].data<Mat>();
            cout << "Size ProbElbow_left = " << this->ProbElbow_Left_.size() <<" --> Centrum = " << this->CenterElbow_L_<< endl;            
        }

        if (variables[n].name().compare("ProbElbowRight") == 0) {
            this->ProbElbow_Right_ = variables[n].data<Mat>();
            cout << "Size ProbElbow_right = " << this->ProbElbow_Right_.size() <<" --> Centrum = " << this->CenterElbow_R_<< endl;
        }

        if (variables[n].name().compare("ProbWristLeft") == 0) {
             this->ProbWrist_Left_ = variables[n].data<Mat>();
            cout << "Size ProbWrist_left = " << this->ProbWrist_Left_.size() <<" --> Centrum = " << this->CenterWrist_R_<< endl;                        
        }

        if (variables[n].name().compare("ProbWristRight") == 0) {
            this->ProbWrist_Right_ = variables[n].data<Mat>();
            cout << "Size ProbWrist_right = " << this->ProbWrist_Right_.size() <<" --> Centrum = " << this->CenterWrist_L_<< endl;            
        }
    }
    // close the file
    matio.close();

    return 1;
}

double ProbCheck::CheckPosition(Mat ProbMap, Point2f Ref, double norm, Point POI, Point2f Center, int side)         // side 0 = Left
                                                                                                                    // side 1 - Right
{  
    this->probabilty_ = 999.0;    
    if(POI.x>0 && POI.y>0 && Ref.x>0 && Ref.y > 0)      // Make sure there is a detection
    {        
        double Rel_X = (POI.x - Ref.x);
        double Rel_Y = (POI.y - Ref.y);

        Rel_X = ((Rel_X / norm) * 100) + Center.x;
        Rel_Y = ((Rel_Y / norm) * 100) + Center.y;  

        if(Rel_X>ProbMap.cols)
            Rel_X = ProbMap.cols;

        if(Rel_Y>ProbMap.rows)
            Rel_Y = ProbMap.rows;

        if(Rel_X<0)
            Rel_X = 0;

        if(Rel_Y<0)
            Rel_Y = 0;

        this->probabilty_ = ProbMap.at<double>(Rel_Y,Rel_X)*100;

        if(side==0) // left
        {                        
            this->AvgProb_L_[this->index_L_] = this->probabilty_;
            IncreaseCounter_L();
            CalcAverage(this->AvgProb_L_,0);
        }

        if(side==1) // right
        {                        
            this->AvgProb_R_[this->index_R_] = this->probabilty_;
            IncreaseCounter_R();
            CalcAverage(this->AvgProb_R_,1);
        }        
    }
    return this->probabilty_;
}

double ProbCheck::CheckPositionWrist(Mat ProbMap, Point2f Ref, double norm, Point POI, Point2f Center, int side)
{
    this->probabilty_ = 999.0;
    if(POI.x>0 && POI.y>0 && Ref.x>0 && Ref.y > 0)      // Make sure there is a detection
    {
        double Rel_X = (POI.x - Ref.x);
        double Rel_Y = (POI.y - Ref.y);

        Rel_X = ((Rel_X / norm) * 100) + Center.x;
        Rel_Y = ((Rel_Y / norm) * 100) + Center.y;

        cout << "X = " << Rel_X << " | Y =" << Rel_Y << endl;

        if(Rel_X>ProbMap.cols)
            Rel_X = ProbMap.cols;

        if(Rel_Y>ProbMap.rows)
            Rel_Y = ProbMap.rows;

        if(Rel_X<0)
            Rel_X = 0;

        if(Rel_Y<0)
            Rel_Y = 0;

        this->probabilty_ = ProbMap.at<double>(Rel_Y,Rel_X)*100;

        if(side==0) // LEFT
        {
            this->RelPosLeftWrist_.x = Rel_X;
            this->RelPosLeftWrist_.y = Rel_Y;
        }
        if(side==1) // RIGHT
        {
            this->RelPosRightWrist_.x = Rel_X;
            this->RelPosRightWrist_.y = Rel_Y;
        }
    }
    return this->probabilty_;
}


Mat ProbCheck::GetProbMapElbow_right()
{
    return this->ProbElbow_Right_;
}

Mat ProbCheck::GetProbMapElbow_left()
{
    return this->ProbElbow_Left_;
}

Mat ProbCheck::GetProbMapWrist_right()
{
    return this->ProbWrist_Right_;
}

Mat ProbCheck::GetProbMapWrist_left()
{
    return this->ProbWrist_Left_;
}

void ProbCheck::SetProbMapElbow_right(Mat New)
{
    this->ProbElbow_Right_ = New;
}

void ProbCheck::setProbMapElbow_left(Mat New)
{
    this->ProbElbow_Left_ = New;
}

void ProbCheck::SetProbMapWrist_right(Mat New)
{
    this->ProbWrist_Right_ = New;
}

void ProbCheck::SetProbMapWrist_left(Mat New)
{
    this->ProbWrist_Left_ = New;
}


Point2f ProbCheck::GetCenterElbow_right()
{
    return this->CenterElbow_R_;
}

Point2f ProbCheck::GetCenterElbow_left()
{
    return this->CenterElbow_L_;
}


Point2f ProbCheck::GetCenterWrist_right()
{
    return this->CenterWrist_R_;
}


Point2f ProbCheck::GetCenterWrist_left()
{
    return this->CenterWrist_L_;
}

 void ProbCheck::IncreaseCounter_L()
 {
     if(index_L_<2)
        index_L_ = index_L_++;
     else
        index_L_ = 0;
 }

 void ProbCheck::IncreaseCounter_R()
 {
     if(index_R_<2)
        index_R_ = index_R_++;
     else
        index_R_ = 0;
 }


 double ProbCheck::CalcAverage(double Brray[3], int side)
 {
     int size = 0;
     double sum = 0;
     for (int i = 0; i < AvgSize; ++i)
      {
         cout << "item["<<i<<"]: " << Brray[i] << endl;
          sum += Brray[i];          
          if(Brray[i]>0)
              size++;
      }

     if(side==0)
         this->Average_L_ = ((float)sum)/size; //or cast sum to double before division

     if(side==1)
         this->Average_R_ = ((float)sum)/size; //or cast sum to double before division
 }

 double ProbCheck::GetAverageLeft()
 {
     return this->Average_L_;
 }

 double ProbCheck::GetAverageRight()
 {
     return this->Average_R_;
 }

 void ProbCheck::SetAverageLeft(double value)
 {
     this->Average_L_ = value;     
     this->AvgProb_L_[this->index_L_]=value;
     IncreaseCounter_L();
 }

 void ProbCheck::SetAverageRight(double value)
 {
     this->Average_R_ = value;    
     this->AvgProb_R_[this->index_R_]=value;
     IncreaseCounter_R();
 }

 Point ProbCheck::GetRelWristLeft()
 {
     return this->RelPosLeftWrist_;
 }

 Point ProbCheck::GetRelWristRight()
 {
     return this->RelPosRightWrist_;
 }

 Point ProbCheck::GetRelElbowLeft()
 {
     return this->RelPosLeftElbow_;
 }

 Point ProbCheck::GetRelElbowRight()
 {
     return this->RelPosRightElbow_;
 }


