#ifndef DETECTIONDHU_H
#define DETECTIONDHU_H
#include <iostream>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>


/*!
	This class represents a detection. It can also be used for annotations. This class forms the input and output for our detection/training-software. By making this format detector-independend we simplify the switching between different detectors
*/

namespace DHU
{
class Detection
{
public:
    Detection();

    int x,y,width,height;
    float score;

};
}

#endif // DETECTIONDHU_H
