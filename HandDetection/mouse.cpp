#include "mouse.h"

//====================================================================================
//
//
//====================================================================================
void my_mouse_callback( int event, int x, int y, int flags, void* param ){

    func_params* ParamMouse = (func_params*) param;
    if(event==CV_EVENT_LBUTTONDOWN)
    {
            ParamMouse->Punt1.y = y;
            ParamMouse->Punt1.x = x;
            cout << "click loc = " << x << "," << y << endl;
    }
}

// Implement mouse callback version 2
void my_mouse_callback2( int event, int x, int y, int flags, void* param ){

    func_params2* ParamMouse = (func_params2*) param;
    if(event==CV_EVENT_LBUTTONDOWN)
    {
        ParamMouse->counter = ParamMouse->counter+1;
        cout << "click loc = " << x << "," << y << endl;

        if(ParamMouse->counter==1)
        {
            ParamMouse->Punt1.y = y;
            ParamMouse->Punt1.x = x;
        }
        else if(ParamMouse->counter==2)
        {
            ParamMouse->Punt2.y = y;
            ParamMouse->Punt2.x = x;
        }
        else if(ParamMouse->counter==3)
        {
            ParamMouse->Punt3.y = y;
            ParamMouse->Punt3.x = x;
        }
        else if(ParamMouse->counter==4)
        {
            ParamMouse->Punt4.y = y;
            ParamMouse->Punt4.x = x;
        }
    }
}
