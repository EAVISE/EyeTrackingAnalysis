#ifndef JOINT_H
#define JOINT_H

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
//#include <opencv2/gpu/gpu.hpp>

#include "opencv2/opencv.hpp"


#include <time.h>

using namespace cv;
using namespace std;

class Joint
{

public:
    Joint();
    void SetJoint(int x, int y);
    void SetJoint(Point P);
    Point GetJoint();
    bool CheckDisplacement(Point Prediction, int TH);
    int GetX();
    int GetY();

    Mat CalculateHistogram(Mat Image);
    void SetRefHist(Mat Hist);
    double CompareHist(Mat Hist);
    double GetDisplace();
    void SetDisplace(double displace);
    int GetDelta_X();
    int GetDelta_Y();
    void SetDelta_X(bool X);
    void SetDelta_Y(bool Y);

    void decreasePrediction();
    void increasePrediction();
    void setPrediction(int pred);
    int getPrediction();

private:
    Point JointCo;    
    Mat RefHist;
    int displaceCounter;
    double displace_;
    bool delta_X_;
    bool delta_Y_;
    int prediction_;

    void IncCounter();
    void DecCounter();
    void SetCounter(int value);
    int GetCounter();    
};

#endif // JOINT_H
