#ifndef MAINWINDOW_H
#define MAINWINDOW_H
//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"

#include "opencv2/opencv.hpp"


#include <QMainWindow>
#include <QtWidgets/QFileDialog>
#include "KalmanDHU.h"
#include "hand.h"
#include "Joint.h"
#include "processthread.h"
#include "probcheck.h"


using namespace cv;
using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();    
    int ManualIntervention();
    void Update(KalmanDHU & Kalman, Hand & HandInst, int frame, Point newPos);
    void UpdateJoint(KalmanDHU & Kalman, Joint & JointInst, Point newPos);
    double CheckElbowPos(Mat ProbElbow, Rect Torso, Joint JointRight);
    float GetPrevScore();
    void SetPrevScore(float score);
    Point ReadAnnotations(int frame, int type);


private slots:
    void on_ManIntervent_clicked();
    void on_Start_clicked();
    void on_SetWorkingDir_clicked();
    void on_VideoProcess_clicked();

private:
    Ui::MainWindow *ui;
    Mat Image;
    vector<string> categories;
     vector<string> keepCategories;
    vector<string> Probcategories;

    Point ManualHandLeft;
    Point ManualHandRight;
    Point ManualJointLeft;
    Point ManualJointRight;
    int manIntercept;
    int manInterceptFrame;
    int CurrentFrame;

    KalmanDHU Kal_Left;
    KalmanDHU Kal_Right;

    KalmanDHU Kal_Joint_Left;
    KalmanDHU Kal_Joint_Right;

    Hand HandLeft;
    Hand HandRight;

    Joint JointLeft;
    Joint JointRight;

    string WorkFolder_;

    int DisplaceTH;

    ProcessThread thread;

    bool initKalJointL;
    bool initKalJointR;

    bool initKalmanFace;
    int avgFaceWidth;
    int facePredictions;

    bool ManLeft;
    bool ManRight;
    bool ManJointLeft;
    bool ManJointRight;

    float PrevFaceScore;

    ProbCheck ProbTest;

    bool ManualLeft_Set;
    bool ManualRight_Set;

    bool KeepLeftHand;
    bool KeepRightHand;
    bool KeepLeftJoint;
    bool KeepRightJoint;

    Point HoldHandLeft;
    Point HoldHandRight;
    Point HoldJointLeft;
    Point HoldJointRight;

    int startframe;
};

#endif // MAINWINDOW_H
