#include "FacedDetection.h"

FaceDetection::FaceDetection(string faceModelFile)
{
    // Face detection
    //CascadeClassifier face_cascade;

    this->setPrediction(0);
    this->valid_ = 0;
    // haarcascade_frontalface_alt.xml {
    if(!face_cascade.load(faceModelFile)) {
        cout << "Error loading haarcascade xml file." << endl;
    }
}

Rect FaceDetection::detectFace(Mat In)
{
    this->prev_height_ = this->face.height;
    this->prev_width_ = this->face.width;


    cout << "original size = " << In.size() << endl;
    int scaleParameter = 2;
    Size size(In.cols/scaleParameter,In.rows/scaleParameter);
    resize(In,In,size);
    cout << "new size = " << In.size() << endl;

    vector<Rect> faces;
    face_cascade.detectMultiScale(In, faces, 1.1, 3);

    // Take first face
    if(faces.size() > 0) {
        this->face = faces[0];
        return faces[0];
    } else {
        cout << "       No face found" << endl;
        return Rect();
    }
}

int FaceDetection::getPrevWidth()
{
    return this->prev_width_;
}

int FaceDetection::getPrevHeight()
{
    return this->prev_width_;
}

int FaceDetection::getPrediction()
{
    return this->prediction_;
}

void FaceDetection::setPrediction(int pred)
{
    this->prediction_ = pred;
}

void FaceDetection::increasePrediction()
{
    this->prediction_ = this->prediction_+1;
}

void FaceDetection::decreasePrediction()
{
    if(this->prediction_>0)
    {
        this->prediction_ = this->prediction_-1;
    }
}

void FaceDetection::setValid(int valid)
{
    this->valid_ = valid;
}

int FaceDetection::getValid()
{
    return this->valid_;
}

void FaceDetection::increaseValid()
{
    this->valid_ = this->valid_+1;
}

void FaceDetection::decreaseValid()
{
    this->valid_ = this->valid_-1;
}

