/*!
 * @class DetHand
 * Hand detection class. Detects hand on images.
 *
 * @author Den Dooven Raphael
 *
 */

#ifndef DETHAND_H
#define DETHAND_H

#include "FFLD/SimpleOpt.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "opencv2/opencv.hpp"

#include <time.h>

using namespace cv;
using namespace std;

class DetHand
{
    public:
        /// @brief Constructor from classe DetHand.
        /// Preps the detector with the right hand model.
        /// @param ModelHand Filename of model
        /// @param threshold Threshold value of detector
        DetHand();

        /// @brief function to segment an image in skin and non-skin
        /// @param image as Mat object
        /// @return binary image of skin pixels
        Mat skinSegmentation(Mat src, double TH, int minBound, int maxBound);
};

#endif // DETHAND_H

