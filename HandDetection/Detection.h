#ifndef DETECTION_H
#define DETECTION_H

#include "FFLD/SimpleOpt.h"
#include "FFLD/Intersector.h"
#include "FFLD/Mixture.h"
#include "FFLD/Scene.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "opencv2/opencv.hpp"


using namespace cv;
//using namespace gpu;
using namespace FFLD;
using namespace std;

//----------------------------------------------------------------//
// Structures
//----------------------------------------------------------------//
struct UniqueDetections
{
    Point Center;
    cv::Rect detect;
    double BestScore;
};


struct TSDetection
{
    Point Center;
    cv::Rect Rectangle;
    int weight;
    int update;
    int lastFrameNr;
    int firstFrameNr;
    int FrameNr;
    int estimate;
    double DetectionScore;
};


struct DetectionFFLD : public FFLD::Rectangle
{
    HOGPyramid::Scalar score;
    int l;
    int x;
    int y;

    DetectionFFLD() : score(0), l(0), x(0), y(0)
    {
    }

    DetectionFFLD(HOGPyramid::Scalar score, int l, int x, int y, FFLD::Rectangle bndbox):
    FFLD::Rectangle(bndbox), score(score), l(l), x(x), y(y)
    {
    }

    bool operator<(const DetectionFFLD & detection) const
    {
        return score > detection.score;
    }
};

//----------------------------------------------------------------//
// Functions
//----------------------------------------------------------------//

void detect(const Mixture & mixture, int width, int height, const HOGPyramid & pyramid,
            double threshold, double overlap, vector<DetectionFFLD> &detections);

int Init(Mixture &mixture, string model);

void NonMaxSuppr(vector<RotatedRect> &input, vector<RotatedRect> &output, int nmsFactor );

void ConvertFormat(vector<DetectionFFLD> input, vector<UniqueDetections> & output);

#endif // DETECTION_H
