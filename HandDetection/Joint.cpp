#include "Joint.h"

using namespace cv;
using namespace std;

Joint::Joint()
{
    this->displaceCounter = 0;    
    this->setPrediction(0);
}

void Joint::SetJoint(int x, int y)
{
    this->JointCo.x = x;
    this->JointCo.y = y;
}

void Joint::SetJoint(Point P)
{
    this->JointCo.x = P.x;
    this->JointCo.y = P.y;
}

Point Joint::GetJoint()
{
    return  this->JointCo;
}

int Joint::GetX()
{
    return this->JointCo.x;
}

int Joint::GetY()
{
    return this->JointCo.y;
}

/*
 * checks displacement between current en predicted poistion of the joint
 * return true if displacement is below TH
 * returns false if displacement is too large
  */
bool Joint::CheckDisplacement(Point Prediction, int TH)
{
    if(this->JointCo.x>0&&this->JointCo.y>0&&Prediction.x>0 &&Prediction.y>0)
    {
        Point2f CurrLeft(this->JointCo.x,this->JointCo.y);
        Point2f PredLeft(Prediction.x,Prediction.y);
        double displace = norm(CurrLeft-PredLeft);
        this->displace_ = displace;
        cout << "displacement = " << this->displace_ << endl;
        if(displace>TH)
            return false;
        else
            return true;
    }
    else
        return true;
}

Mat Joint::CalculateHistogram(Mat Image)
{
    Mat Mask = Mat::zeros(Image.rows, Image.cols,CV_8U );
    circle(Mask,this->JointCo,15,Scalar(255),-1);

    Mat hsv,hue,hist;
    cvtColor(Image, hsv, CV_BGR2HSV);
    hue.create( hsv.size(), hsv.depth() );
    int ch[] = { 0, 0 };
    mixChannels( &hsv, 1, &hue, 1, ch, 1 );

    int bins = 10;
    float hranges[] = {0,179};
    const float* ranges = { hranges };
    int histSize = MAX( bins, 2 );

    calcHist( &hue, 1, 0, Mask, hist, 1, &histSize, &ranges, true, false );
    normalize( hist, hist, 0, 1, NORM_MINMAX, -1, Mat() );

    return hist;
}

void Joint::SetRefHist(Mat Hist)
{
    this->RefHist = Hist;
}

double Joint::CompareHist(Mat Hist)
{
    double res=0;
    if(this->RefHist.rows>0)
    {
        res = compareHist( Hist, this->RefHist, CV_COMP_INTERSECT );
    }
    return res;
}

void Joint::IncCounter()
{
    this->displaceCounter = this->displaceCounter+1;
}

void Joint::DecCounter()
{
    this->displaceCounter = this->displaceCounter-1;
}

int Joint::GetCounter()
{
    return this->displaceCounter;
}

void Joint::SetCounter(int value)
{
    this->displaceCounter = value;
}

double Joint::GetDisplace()
{
    return this->displace_;
}

void Joint::SetDisplace(double displace)
{
    this->displace_ = displace;
}

int Joint::GetDelta_X()
{
    return this->delta_X_;
}

int Joint::GetDelta_Y()
{
    return this->delta_Y_;
}

void Joint::SetDelta_X(bool X)
{
    this->delta_X_ = X;
}

void Joint::SetDelta_Y(bool Y)
{
    this->delta_Y_ = Y;
}

int Joint::getPrediction()
{
    return this->prediction_;
}

void Joint::setPrediction(int pred)
{
    this->prediction_ = pred;
}

void Joint::increasePrediction()
{
    this->prediction_ = this->prediction_+1;
}

void Joint::decreasePrediction()
{
    if(this->prediction_>0)
    {
        this->prediction_ = this->prediction_-1;
    }
}
