#ifndef MOUSE_H
#define MOUSE_H

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <iomanip>
#include <dirent.h>

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
//#include <opencv2/gpu/gpu.hpp>
#include "opencv2/opencv.hpp"

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace cv;

struct func_params {
  Point Punt1;
};

struct func_params2 {
  Point Punt1;
  Point Punt2;
  Point Punt3;
  Point Punt4;
  int counter = 0;
};

void my_mouse_callback(int event, int x, int y, int flags, void* param);
void my_mouse_callback2(int event, int x, int y, int flags, void* param);

#endif // MOUSE_H
