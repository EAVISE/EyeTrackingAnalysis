#ifndef __H_LOADDETS
#define __H_LOADDETS

//#include "opencv2/imgproc/imgproc.hpp"
//#include "opencv2/objdetect/objdetect.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/ml/ml.hpp"
#include "opencv2/opencv.hpp"

#include <fstream>

#include "detectionFDS.h"
#include <vector>

#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_print.hpp"


/*!
	This class holds a list of detections found in the image.
*/
class DetectionList {
public:
    //! filename of the image
    std::string m_path;
    //! vector of detections associated with the image
    std::vector<Detection*> m_dets;
};

/*!
	This function is used to read the detections from an xml-file and creates a vector of all these detections and vectorizes those per image they are found in (or annotated in)
	\param Detections The filename to read
	\return vector of DetectionList which holds for each image file the associated detections
*/
std::vector<DetectionList> ReadDetectionFile(std::string Detections);

#endif
