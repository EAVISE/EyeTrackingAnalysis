#include "DetHand.h"

DetHand::DetHand()
{  
    // constructor
}

Mat DetHand::skinSegmentation( Mat src, double TH, int minBound, int maxBound)
{
        // allocate the result matrix
        Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC1);

        Mat src_ycrcb, src_hsv,src_luv;;
        // OpenCV scales the YCrCb components, so that they
        // cover the whole value range of [0,255], so there's
        // no need to scale the values:
        cvtColor(src, src_ycrcb, CV_BGR2YCrCb);
        // OpenCV scales the Hue Channel to [0,180] for
        // 8bit images, so make sure we are operating on
        // the full spectrum from [0,360] by using floating
        // point precision:
        //src.convertTo(src_hsv, CV_32FC3);
        cvtColor(src, src_hsv, CV_BGR2HSV);
        // Now scale the values between [0,255]:
        //normalize(src_hsv, src_hsv, 0.0, 255.0, NORM_MINMAX, CV_32FC3);
		cvtColor(src, src_luv, CV_BGR2Luv);
    vector<Mat> luv_channels;
    split(src_luv, luv_channels);
        
        
        for(int i = 0; i < src.rows; i++) {
            for(int j = 0; j < src.cols; j++) {

                Vec3b pix_bgr = src.ptr<Vec3b>(i)[j];
                int B = pix_bgr.val[0];
                int G = pix_bgr.val[1];
                int R = pix_bgr.val[2];
                // apply rgb rule
                bool e1 = (R>95) && (G>40) && (B>20) && ((max(R,max(G,B)) - min(R, min(G,B)))>15) && (abs(R-G)>15) && (R>G) && (R>B);
                bool e2 = (R>220) && (G>210) && (B>170) && (abs(R-G)<=15) && (R>B) && (G>B);
                bool a = (e1||e2);

                Vec3b pix_ycrcb = src_ycrcb.ptr<Vec3b>(i)[j];
                //int Y = pix_ycrcb.val[0];
                int Cr = pix_ycrcb.val[1];
                int Cb = pix_ycrcb.val[2];
                // apply ycrcb rule
                bool e3 = Cr <= 1.5862*Cb+20;
                bool e4 = Cr >= 0.3448*Cb+76.2069;
                bool e5 = Cr >= -4.5652*Cb+234.5652;
                bool e6 = Cr <= -1.15*Cb+301.75;
                bool e7 = Cr <= -2.2857*Cb+432.85;
                bool b = e3 && e4 && e5 && e6 && e7;
                bool c = false;
                if(TH>0)
                {
                    // use face information
                    Vec3b pix_luv = src_luv.ptr<Vec3b>(i)[j];
                    int U = pix_luv.val[1];
                    c = (U>TH-minBound) && (U<TH+maxBound);
                }
				else
				{
					// use standard rule in HSV domain
					Vec3f pix_hsv = src_hsv.ptr<Vec3f>(i)[j];
					float H = pix_hsv.val[0];
					//float S = pix_hsv.val[1];
					//float V = pix_hsv.val[2];
					// apply hsv rule
					c = (H<25) || (H > 230);
				}
				
                if((a&&b&&c))
                    dst.ptr(i)[j] = 255;
            }
        }
        return dst;   
}
