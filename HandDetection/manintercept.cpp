#include "manintercept.h"

ManIntercept::ManIntercept(vector<string> categories, vector<string> Probcategories, QWidget *parent)
    : QDialog(parent)
{
    this->Close = false;
    this->LeftWristOK = false;
    this->LeftElbowOK = false;
    this->RightWristOK = false;
    this->RightElbowOK = false;
    this->currentID = -1;

    // ID of items = 0 - 3
    CatList newItem;
    for(int i=0;i<categories.size();i++)
    {
        newItem.cat_id = i;
        newItem.cat = categories[i];
        this->List.push_back(newItem);
    }

    // ID of probItems = 10 - 13
    CatList newProbItem;
    for(int i=0;i<Probcategories.size();i++)
    {
        newProbItem.cat_id = i+10;
        newProbItem.cat = Probcategories[i];
        this->ProbList.push_back(newProbItem);
    }

    cout << "size vector list = " << this->List.size() << endl;
    cout << "size vector Problist = " << this->ProbList.size() << endl;

    CancelButton = new QPushButton(tr("&Stop"));
    CancelButton->setDefault(true);
    connect(CancelButton,SIGNAL(clicked()),this,SLOT(CloseDialog()));

    ProbLeftWristButton = new QPushButton(tr("&Prob Left Wrist OK"));
    connect(ProbLeftWristButton,SIGNAL(clicked()),this,SLOT(LeftWristOKDialog()));

    ProbRightWristButton = new QPushButton(tr("&Prob Right Wrist OK"));
    connect(ProbRightWristButton,SIGNAL(clicked()),this,SLOT(RightWristOKDialog()));

    ProbLeftElbowButton = new QPushButton(tr("&Prob Left Elbow OK"));
    connect(ProbLeftElbowButton,SIGNAL(clicked()),this,SLOT(LeftElbowOKDialog()));

    ProbRightElbowButton = new QPushButton(tr("&Prob Right Elbow OK"));
    connect(ProbRightElbowButton,SIGNAL(clicked()),this,SLOT(RightElbowOKDialog()));

    QVBoxLayout *Layout1 = new QVBoxLayout;
    Layout1->addWidget(CancelButton);

    QVBoxLayout *Layout2 = new QVBoxLayout;
    QVBoxLayout *Layout3 = new QVBoxLayout;
    QVBoxLayout *Layout4 = new QVBoxLayout;

    QHBoxLayout *Layout = new QHBoxLayout;
    Layout->addLayout(Layout1);

    QMainWindow *window = new QMainWindow();
    QWidget *centralWidget = new QWidget(window);

    MyButtonGroup* group = new MyButtonGroup(centralWidget);
    connect(group,SIGNAL(WhichButton(int)),this, SLOT(UpdateList(int)));
    int halfSize = List.size()/2;
    cout << "half size = " << halfSize << endl;

    for(int i=0;i<this->List.size();i++)
    {
        if(i<halfSize)
        {
            cout << "Cat " << this->List[i].cat_id << ": " << this->List[i].cat << endl;
            QString qstr = QString::fromStdString(this->List[i].cat);

            QPushButton* button = new QPushButton(qstr,centralWidget);
            group->addButton(button,i);
            Layout2->addWidget(button);
        }
        else
        {
            cout << "Cat " << this->List[i].cat_id << ": " << this->List[i].cat << endl;
            QString qstr = QString::fromStdString(this->List[i].cat);

            QPushButton* button = new QPushButton(qstr,centralWidget);
            group->addButton(button,i);
            Layout4->addWidget(button);
        }
    }

    // TEST PHASE
    MyButtonGroup* group2 = new MyButtonGroup(centralWidget);
    connect(group2,SIGNAL(WhichButton(int)),this, SLOT(UpdateProbList(int)));
    for(int i=0;i<this->ProbList.size();i++)
    {
        cout << "Cat " << this->ProbList[i].cat_id << ": " << this->ProbList[i].cat << endl;
        QString qstr = QString::fromStdString(this->ProbList[i].cat);
        QPushButton* button = new QPushButton(qstr,centralWidget);
        group2->addButton(button,i);
        Layout3->addWidget(button);
    }    

    Layout->addLayout(Layout2);
    Layout->addLayout(Layout4);
    Layout->addLayout(Layout3);

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    mainLayout->addLayout(Layout, 0, 0);
    setLayout(mainLayout);
    setWindowTitle(tr("Manual Intercept"));
}

 /*******************************************************************
 // Selected FOI belongs to a previous defined
 // category. Add this new FOI to the FOI-list (TrainInfo.txt)
 ********************************************************************/
  void ManIntercept::UpdateList(int NewItem)
  {
      cout << "Category = " << NewItem << endl;

      // Find name of this category
      cout << "Cat name = " << this->List[NewItem].cat << endl;
      currentCat = List[NewItem].cat;
      currentID = NewItem;

      this->close();
  }

  void ManIntercept::UpdateProbList(int Item)
  {
      cout << "id selected cat = " << Item << endl;
      this->CurrentProbID = Item;
      this->currentProbCat = this->ProbList[Item].cat;

      string value = this->ProbList[Item].cat;
      cout << "value = " << value.c_str() << endl;

      if(value.compare("Prob Left Wrist OK")==0)
         this->LeftWristOK = true;

      if(value.compare("Prob Right Wrist OK")==0)
          this->RightWristOK = true;

      if(value.compare("Prob Left Elbow OK")==0)
          this->LeftElbowOK = true;

      if(value.compare("Prob Right Elbow OK")==0)
          this->RightElbowOK = true;
  }

 void ManIntercept::CloseDialog()
  {
      cout << "close dialog!" << endl;
      this->Close = true;
      this->close();
  }

string ManIntercept::GetCurrentProbCat()
{
    return this->currentProbCat;
}

string ManIntercept::GetCurrentCat()
{
      return this->currentCat;
}

int ManIntercept::GetCurrentID()
{
      return this->currentID;
}

bool ManIntercept::getClose()
{
    return this->Close;
}

bool ManIntercept::GetLeftWristOK()
{
    return this->LeftWristOK;
}

bool ManIntercept::GetRightWristOK()
{
    return this->RightWristOK;
}

bool ManIntercept::GetLeftElbowOK()
{
    return this->LeftElbowOK;
}

bool ManIntercept::GetRightElbowOK()
{
    return this->RightElbowOK;
}

void ManIntercept::LeftWristOKDialog()
{
    this->LeftWristOK = true;
}

void ManIntercept::RightWristOKDialog()
{
    this->RightWristOK = true;
}

void ManIntercept::LeftElbowOKDialog()
{
    this->LeftElbowOK = true;
}

void ManIntercept::RightElbowOKDialog()
{
    this->RightElbowOK = true;
}
