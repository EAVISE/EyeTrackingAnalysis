/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *Start;
    QPushButton *ManIntervent;
    QPushButton *SetWorkingDir;
    QLineEdit *InfoField;
    QPushButton *VideoProcess;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *StartFrame;
    QLineEdit *NrOfFrames;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(675, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Start = new QPushButton(centralWidget);
        Start->setObjectName(QStringLiteral("Start"));
        Start->setGeometry(QRect(10, 150, 151, 27));
        ManIntervent = new QPushButton(centralWidget);
        ManIntervent->setObjectName(QStringLiteral("ManIntervent"));
        ManIntervent->setGeometry(QRect(10, 180, 151, 27));
        SetWorkingDir = new QPushButton(centralWidget);
        SetWorkingDir->setObjectName(QStringLiteral("SetWorkingDir"));
        SetWorkingDir->setGeometry(QRect(10, 20, 151, 27));
        InfoField = new QLineEdit(centralWidget);
        InfoField->setObjectName(QStringLiteral("InfoField"));
        InfoField->setGeometry(QRect(180, 20, 371, 26));
        VideoProcess = new QPushButton(centralWidget);
        VideoProcess->setObjectName(QStringLiteral("VideoProcess"));
        VideoProcess->setGeometry(QRect(10, 120, 151, 27));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 60, 121, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 90, 121, 20));
        StartFrame = new QLineEdit(centralWidget);
        StartFrame->setObjectName(QStringLiteral("StartFrame"));
        StartFrame->setGeometry(QRect(170, 60, 91, 26));
        NrOfFrames = new QLineEdit(centralWidget);
        NrOfFrames->setObjectName(QStringLiteral("NrOfFrames"));
        NrOfFrames->setGeometry(QRect(170, 90, 91, 26));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 675, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        Start->setText(QApplication::translate("MainWindow", "Post-Process", 0));
        ManIntervent->setText(QApplication::translate("MainWindow", "Manual intervention", 0));
        SetWorkingDir->setText(QApplication::translate("MainWindow", "Set Working Dir", 0));
        VideoProcess->setText(QApplication::translate("MainWindow", "Process Video", 0));
        label->setText(QApplication::translate("MainWindow", "Enter start frame:", 0));
        label_2->setText(QApplication::translate("MainWindow", "Enter nr of frames:", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
