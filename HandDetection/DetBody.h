/*!
 * @class DetBody
 * Detects upperbodys in images
 *
 * @author Den Dooven Raphael
 *
 */

#ifndef DETBODY_H
#define DETBODY_H

#include "FFLD/SimpleOpt.h"
#include "FFLD/Intersector.h"
#include "FFLD/Mixture.h"
#include "FFLD/Scene.h"
#include "Detection.h"

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "opencv2/opencv.hpp"


class DetBody
{
    public:
        /// @brief Constructor from classe DetBody
        /// Prepares the detector with the right model
        /// @param ModelBody Filename of model
        /// @param threshold Threshold value of detector
        DetBody(String modelBody, double threshold);

        /// @brief Apply the detector on a image
        /// @param image The image for the detector
        /// return nothing, but final/best detection is stored in this->pos
        void runDetection(Mat image );

        /// @brief Strecth detection bouding box in order to cover all possible hand positions
        /// @param image The image for the detector        
        void CalculateCutOuts(Mat image);

        /// @brief Get all the strected bounding boxes
        /// @return All cutouts of type Mat put in a vector
        vector<Mat> getCutouts();

        /// @brief Get all the detection regions of the last detection
        /// @return All regions of type Rect put in a vector
        vector<Rect> getRect();

        /// @brief Get number of detections found
        /// @return Number of detections found of type integer
        int getSize();

        /// @brief Get height of the previous cutout
        /// @return height of previous cutout
        int getPrevHeight();

        /// @brief Get width of the previous cutout
        /// @return width of previous cutout
        int getPrevWidth();

        /// @brief Get number of torso predictions
        /// @return number of predictions
        int getPrediction();

        /// @brief set number of predictions
        /// @param number of predictions
        void setPrediction(int pred);

        /// @brief increase number of predictions by 1
        void increasePrediction();

        /// @brief decrease number of predictions by 1
        void decreasePrediction();

        void AddOffset(Point Offset,int index);

        void clearDetections();

        void UpdateDet(Rect New, int index);

        void SetHeight(int index, int value);

    private:        
        vector<UniqueDetections> FinalTorsoDetections;
        vector<UniqueDetections> FinalDetections;
        Mixture mixtureBody;
        String modelBody;

        vector<Mat> detections;
        vector<Rect> pos;

        double TH;        

        int prev_width_;
        int prev_height_;
        int prediction_;
};

#endif // DETBODY_H
