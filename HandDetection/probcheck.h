#ifndef PROBCHECK_H
#define PROBCHECK_H

//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"


#include "cvmatio-master/include/MatlabIO.hpp"
#include "cvmatio-master/include/MatlabIOContainer.hpp"

#include <stdio.h>

using namespace cv;
using namespace std;

class ProbCheck
{
public:
    ProbCheck();
    int Init(string path);
    double CheckPosition(Mat ProbMap, Point2f Ref, double norm, Point POI, Point2f Center, int side);
    double CheckPositionWrist(Mat ProbMap, Point2f Ref, double norm, Point POI, Point2f Center, int side);

    Mat GetProbMapElbow_right();
    Mat GetProbMapElbow_left();
    Mat GetProbMapWrist_right();
    Mat GetProbMapWrist_left();
    void SetProbMapElbow_right(Mat New);
    void setProbMapElbow_left(Mat New);
    void SetProbMapWrist_right(Mat New);
    void SetProbMapWrist_left(Mat New);
    Point2f GetCenterElbow_right();
    Point2f GetCenterElbow_left();
    Point2f GetCenterWrist_right();
    Point2f GetCenterWrist_left();
    void IncreaseCounter_L();
    void IncreaseCounter_R();
    double CalcAverage(double Brray[2], int side);
    double GetAverageLeft();
    double GetAverageRight();
    void SetAverageLeft(double value);
    void SetAverageRight(double value);
    void UpdateMap(Mat &Map, Point NewPoint);
    Point GetRelWristLeft();
    Point GetRelWristRight();
    Point GetRelElbowLeft();
    Point GetRelElbowRight();

private:
    Mat ProbElbow_Right_;
    Mat ProbElbow_Left_;
    Mat ProbWrist_Right_;
    Mat ProbWrist_Left_;
    Point2f CenterElbow_R_;
    Point2f CenterElbow_L_;
    Point2f CenterWrist_R_;
    Point2f CenterWrist_L_;
    double probabilty_;
    double AvgProb_L_[3] = {0,0,0};
    double AvgProb_R_[3] = {0,0,0};
    int index_L_;
    int index_R_;
    double Average_L_;
    double Average_R_;
    int AvgSize;

    Point RelPosLeftWrist_;
    Point RelPosRightWrist_;
    Point RelPosLeftElbow_;
    Point RelPosRightElbow_;
};

#endif // PROBCHECK_H
