#ifndef DETECTION_H
#define DETECTION_H
#include <iostream>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

/*!
	\file Declaration of the Detection class
*/

/*!
	This class represents a detection. It can also be used for annotations. This class forms the input and output for our detection/training-software. By making this format detector-independend we simplify the switching between different detectors

        \author F. De Smedt
        \date 2014
*/

class Detection
{

public:
    Detection();
    int getX() const;
    int getY() const;
    int getWidth() const;
    int getHeight() const;
    float getScore() const; 
    std::string getFilename() const;
    std::string getClass() const;
    double getAngle() const;
    cv::Scalar getColor() const;
    std::string getModelName() const;
    int getLevel() const;

    void setX(int x);
    void setY(int y);
    void setWidth(int width);
    void setHeight(int height);
    void setScore(float score);
    void setFilename(std::string filename);
    void setClass(std::string classname);
    void setAngle(double angle);
    void setColor(cv::Scalar color);
    void setModelName(std::string modelname);

private:
    int m_x,m_y,m_width,m_height;
    float m_score;
    double m_angle;
    std::string m_filename;
    std::string m_class;
    cv::Scalar m_color;
    std::string m_modelName;
    int m_level;
};

bool compareByScore(const Detection *a, const Detection *b);
void SortDetections(std::vector<Detection*> &Dets);

#endif // DETECTION_H
