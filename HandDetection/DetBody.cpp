
#include "DetBody.h"

/*!
    \file The implementation of functions used to work with the DetBody class
*/

/*!
    \brief DetBody constructor
    The constructor of the DetBody-class. The DetBody constructor requires 2 parameters
    \param modelBody location of the torso model file
    \param threshold minimum value of the detection score

    \author S. De Beugher
    \date 2015
*/
DetBody::DetBody(String modelBody, double threshold)
{
    // initialization: open mixtureBody + initialize patchwork class + calculate filter transform
    int res = Init(mixtureBody, modelBody);

    if(res == -1) {
        exit(1);
    }

    this->TH = threshold;
    this->prev_height_ = 0;
    this->prev_width_ = 0;
    this->prediction_ = 0;
}

void DetBody::runDetection(Mat image)
{
    Mat detImage;
    detImage = image.clone();

    // update previous height/width variables
    if(this->getCutouts().size()>0)
    {
        this->prev_height_ = this->getCutouts()[0].rows;
        this->prev_width_ = this->getCutouts()[0].cols;
    }

    // clear vectors
    this->detections.clear();
    this->pos.clear();
    this->FinalDetections.clear();

    vector<DetectionFFLD> detections;    
    int padding = 6;
    int interval = 5;
    double overlap = 0.50;      // Minimum overlap in nms

    // Convert OpenCv image to JPEGImage
    JPEGImage imageTorso(detImage);

    // Create Image Pyramid
    HOGPyramid pyramid(imageTorso, padding, padding, interval);

    // Perform detection
    detect(mixtureBody, imageTorso.width(), imageTorso.height(), pyramid, TH, overlap,detections);

    // OLD: detect(mixtureBody, imageTorso.width(), imageTorso.height(), pyramid, TH, overlap, detections,ratiotest);
    vector<UniqueDetections>FinalTorsoDetections;

    // Convert detections to other format
    ConvertFormat(detections,FinalTorsoDetections);
    double score=-999;
    int index = 0;

    // Find detection with best score:
    for(unsigned int k = 0; k < FinalTorsoDetections.size(); k++) {         
        if(FinalTorsoDetections[k].BestScore>score)
        {
            score = FinalTorsoDetections[k].BestScore;
            index = k;
        }
    }

    // put best detection in detection vector...
    if(FinalTorsoDetections.size()>0)
    {
        this->pos.push_back(FinalTorsoDetections[index].detect);
    }    
}

void DetBody::CalculateCutOuts(Mat image)
{
    for(int n=0;n<this->pos.size();n++)
    {       
        if(this->pos[n].width == this->pos[n].height)
        {
            this->pos[n].height = this->pos[n].width*1.81;
        }

        // Stretching bounding box
        this->pos[n].y = (this->pos[n].y) - ( this->pos[n].height * .5 );
        this->pos[n].x = (this->pos[n].x) - ( this->pos[n].width * 1.25);

        this->pos[n].width = (this->pos[n].width *3.5);
        this->pos[n].height = (this->pos[n].height *1.8);

        // Secure an in image coutout
        if(this->pos[n].height<0)
            this->pos[n].height = 0;

        if(this->pos[n].width<0)
            this->pos[n].width = 0;

        if(this->pos[n].x>image.cols)
            this->pos[n].x = image.cols-5;

        if(this->pos[n].y>image.rows)
            this->pos[n].y = image.rows-5;

        if(this->pos[n].x < 0) {
            this->pos[n].width = this->pos[n].width + this->pos[n].x;
            this->pos[n].x = 0;
        }

        if(this->pos[n].y <0) {
            this->pos[n].height = this->pos[n].height + this->pos[n].y;
            this->pos[n].y = 0;
        }

        if(this->pos[n].x + this->pos[n].width > image.cols)  {
            this->pos[n].width = image.cols - this->pos[n].x;
        }

        if(this->pos[n].y + this->pos[n].height > image.rows)  {
            this->pos[n].height = image.rows - this->pos[n].y;
        }        

        // Cutout
        this->detections.push_back(image(this->pos[n]).clone());
    }
}

vector<Mat> DetBody::getCutouts()
{
    return this->detections;
}

vector<Rect> DetBody::getRect()
{
    return this->pos;
}

int DetBody::getSize()
{
    return this->detections.size();
}

int DetBody::getPrevWidth()
{
    return this->prev_width_;
}

int DetBody::getPrevHeight()
{
    return this->prev_width_;
}

int DetBody::getPrediction()
{
    return this->prediction_;
}

void DetBody::setPrediction(int pred)
{
    this->prediction_ = pred;
}

void DetBody::increasePrediction()
{
    this->prediction_ = this->prediction_+1;
}

void DetBody::decreasePrediction()
{
    if(this->prediction_>0)
    {
        this->prediction_ = this->prediction_-1;
    }
}

void DetBody::AddOffset(Point Offset,int index)
{
    this->pos[index].x += Offset.x;
    this->pos[index].y += Offset.y;
}

void DetBody::clearDetections()
{
    this->pos.clear();
    this->detections.clear();
}

void DetBody::UpdateDet(Rect New, int index)
{
    this->pos[index].x = New.x;
    this->pos[index].y = New.y;
    this->pos[index].width = New.width;
    this->pos[index].height = New.height;
}

void DetBody::SetHeight(int index, int value)
{
    this->pos[index].height = value;
}
