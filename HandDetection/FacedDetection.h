#ifndef FACEDDETECTION_H
#define FACEDDETECTION_H

#include <iostream>

#include "opencv2/opencv.hpp"

using namespace cv;
using namespace std;

class FaceDetection
{
    public:
        FaceDetection(string faceModelFile);

        Rect detectFace(Mat In);

        int getPrevHeight();
        int getPrevWidth();
        int getPrediction();
        void setPrediction(int pred);
        void increasePrediction();
        void decreasePrediction();

        void setValid(int valid);
        int getValid();
        void increaseValid();
        void decreaseValid();

    private:
        CascadeClassifier face_cascade;

        Rect face;
        int prev_width_;
        int prev_height_;
        int prediction_;
        int valid_;
};

#endif // FACEDDETECTION_H
