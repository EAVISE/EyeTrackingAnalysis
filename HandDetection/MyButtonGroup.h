//MyButtonGroup.h File

#ifndef MYBUTTONGROUP_H
#define MYBUTTONGROUP_H

#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QMessageBox>

//Derived Class from QButtonGroup
class MyButtonGroup: public QButtonGroup
{
  Q_OBJECT
  public:
    MyButtonGroup(QWidget* parent)
    {
      this->setParent(parent);

      //connect buttonClicked signal to our custom slot 'buttonClick'
      connect(this , SIGNAL(buttonClicked(int)),this, SIGNAL(WhichButton(int)));

    };
    ~ MyButtonGroup(){};

signals:
    void WhichButton(int id);
};
#endif // MYBUTTONGROUP_H
