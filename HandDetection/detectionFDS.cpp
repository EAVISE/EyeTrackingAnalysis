#include "detectionFDS.h"
/*! 
	\file The implementation of functions used to work with the detection class

*/

/*!
	\brief Helper function to sort detections

	This is a helper function to sort detections based on the score they have
	\param a a pointer to a detection
	\param b a pointer to an other detection
	\return 1 of the score of a is higher, 0 otherwise
        
	\author F. De Smedt
        \date 2014
*/
bool compareByScore(const Detection *a, const Detection *b)
{
    return a->getScore() > b->getScore(); //sort from highto low
}

/*!
	\brief Sort the detections based on score

	This function sorts the detection based on score they have. The compareByScore function is used here to evaluate which score is higher. 
        
	\author F. De Smedt
        \date 2014
*/
void SortDetections(std::vector<Detection*> &Dets) {
    std::sort(Dets.begin(),Dets.end(),compareByScore);

}

/*! \brief Detection constructor

	The constructor of the Detection-class. Nothing has to be done at construction time
        
	\author F. De Smedt
        \date 2014
*/
Detection::Detection()
{
}

int Detection::getX() const
        {
                return this->m_x;
        }
    int Detection::getY() const
        {
                return this->m_y;
        }
    int Detection::getWidth() const
        {
                return this->m_width;
        }
    int Detection::getHeight() const
        {
                return this->m_height;
        }
    float Detection::getScore() const
        {
                return this->m_score;
        }
    std::string Detection::getFilename() const
        {
                return this->m_filename;
        }
    std::string Detection::getClass() const
        {
                return this->m_class;
        }

    double Detection::getAngle() const {
        return this->m_angle;
    }

    cv::Scalar Detection::getColor() const
        {
                return this->m_color;
        }
    std::string Detection::getModelName() const
        {
                return this->m_modelName;
        }
    int Detection::getLevel() const
        {
                return this->m_level;
        }
    void Detection::setX(int x){
        this->m_x = x;
        }
    void Detection::setY(int y){
        this->m_y = y;
        }
    void Detection::setWidth(int width){
        this->m_width = width;
        }
    void Detection::setHeight(int height){
        this->m_height = height;
        }
    void Detection::setScore(float score){
        this->m_score = score;
        }
    void Detection::setFilename(std::string filename){
        this->m_filename = filename;
        }
    void Detection::setClass(std::string classname){
        this->m_class = classname;
        }

    void Detection::setAngle(double angle){
        this->m_angle = angle;
    }

void Detection::setColor(cv::Scalar color){
        this->m_color = color;
        }
    void Detection::setModelName(std::string modelname){
        this->m_modelName = modelname;
        }

