/****************************************************************************
** Meta object code from reading C++ file 'manintercept.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "manintercept.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'manintercept.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ManIntercept_t {
    QByteArrayData data[11];
    char stringdata0[139];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ManIntercept_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ManIntercept_t qt_meta_stringdata_ManIntercept = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ManIntercept"
QT_MOC_LITERAL(1, 13, 10), // "UpdateList"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "NewItem"
QT_MOC_LITERAL(4, 33, 14), // "UpdateProbList"
QT_MOC_LITERAL(5, 48, 4), // "Item"
QT_MOC_LITERAL(6, 53, 11), // "CloseDialog"
QT_MOC_LITERAL(7, 65, 17), // "LeftWristOKDialog"
QT_MOC_LITERAL(8, 83, 18), // "RightWristOKDialog"
QT_MOC_LITERAL(9, 102, 17), // "LeftElbowOKDialog"
QT_MOC_LITERAL(10, 120, 18) // "RightElbowOKDialog"

    },
    "ManIntercept\0UpdateList\0\0NewItem\0"
    "UpdateProbList\0Item\0CloseDialog\0"
    "LeftWristOKDialog\0RightWristOKDialog\0"
    "LeftElbowOKDialog\0RightElbowOKDialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ManIntercept[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    1,   52,    2, 0x0a /* Public */,
       6,    0,   55,    2, 0x0a /* Public */,
       7,    0,   56,    2, 0x0a /* Public */,
       8,    0,   57,    2, 0x0a /* Public */,
       9,    0,   58,    2, 0x0a /* Public */,
      10,    0,   59,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ManIntercept::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ManIntercept *_t = static_cast<ManIntercept *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->UpdateList((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->UpdateProbList((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->CloseDialog(); break;
        case 3: _t->LeftWristOKDialog(); break;
        case 4: _t->RightWristOKDialog(); break;
        case 5: _t->LeftElbowOKDialog(); break;
        case 6: _t->RightElbowOKDialog(); break;
        default: ;
        }
    }
}

const QMetaObject ManIntercept::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ManIntercept.data,
      qt_meta_data_ManIntercept,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ManIntercept::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ManIntercept::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ManIntercept.stringdata0))
        return static_cast<void*>(const_cast< ManIntercept*>(this));
    return QDialog::qt_metacast(_clname);
}

int ManIntercept::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
