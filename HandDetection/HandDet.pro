#-------------------------------------------------
#
# Project created by QtCreator 2017-06-22T09:43:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HandDet
TEMPLATE = app

QMAKE_CXXFLAGS += -fopenmp

LIBS += `pkg-config opencv --libs`		# so files
LIBS += -fopenmp
LIBS += -L/usr/lib/x86_64-linux-gnu/ -ljpeg
LIBS += -L/usr/lib/x86_64-linux-gnu/ -lfftw3f
LIBS += -L/usr/lib/x86_64-linux-gnu/ -lxml2
LIBS += -L /usr/local/lib/ -lcvmatio

INCLUDEPATH += -I "/usr/local/include/opencv2" 	# headers
INCLUDEPATH += -I "/usr/include/libxml2/"
#INCLUDEPATH += /usr/local/include/MatIO/ mqk

SOURCES += main.cpp\
        mainwindow.cpp \
    DetBody.cpp \
    Detection.cpp \
    FFLD/HOGPyramid.cpp \
    FFLD/JPEGImage.cpp \
    FFLD/LBFGS.cpp \
    FFLD/Mixture.cpp \
    FFLD/Model.cpp \
    FFLD/Object.cpp \
    FFLD/Patchwork.cpp \
    FFLD/Rectangle.cpp \
    FFLD/Scene.cpp \
    hand.cpp \
    Joint.cpp \
    KalmanDHU.cpp \
    manintercept.cpp \
    mouse.cpp \
    OpenXml.cpp \
    SaveDetections.cpp \
    processthread.cpp \
    probcheck.cpp \
    LoadDetections.cpp \
    detectionFDS.cpp \
    FaceDetection.cpp \
    DetHand.cpp \
    Detector_VJ.cpp \
    Detector.cpp \
    detectionDHU.cpp

HEADERS  += mainwindow.h \
    DetBody.h \
    Detection.h \
    FFLD/HOGPyramid.h \
    FFLD/Intersector.h \
    FFLD/JPEGImage.h \
    FFLD/LBFGS.h \
    FFLD/Mixture.h \
    FFLD/Model.h \
    FFLD/Object.h \
    FFLD/Patchwork.h \
    FFLD/Rectangle.h \
    FFLD/Scene.h \
    FFLD/SimpleOpt.h \
    hand.h \
    Joint.h \
    KalmanDHU.h \
    manintercept.h \
    mouse.h \
    OpenXml.h \
    SaveDetections.h \
    MyButtonGroup.h \
    processthread.h \
    probcheck.h \
    LoadDetections.h \
    detectionFDS.h \
    FacedDetection.h \
    DetHand.h \
    Detector_VJ.h \
    Detector.h \
    detectionDHU.h

FORMS    += mainwindow.ui
