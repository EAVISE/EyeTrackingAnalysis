#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QXmlStreamReader>

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "SaveDetections.h"
#include "KalmanDHU.h"
#include "OpenXml.h"
#include "Joint.h"
#include "hand.h"
#include "manintercept.h"
#include "mouse.h"
#include "LoadDetections.h"


using namespace std;
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    cout << "main window..." << endl;
    this->manIntercept = 0;
    this->CurrentFrame = 0;
    this->DisplaceTH = 39;	//50 NP

    this->initKalmanFace = true;
    this->avgFaceWidth = 0;
    this->facePredictions = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Start_clicked()
{
    //=======================================================================================================//
    // read probability maps
    int ret = ProbTest.Init("../ProbabilitiesNew.mat");
    cout << "return value of probability init = " << ret << endl;
    //=======================================================================================================//
    // read Detections File (Raphael format)
    cout << "---> Reading data from detection xml file." << endl;
    string AllDetections;
    AllDetections = this->WorkFolder_+"AllDetections.xml";

    OpenXml xml(AllDetections);
    xml.XmlToData();

    cout << "---> Those detections belongs to this avi file: " << xml.filenameAvi << endl;
    cout << "---> First frame = " << xml.frameNr[0] << endl;
    cout << "---> Last frame = " << xml.frameNr[xml.frameNr.size()-1] << endl;

    this->startframe = xml.frameNr[0];

    //=======================================================================================================//
    // Initialization, init variables
    Mat cameraImage,DrawImage, Skeleton;
    char fileName[200];

    vector<Vec4i> Det;

    this->ManLeft=true;
    this->ManRight=true;
    this->ManJointLeft = false;
    this->ManJointRight=false;    

    initKalJointL = false;
    initKalJointR = false;

    int maxKalmanPredict =5;    //  5

    SaveDetections* save;
    save = new SaveDetections("test","FinalDet.xml");

    KalmanDHU Kalman_Face;
    vector<StructDetections> Structure;

    this->manInterceptFrame = 0;
    // ========================================================================================================//
    // loop over all frames
    for(int i=0 ;i< xml.frameNr.size()-1;i++)
    {                
        sprintf(fileName, "%s%05d%s", this->WorkFolder_.c_str(),xml.frameNr[i],".png");

        cout << "======================================================================================" << endl;
        cout << "image path 2 = " << fileName << endl;

        cout << "Value of keep left hand = " << this->KeepLeftHand << endl;
        cout << "Value of keep left joint = " << this->KeepLeftJoint << endl;
        cout << "Value of keep right hand = " << this->KeepRightHand << endl;
        cout << "Value of keep right joint = " << this->KeepRightJoint << endl;

        // Structure element for saving the detections...
        StructDetections StructElement;

        bool MaxPredExceed = false;

        // READ IMAGE
        cameraImage = imread(fileName);
        if(cameraImage.empty())
            break;
        else
            cout << "image dimensions: " << cameraImage.cols << " " << cameraImage.rows << endl;

        //=======================================================================================================//
        // Clone input image + make mask of the same size
        DrawImage = cameraImage.clone();
        Skeleton = cameraImage.clone();
        Mat CandidatesWindow = cameraImage.clone();
        this->CurrentFrame = xml.frameNr[i];

        //=======================================================================================================//
        // draw frame number
        Point org;
        org.x = 100;
        org.y = 100;
        char id[200];
        sprintf(id,"%d",this->CurrentFrame);
        putText(Skeleton, id, org,2,2, Scalar(0,0,255));

        //=======================================================================================================//
        // save frame number        
        StructElement.FrameNr = this->CurrentFrame;

        //=======================================================================================================//
        // Get the Torso detection of this frame from the XML File
        Rect Torso;
        Torso = xml.upperBody[i][0];
        rectangle(Skeleton,Torso,Scalar(185,255,100),1);        
        StructElement.UB = Torso;

        //=======================================================================================================//
        // Get the Face detection of this frame from the XML File
        Kalman_Face.predictKalman();
        Rect Face;
        Face = xml.face[i];
        cout << "Face x = " << Face.x << " | Face y = " << Face.y << " | Face width = " << Face.width << " | Face Height = " << Face.height << endl;

        Point FacePoint(Face.x,Face.y);
        Kalman_Face.predictKalman();

        float faceScore = xml.facescore[i][0];

        // if needed, initialize the face kalman
        if(Face.width>0 && initKalmanFace)       // INIT KALMAN
        {
            Kalman_Face.Init(FacePoint);
            avgFaceWidth = Face.width;
        }

        // in case there was a face detection, update the prediction
        if(Face.width!=0)
        {
            cout << "do kalman face correction with detection: " << FacePoint << endl;

            Kalman_Face.correctKalman(FacePoint);
            Face.x = Kalman_Face.getCorrX();
            Face.y = Kalman_Face.getCorrY();
            Point NewFacePoint(Face.x,Face.y);
            circle(Skeleton,NewFacePoint,5,Scalar(255,0,0),1);       // BLUE
            rectangle(Skeleton,Face,Scalar(255,0,0),1);
            avgFaceWidth = 0.3*Face.width+ 0.7*avgFaceWidth;

            if(facePredictions>0)
                facePredictions--;
        }
        // in case the was no face detection and the number of predictions < 3, do a prediction for the face
        else if(facePredictions<3)
        {
            cout << "do kalman face correction with prediction: " << Kalman_Face.getPreXY()<< endl;
            Kalman_Face.correctKalman(Kalman_Face.getPreXY());
            Face.x = Kalman_Face.getCorrX();
            Face.y = Kalman_Face.getCorrY();
            Face.width = avgFaceWidth;
            Face.height = avgFaceWidth;
            Point NewFacePoint(Face.x,Face.y);
            circle(Skeleton,NewFacePoint,5,Scalar(0,255,0),1);       // GREEN
            rectangle(Skeleton,Face,Scalar(0,255,0),1);
            facePredictions++;
        }
        else
        {
            initKalmanFace = true;
        }

        // if there was a face detection, update the displacement TH (used in the next steps)
        if(Face.width>0)
        {
            this->DisplaceTH = Face.width*0.75;
        }
        cout << "Distance TH = " << this->DisplaceTH << endl;

        StructElement.Face = Face;
        StructElement.FaceScore = 0;

        if(i==0)
        {
            Point ImageCenter(320,240);
            Kal_Left.Init(ImageCenter);
            Kal_Right.Init(ImageCenter);
            Kal_Joint_Left.Init(ImageCenter);
            Kal_Joint_Right.Init(ImageCenter);
        }

        //=======================================================================================================//
        // Get Hand detections of this frame from the XML file
        cout << "nr of Detections: " << xml.arm[i].size()<< endl;
        for(int item=0;item<xml.arm[i].size();item++)
        {
            Point P1,P2;
            P1.x = xml.arm[i][item][0];
            P1.y = xml.arm[i][item][1];
            P2.x = xml.arm[i][item][2];
            P2.y = xml.arm[i][item][3];
            Det.push_back(xml.arm[i][item]);
            line(Skeleton,P1,P2,Scalar(255,5,125),1);
            line(CandidatesWindow,P1,P2,Scalar(255,5,125),1);
            circle(CandidatesWindow,P1,5,Scalar(255.255,0),1);
            circle(CandidatesWindow,P2,5,Scalar(255.255,0),1);
            cout << " \tcandidate: " <<  item << " P1 = " << P1 << " | P2 = " << P2 << endl;
        }

        //=======================================================================================================//
        // do kalman prediction
        this->Kal_Left.predictKalman();
        this->Kal_Right.predictKalman();        
        Kal_Joint_Left.predictKalman();
        Kal_Joint_Right.predictKalman();

        circle(Skeleton,Kal_Left.getPreXY(),10,Scalar(255,255,0),2);
        circle(Skeleton,Kal_Right.getPreXY(),10,Scalar(255,255,0),2);

        circle(Skeleton,Kal_Joint_Left.getPreXY(),6,Scalar(0,255,255),2);
        circle(Skeleton,Kal_Joint_Right.getPreXY(),6,Scalar(0,255,255),2);

        cout << "---------------------------------------------------------" << endl;
        cout << "prediction left hand = " << this->Kal_Left.getPreXY() << endl;
        cout << "prediction right hand = " << this->Kal_Right.getPreXY() << endl;

        cout << "prediction left joint = " << Kal_Joint_Left.getPreXY() << endl;
        cout << "prediction right joint = " << Kal_Joint_Right.getPreXY() << endl;
        cout << "---------------------------------------------------------" << endl;

        //=======================================================================================================//
        // Find detection closest to prediction right
        if(!Kal_Right.GetInit() && Det.size()>=2)
        {
            cout << "Get candidates for right hand:" << endl;
            HandRight.GetCandidates(Kal_Right.getPreXY(),Det);

            HandRight.SetHand(HandRight.GetBest()[0],HandRight.GetBest()[1]);
            HandRight.SetUsedDist(HandRight.GetBestDist());            
            JointRight.SetJoint(HandRight.GetBest()[2],HandRight.GetBest()[3]);
        }

        //=======================================================================================================//
        // Find detection closest to prediction left
        if(!this->Kal_Left.GetInit() && Det.size()>=2)
        {
            cout << "Get candidates for left hand:" << endl;
            HandLeft.GetCandidates(this->Kal_Left.getPreXY(),Det);

            HandLeft.SetHand(HandLeft.GetBest()[0],HandLeft.GetBest()[1]);
            HandLeft.SetUsedDist(HandLeft.GetBestDist());            
            JointLeft.SetJoint(HandLeft.GetBest()[2],HandLeft.GetBest()[3]);
        }

        //=======================================================================================================//
        // Check if the same line was chosen for both hands
        if(HandLeft.GetBestIndex() == HandRight.GetBestIndex() )
        {
            // Select second best for weakest candidate
            if(HandLeft.GetBestDist()<HandRight.GetBestDist())
            {
                // choose second best candidate for right hand                
                HandRight.SetHand(HandRight.GetSecBest()[0],HandRight.GetSecBest()[1]);
                HandRight.SetUsedDist(HandRight.GetSecBestDist());                
                JointRight.SetJoint(HandRight.GetSecBest()[2],HandRight.GetSecBest()[3]);
                cout << "choose second best for right hand: " << HandRight.GetHand() << endl;
            }
            else
            {
                // choose second best candidate for left hand
                HandLeft.SetHand(HandLeft.GetSecBest()[0],HandLeft.GetSecBest()[1]);
                HandLeft.SetUsedDist(HandLeft.GetSecBestDist());                
                JointLeft.SetJoint(HandLeft.GetSecBest()[2],HandLeft.GetSecBest()[3]);
                cout << "choose second best for left hand: " << HandLeft.GetHand() << endl;
            }            
        }

        cout << "---------------------------------------------------------" << endl;
        cout << "Hand right = " << HandRight.GetHand() << endl;
        cout << "Joint right = " << JointRight.GetJoint() << endl;
        cout << "Hand left = " << HandLeft.GetHand() << endl;
        cout << "Joint left = " << JointLeft.GetJoint() << endl;
        cout << "init joint left = " << initKalJointL << endl;
        cout << "init joint right = " << initKalJointR << endl;
        cout << "---------------------------------------------------------" << endl;

        if(initKalJointL && JointLeft.GetX()!=0 && JointLeft.GetY()!=0)
        {
            // init Joint Left kalman
            cout << "UPDATE JOINT LEFT KALMAN!!" << endl;
            UpdateJoint(this->Kal_Joint_Left,JointLeft,JointLeft.GetJoint());
            initKalJointL = false;
        }

        //if(initKalJointR && JointRight.GetX()!=0 && JointRight.GetY()!=0)
        if(initKalJointR && JointRight.GetX()!=0 && JointRight.GetY()!=0 && HandRight.GetUsedDist()<this->DisplaceTH)
        {
            // init Joint Right kalman
            cout << "UPDATE JOINT RIGHT KALMAN!!" << endl;
            cout << "\tvalue of initKalJointR = " << initKalJointR << endl;
            cout << "\tvalue of JointR = " << JointRight.GetJoint() << endl;
            cout << "\tvalue of used dist = " << HandRight.GetUsedDist() << endl;
            UpdateJoint(this->Kal_Joint_Right,JointRight,JointRight.GetJoint());
            initKalJointR = false;
        }

        //=======================================================================================================//
        // Check displacement of Right Hand
        cout << "Displacement of Right hand" << HandRight.GetHand() << " = " << HandRight.GetUsedDist() << endl;
        if(HandRight.GetUsedDist()>this->DisplaceTH)
        {
            if(JointRight.GetJoint().x>0 && JointRight.GetJoint().x>0 && JointRight.CheckDisplacement(Kal_Joint_Right.getPreXY(),this->DisplaceTH))
            {
                   cout << "Try right hand detection with co " << HandRight.GetHand() << endl;
            }
            else
            {
                HandRight.SetHand(0,0);
                cout << "\tDistance between current and previous right hand is too large --> clear right hand" << endl;
            }
        }

        //=======================================================================================================//
         // Check displacement of Left Hand
        cout << "Displacement of Left hand" << HandLeft.GetHand() << " = " << HandLeft.GetUsedDist() << endl;
        //circle(Skeleton,HandLeft.GetHand(),3,Scalar(255,255,255),2);
        if(HandLeft.GetUsedDist()>this->DisplaceTH)
        {
            if(JointLeft.GetJoint().x>0 && JointLeft.GetJoint().x>0 && JointLeft.CheckDisplacement(Kal_Joint_Left.getPreXY(),this->DisplaceTH))
            {
                   cout << "Try left hand detection with co " << HandLeft.GetHand() << endl;
            }
            else
            {
                HandLeft.SetHand(0,0);
                cout << "\tDistance between current and previous left hand is too large --> clear left hand" << endl;
            }
        }

        //=======================================================================================================//
        // check joint displacement...
        cout << "displacement of left joint = " << endl;
        if(!JointLeft.CheckDisplacement(Kal_Joint_Left.getPreXY(),this->DisplaceTH))
        {
            cout << "\t --> displacement between current and predicted left joint is too large!" << endl;
            JointLeft.SetJoint(0,0);
        }

        cout << "displacement of right joint = " << endl;
        if(!JointRight.CheckDisplacement(Kal_Joint_Right.getPreXY(),this->DisplaceTH))
        {
            cout << "\t displacement between current and predicted right joint is too large!" << endl;
            JointRight.SetJoint(0,0);
        }
        cout << "displace TH = " << this->DisplaceTH << endl;
        cout << "---------------------------------------------------------" << endl;

        //=======================================================================================================//
        // Do correction for Right detection:
        cout << " # predicitions Right " << HandRight.getPrediction() << endl;
        if(HandRight.NotEmpty())
        {
            cout << "do right hand correction with co " << HandRight.GetHand() << endl;
            this->Kal_Right.correctKalman(HandRight.GetHand());
            HandRight.SetHand(this->Kal_Right.getCorrXY());
            HandRight.decreasePrediction();            
        }        
        else if(HandRight.getPrediction()<=maxKalmanPredict)
        {
            cout << "do right hand correction with prediction: " << Kal_Right.getPreXY() << endl;
            this->Kal_Right.correctKalman(this->Kal_Right.getPreXY());
            HandRight.SetHand(this->Kal_Right.getCorrXY());
            HandRight.increasePrediction();            
        }
        else
        {
            ManRight = true;
            MaxPredExceed = true;
        }

        //=======================================================================================================//
        // Do correction for Left detection:        
        cout << " # predicitions Left " << HandLeft.getPrediction() << endl;
        if(HandLeft.NotEmpty())
        {
            cout << "do left hand correction with co " << HandLeft.GetHand() << endl;
            this->Kal_Left.correctKalman(HandLeft.GetHand());
            HandLeft.SetHand(this->Kal_Left.getCorrXY());            
            HandLeft.decreasePrediction();            
        }        
        else if(HandLeft.getPrediction()<=maxKalmanPredict)
        {
            cout << "do left hand correction with prediction: " << Kal_Left.getPreXY() << endl;
            this->Kal_Left.correctKalman(this->Kal_Left.getPreXY());
            HandLeft.SetHand(this->Kal_Left.getCorrXY());

            HandLeft.increasePrediction();

            if(this->KeepLeftHand)
            {
                cout << "------------------> KEEP HAND FIXED!" << endl;
                Update(Kal_Left, HandLeft,CurrentFrame,HoldHandLeft);
            }
        }
        else
        {
            ManLeft = true;
            MaxPredExceed = true;
        }

        //=======================================================================================================//
        // Do correction for Left Joint
        cout << " # predicitions Left Joint" << JointLeft.getPrediction() << endl;
        if(JointLeft.GetX()!=0 && JointLeft.GetY()!=0)
        {
            cout << "do left joint correction with co " << JointLeft.GetJoint() << endl;
            Kal_Joint_Left.correctKalman(JointLeft.GetJoint());
            JointLeft.SetJoint(Kal_Joint_Left.getCorrXY());
            JointLeft.decreasePrediction();
        }
        else if(JointLeft.getPrediction()<=maxKalmanPredict)
        {
            cout << "do left joint correction with prediction: ";
            Kal_Joint_Left.correctKalman(Kal_Joint_Left.getPreXY());
            JointLeft.SetJoint(Kal_Joint_Left.getCorrXY());
            JointLeft.increasePrediction();
            cout << Kal_Joint_Left.getCorrXY() << endl;

            if(this->KeepLeftJoint)
            {
                UpdateJoint(this->Kal_Joint_Left,JointLeft,HoldJointLeft);
            }
        }
        else
        {
            ManJointLeft = true;
            ManLeft = true;
            MaxPredExceed = true;
        }

        //=======================================================================================================//
        // Do correction for Right Joint.
        cout << " # predicitions Right Joint" << JointRight.getPrediction() << endl;
        if(JointRight.GetX()!=0 && JointRight.GetY()!=0)
        {
            cout << "do right joint correction with co " << JointRight.GetJoint() << endl;
            Kal_Joint_Right.correctKalman(JointRight.GetJoint());
            JointRight.SetJoint(Kal_Joint_Right.getCorrXY());
            JointRight.decreasePrediction();
        }
        else if(JointRight.getPrediction()<=maxKalmanPredict)
        {
            cout << "do right joint correction with prediction: " <<  Kal_Joint_Right.getCorrXY() << endl;
            Kal_Joint_Right.correctKalman(Kal_Joint_Right.getPreXY());
            JointRight.SetJoint(Kal_Joint_Right.getCorrXY());
            JointRight.increasePrediction();
        }
        else
        {
            ManJointRight = true;
            ManRight = true;
            MaxPredExceed = true;
        }

        // plot stick-men       
        if(faceScore>0)
        {
            this->SetPrevScore(faceScore);
        }
        else
        {
            // use previous score...
            faceScore = this->GetPrevScore();
        }

        cout << "Face area: " << Face.area() << endl;
        cout << "face score = " << faceScore << endl;

        Point2f ShoulderR,ShoulderL,FaceBot;
        if(Face.area()>0)
        {
            if(faceScore<1000)
            {
                // left face
                // shoulder right = X face
                ShoulderR.x = Face.x;
                ShoulderR.y = Face.y + 1.2*Face.height;

                ShoulderL.x = Face.x + 1.5*Face.width;
                ShoulderL.y = Face.y + 1.2*Face.height;

                FaceBot.x = Face.x+Face.width/2;
                FaceBot.y = Face.y + 1.2*Face.height;
            }

            if(faceScore>1000&&faceScore<2000)
            {
                // right face
                // shoulder right = X face
                ShoulderR.x = Face.x-1.5*Face.width;
                ShoulderR.y = Face.y + 1.2*Face.height;

                // shoulder left = X face + face.width/2
                ShoulderL.x = Face.x+Face.width;
                ShoulderL.y = Face.y + 1.2*Face.height;

                FaceBot.x = Face.x+Face.width/2;
                FaceBot.y = Face.y + 1.2*Face.height;
            }

            if(faceScore>=2000)
            {
                // frontal face
                ShoulderR.x = Face.x - 0.4*Face.width;
                ShoulderR.y = Face.y + 1.3*Face.height;

                // shoulder left = X face + face.width/2
                ShoulderL.x = Face.x + 1.4*Face.width;
                ShoulderL.y = Face.y + 1.3*Face.height;

                FaceBot.x = Face.x+Face.width/2;
                FaceBot.y = Face.y + 1.3*Face.height;
            }

            Point FaceDot(Face.x+Face.width/2,Face.y+Face.height/2);
            circle(Skeleton,FaceDot,3,Scalar(0,0,255),2);

            line(Skeleton,FaceDot,FaceBot,Scalar(255,0,55),2);

            circle(Skeleton,ShoulderR,3,Scalar(0,0,255),2);
            line(Skeleton,FaceBot,ShoulderR,Scalar(255,0,55),2);

            circle(Skeleton,ShoulderL,3,Scalar(0,0,255),2);
            line(Skeleton,FaceBot,ShoulderL,Scalar(255,0,55),2);

            line(Skeleton,ShoulderR,JointRight.GetJoint(),Scalar(255,0,55),2);
            line(Skeleton,ShoulderL,JointLeft.GetJoint(),Scalar(255,0,55),2);

            line(Skeleton,HandRight.GetHand(),JointRight.GetJoint(),Scalar(255,0,55),2);
            line(Skeleton,HandLeft.GetHand(),JointLeft.GetJoint(),Scalar(255,0,55),2);

            double Dist_Norm = norm((ShoulderR-ShoulderL));               // calculate normalization distance
            cout << "Norm Dist = " << Dist_Norm << endl;

            double ProbElbowRight = ( ProbTest.CheckPosition(ProbTest.GetProbMapElbow_right(),ShoulderR,Dist_Norm,JointRight.GetJoint(),ProbTest.GetCenterElbow_right(),1) );
            cout << " --> Prob R = " << ProbElbowRight << " | Average Prob = " << ProbTest.GetAverageRight() << endl;

            double ProbElbowLeft = (ProbTest.CheckPosition(ProbTest.GetProbMapElbow_left(),ShoulderL,Dist_Norm,JointLeft.GetJoint(),ProbTest.GetCenterElbow_left(),0) );
            cout << " --> Prob L = " << ProbElbowLeft << " | Average Prob = " << ProbTest.GetAverageLeft() << endl;

            double ProbWristRight = ProbTest.CheckPositionWrist(ProbTest.GetProbMapWrist_right(),ShoulderR,Dist_Norm,JointRight.GetJoint(),ProbTest.GetCenterWrist_right(),1);
            cout << " --> Prob wrist right = " << ProbWristRight << endl;

            double ProbWristLeft = ProbTest.CheckPositionWrist(ProbTest.GetProbMapWrist_left(),ShoulderL,Dist_Norm,JointLeft.GetJoint(),ProbTest.GetCenterWrist_left(),0);
            cout << " --> Prob wrist left = " << ProbWristLeft << endl;           

            this->Probcategories.clear();

            int probTH = 5; //5
            int avgTh = 2*probTH; //10

            if( (ProbWristLeft>probTH || ProbElbowLeft>probTH || ProbTest.GetAverageLeft() > avgTh ) && ProbWristLeft > 0.01)
            {
                cout << "probabilities left are between boundaries... " << endl;
            }
            else
            {
                this->ManLeft = true;
                this->ManJointLeft = true;
                this->Probcategories.push_back("Prob Left Wrist OK");
                this->Probcategories.push_back("Prob Left Elbow OK");
            }

            if( (ProbWristRight>probTH || ProbElbowRight>probTH || ProbTest.GetAverageRight() > avgTh ) && ProbWristRight>0.01)
            {
                cout << "probabilities right are between boundaries... " << endl;
            }
            else
            {
                this->ManRight = true;
                this->ManJointRight = true;
                this->Probcategories.push_back("Prob Right Wrist OK");
                this->Probcategories.push_back("Prob Right Elbow OK");
            }


         if(i==0)
         {
             ManLeft= true;
             ManRight = true;
             ManJointLeft = true;
             ManJointRight =true;
         }       
        }

        //=======================================================================================================//
        cout << "---------------------------------------------------------" << endl;
        this->Image = cameraImage;        
        this->categories.clear();
        this->keepCategories.clear();

        cout << "Man intercept for left hand:" << ManLeft << endl;
        cout << "Man intercept for right hand:" << ManRight << endl;
        cout << "Man intercept for left joint:" << ManJointLeft << endl;
        cout << "Man intercept for right joint:" << ManJointRight << endl;

        if(ManLeft)
        {
            this->categories.push_back("Left Hand");
            this->categories.push_back("Hold Left Hand");
        }
        if(ManRight)
        {
            this->categories.push_back("Right Hand");
            this->categories.push_back("Hold Right Hand");
        }
        if(ManJointLeft)
        {
            this->categories.push_back("Left Joint");
            this->categories.push_back("Hold Left Joint");
        }
        if(ManJointRight)
        {
            this->categories.push_back("Right Joint");
            this->categories.push_back("Hold Right Joint");
        }

        namedWindow("image");
        this->Image = CandidatesWindow;

        int returnManInt = 0;
        if(this->categories.size()>0)
        {
            // There was a manual intervention
            returnManInt = ManualIntervention();
            circle(Skeleton,HandLeft.GetHand(),3,Scalar(0,5,255),3);
            circle(Skeleton,HandRight.GetHand(),3,Scalar(0,5,255),3);
        }
        else
        {
            // the was no manual intervention
            circle(Skeleton,HandLeft.GetHand(),3,Scalar(0,255,0),3);
            circle(Skeleton,HandRight.GetHand(),3,Scalar(0,255,0),3);
        }

        cout << "return value of manual intervention = " << returnManInt << endl;

        if(returnManInt==1) // in manual intervention LEFT OK was chosen
        {
            Mat Temp = ProbTest.GetProbMapWrist_left();
            ProbTest.UpdateMap(Temp,ProbTest.GetRelWristLeft());
            ProbTest.SetProbMapWrist_left(Temp);
        }

        if(returnManInt==2) // in manual intervention RIGHT OK was chosen
        {
            Mat Temp = ProbTest.GetProbMapWrist_right();
            ProbTest.UpdateMap(Temp,ProbTest.GetRelWristRight());
            ProbTest.SetProbMapWrist_right(Temp);
        }

        if(returnManInt==3) // in manual intervention LEFT OK was chosen
        {
            Mat Temp = ProbTest.GetProbMapElbow_left();
            ProbTest.UpdateMap(Temp,ProbTest.GetRelElbowLeft());
            ProbTest.setProbMapElbow_left(Temp);
        }

        if(returnManInt==4) // in manual intervention RIGHT OK was chosen
        {
            Mat Temp = ProbTest.GetProbMapElbow_right();
            ProbTest.UpdateMap(Temp,ProbTest.GetRelElbowRight());
            ProbTest.SetProbMapElbow_right(Temp);
        }       

        // ======================================================================================== //
        cout << "---------------------------------------------------------" << endl;
        cout << "left hand = " << HandLeft.GetHand() << endl;
        cout << "right hand = " << HandRight.GetHand() << endl;

        StructElement.HandLeft = HandLeft.GetHand();
        StructElement.ScoreLeft = 0;
        StructElement.HandRight = HandRight.GetHand();
        StructElement.ScoreRight = 0;
        StructElement.JointLeft = JointLeft.GetJoint();
        StructElement.JointRight = JointRight.GetJoint();

        cout << "---------------------------------------------------------" << endl;        
        cout << "#manual interceptions: " << this->manIntercept << endl;
        cout << "#frame of man intercept: " << manInterceptFrame << endl;
        imshow("image",Skeleton);
        waitKey(1);
        //waitKey();

        char OutputFileName[200];
        sprintf(OutputFileName,"%s%05d%s","Detections/",this->CurrentFrame,".jpg");
        imwrite(OutputFileName, Skeleton);

        Det.clear();
        categories.clear();
        Structure.push_back(StructElement);

        cout << "ManLeft = " << this->ManualLeft_Set << endl;

        // show 5-last detections on the left side
        if(this->ManualLeft_Set==true && Structure.size()>5)
        {
            int lastElement = Structure.size()-1;
            cout << "annotation = " << HandLeft.GetHand() << endl;
            cout << "last valid detection = " << Structure[lastElement-5].HandLeft << endl;
            double dist_X = HandLeft.GetHand().x - Structure[lastElement-5].HandLeft.x;
            double dist_Y = HandLeft.GetHand().y - Structure[lastElement-5].HandLeft.y;
            cout << "dist: " << dist_X << "," << dist_Y << endl;
            dist_X = dist_X/5;
            dist_Y = dist_Y/5;
            cout << "dist step size: " << dist_X << "," << dist_Y << endl;

            for(int c=1;c<5;c++)
            {
                Point Temp = Structure[lastElement-c].HandLeft;
                cout << "index-"<<c << "\t";
                cout << "\tOriginal = " << Temp;
                Structure[lastElement-c].HandLeft.x = Structure[lastElement-c].HandLeft.x + dist_X;
                Structure[lastElement-c].HandLeft.y = Structure[lastElement-c].HandLeft.y + dist_Y;
                cout << "\tNew = " << Structure[lastElement-c].HandLeft << endl;
                cout << "======"<<endl;
            }
            this->ManualLeft_Set = false;
        }

        if(this->ManualRight_Set==true && Structure.size()>5)
        {
            int lastElement = Structure.size()-1;
            cout << "annotation = " << HandRight.GetHand() << endl;
            cout << "last valid detection = " << Structure[lastElement-5].HandRight << endl;
            double dist_X = HandRight.GetHand().x - Structure[lastElement-5].HandRight.x;
            double dist_Y = HandRight.GetHand().y - Structure[lastElement-5].HandRight.y;
            cout << "dist: " << dist_X << "," << dist_Y << endl;
            dist_X = dist_X/5;
            dist_Y = dist_Y/5;
            cout << "dist step size: " << dist_X << "," << dist_Y << endl;

            for(int c=1;c<5;c++)
            {
                Point Temp = Structure[lastElement-c].HandRight;
                cout << "index-"<<c << "\t";
                cout << "\tOriginal = " << Temp;
                Structure[lastElement-c].HandRight.x = Structure[lastElement-c].HandRight.x + dist_X;
                Structure[lastElement-c].HandRight.y = Structure[lastElement-c].HandRight.y + dist_Y;
                cout << "\tNew = " << Structure[lastElement-c].HandRight << endl;
                cout << "======"<<endl;
            }
            this->ManualRight_Set = false;
        }
    }

    for(int s=0;s<Structure.size();s++)
    {
        save->newFrame(Structure[s].FrameNr);
        save->newUpperBody(Structure[s].UB);
        save->newFace(Structure[s].Face,0);
        save->newHand(Structure[s].HandLeft,"Left",0);
        save->newHand(Structure[s].HandRight,"Right",0);
        save->newJoint(Structure[s].JointLeft,"Left",0);
        save->newJoint(Structure[s].JointRight,"Right",0);
    }

    save->~SaveDetections();   
}

void MainWindow::on_ManIntervent_clicked()
{    
    this->categories.clear();
    this->categories.push_back("Left Hand");
    this->categories.push_back("Right Hand");
    this->categories.push_back("Left Joint");
    this->categories.push_back("Right Joint");
    ManualIntervention();    
}

//returns 1 if probmap of left wrist needs te be updated
//returns 2 if probmap of right wrist needs te be updated
//returns 3 if probmap of left elbow needs te be updated
//returns 4 if probmap of right elbow needs te be updated
int MainWindow::ManualIntervention()
{    
    int returnValue = 0;
    func_params ParamMouse;
    cvSetMouseCallback( "image", my_mouse_callback, (void*) &ParamMouse);

    int catSize = categories.size();
    vector<CatList> interventions;
    Point MousePoint;        

    bool LeftWristOK = false;
    bool LeftElbowOK = false;
    bool RightWristOK = false;
    bool RightElbowOK = false;
    this->ManualLeft_Set = false;
    this->ManualRight_Set = false;


    for(int cat=0;cat<catSize;cat++)
    {
        ManIntercept window(categories,Probcategories);
        int unusedValue = window.exec();

        if(window.GetLeftWristOK())
        {
            LeftWristOK = true;
            returnValue = 1;
        }

        if(window.GetRightWristOK())
        {
            RightWristOK = true;
            returnValue = 2;
        }

        if(window.GetLeftElbowOK())
        {
            LeftElbowOK = true;
            returnValue = 3;
        }

        if(window.GetRightElbowOK())
        {
            RightElbowOK = true;
            returnValue = 4;
        }

        if(window.getClose())
            break;

        string currentCat =  window.GetCurrentCat();
        cout << "current CAT = " << currentCat << endl;
        cout << "current ID = " << window.GetCurrentID() << endl;

        if(window.GetCurrentID()>-1)
            this->categories.erase (this->categories.begin()+window.GetCurrentID());

        while(true)
        {
            imshow("image", this->Image);
            int key = waitKey(1);

            if(ParamMouse.Punt1.x!=0&&ParamMouse.Punt1.x!=0)
            {
                MousePoint = ParamMouse.Punt1;
                break;
            }
        }

        CatList intervention;
        intervention.cat = currentCat;
        intervention.cat_id = window.GetCurrentID();
        intervention.pos = MousePoint;
        interventions.push_back(intervention);

        ParamMouse.Punt1.x = 0;
        ParamMouse.Punt1.y = 0;

    }


    int tempintercept = this->manIntercept;
    for(int list=0;list<interventions.size();list++)
    {
        cout << "interventions list = " << interventions[list].cat << " | ID = " << interventions[list].cat_id ;
        if(!interventions[list].cat.compare("Left Hand"))
        {
           this->ManualHandLeft = interventions[list].pos;
           this->KeepLeftHand = false;
        }

        if(!interventions[list].cat.compare("Right Hand"))
        {
            this->ManualHandRight = interventions[list].pos;
            this->KeepRightHand = false;
        }

        if(!interventions[list].cat.compare("Left Joint"))
        {
           this->ManualJointLeft = interventions[list].pos;
           this->KeepLeftJoint = false;
        }

        if(!interventions[list].cat.compare("Right Joint"))
        {
           this->ManualJointRight = interventions[list].pos;
            this->KeepRightJoint = false;
        }

        if(!interventions[list].cat.compare("Hold Left Hand"))
        {
            this->KeepLeftHand = true;
            HoldHandLeft = interventions[list].pos;
        }

        if(!interventions[list].cat.compare("Hold Right Hand"))
        {
            this->KeepRightHand = true;
            HoldHandRight = interventions[list].pos;
        }
        if(!interventions[list].cat.compare("Hold Left Joint"))
        {
            this->KeepLeftJoint = true;
            HoldJointLeft = interventions[list].pos;
        }

        if(!interventions[list].cat.compare("Hold Right Joint"))
        {
            this->KeepRightJoint = true;
            HoldJointRight = interventions[list].pos;
        }
    }
    interventions.clear();

    if(this->ManualHandRight.x>0 && ManualHandRight.y>0)
    {
        cout << "manual annotation right = " << this->ManualHandRight << endl;
        Update(Kal_Right, HandRight,CurrentFrame,this->ManualHandRight);
        this->ManualHandRight.x = 0;
        this->ManualHandRight.y = 0;
        this->manIntercept++;
        initKalJointR = true;

        ManRight = false;
        ManJointRight = false;
        JointRight.setPrediction(0);    // clear prediction counter for right joint
        ProbTest.SetAverageRight(100);
        this->ManualRight_Set = true;
    }

    if(this->ManualHandLeft.x>0 && this->ManualHandLeft.y>0)
    {
        cout << "manual annotation left = " << this->ManualHandLeft << endl;
        Update(Kal_Left, HandLeft,CurrentFrame,this->ManualHandLeft);
        this->ManualHandLeft.x = 0;
        this->ManualHandLeft.y = 0;
        this->manIntercept++;
        initKalJointL = true;

        ManLeft = false;
        ManJointLeft = false;
        JointLeft.setPrediction(0);
        ProbTest.SetAverageLeft(100);
        this->ManualLeft_Set = true;
    }

    if(this->ManualJointRight.x>0 && this->ManualJointRight.y > 0)
    {
        cout << "--------------------------------------------------------------------->manual annotation right joint = " << this->ManualJointRight << endl;
        UpdateJoint(this->Kal_Joint_Right,JointRight,ManualJointRight);
        this->manIntercept++;
        this->ManualJointRight.x = 0;
        this->ManualJointRight.y = 0;
        initKalJointR = false;
        ProbTest.SetAverageRight(100);
        ManJointRight = false;
    }

    if(this->ManualJointLeft.x>0 && this->ManualJointLeft.y > 0)
    {
        cout << "manual annotation left joint = " << this->ManualJointLeft << endl;
        UpdateJoint(this->Kal_Joint_Left,JointLeft,ManualJointLeft);
        this->manIntercept++;
        this->ManualJointLeft.x = 0;
        this->ManualJointLeft.y = 0;
        initKalJointL = false;        
        ProbTest.SetAverageLeft(100);
        ManJointLeft = false;
    }    

    if(returnValue==1 || returnValue==3)
    {
        cout << "Left OK!!" << endl;
        initKalJointL = false;
        ManLeft = false;
        ManJointLeft = false;
        initKalJointL = false;
        this->manIntercept++;

        JointLeft.setPrediction(0);
        HandLeft.setPrediction(0);

        cout << "Joint Left Prediction = " << JointLeft.getPrediction() << endl;
        cout << "Hand Left Prediction = " << HandLeft.getPrediction() << endl;

        ProbTest.SetAverageLeft(100);
        returnValue = 1;
    }

    if(returnValue==2 || returnValue==4)
    {
        cout << "Right OK!!" << endl;
        initKalJointR = false;
        ManRight = false;
        ManJointRight = false;
        initKalJointR = false;
        this->manIntercept++;

        JointRight.setPrediction(0);
        HandRight.setPrediction(0);

        cout << "Joint Right Prediction = " << JointRight.getPrediction() << endl;
        cout << "Hand Right Prediction = " << HandRight.getPrediction() << endl;

        ProbTest.SetAverageRight(100);
        returnValue = 2;
    }


    if (this->manIntercept > tempintercept)
            manInterceptFrame++;
    return returnValue;    
}

void MainWindow::Update(KalmanDHU & Kalman, Hand & HandInst, int frame, Point newPos)
{    

    if(frame-HandInst.GetManIndex()>3 || frame==this->startframe)     // if previous interception was only 5 frames ago, do not re-initialize kalman
    {        
        Kalman.Init(newPos);
        HandInst.setPrediction(0);
    }
    else
    {        
        Kalman.correctKalman(newPos);
    }

    HandInst.SetHand(newPos);
    HandInst.SetManIndex(frame);
}

void MainWindow::UpdateJoint(KalmanDHU & Kalman, Joint & JointInst, Point newPos)
{
    Kalman.Init(newPos);
    JointInst.SetJoint(newPos);
    cout << "--> \tNew joint pos = " << JointInst.GetJoint() << endl;
    Kalman.predictKalman();
    JointInst.setPrediction(0);
}

void MainWindow::on_SetWorkingDir_clicked()
{    
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home",QFileDialog::ShowDirsOnly);
    ui->InfoField->setText(fileName);

    this->WorkFolder_ = fileName.toUtf8().constData();
    this->WorkFolder_.append("/");
    cout << "path = " << this->WorkFolder_ << endl;   
}

void MainWindow::on_VideoProcess_clicked()
{
    //QString startFrame = ui->StartFrame->text();
    int startFrame = ui->StartFrame->text().toInt();
    int NrOfFrames = ui->NrOfFrames->text().toInt();
    cout << "start frame = " << startFrame << endl;
    cout << "#frames = " << NrOfFrames << endl;

    thread.process(this->WorkFolder_, startFrame,NrOfFrames);
}

float MainWindow::GetPrevScore()
{
    return this->PrevFaceScore;
}

void MainWindow::SetPrevScore(float score)
{
    this->PrevFaceScore = score;
}

Point MainWindow::ReadAnnotations(int frame, int type)
{
    // type = 0 --> LEFT
    // type = 1 --> RIGHT
    String XmlPath = "/home/sdb/Desktop/Dataset3/AnnotationEndpointsD3.xml";
	//String XmlPath = "/home/sdb/Desktop/Dataset3/AnnotationsVisapp.xml";

    vector<DetectionList> Annotation;
    Annotation = ReadDetectionFile(XmlPath);
    cout << "Nr of Annotated frames = " << Annotation.size() << endl;

    Point Anno;
    cout << "frame = " << frame << endl;
    int offset = 0;
    cout << "path = " << Annotation[frame-offset-1].m_path << endl;

    for(int j=0;j<Annotation[frame-offset].m_dets.size();j++)
    {
        std::string annoClass = Annotation[frame-offset].m_dets[j]->getClass();
        if(annoClass.compare("Left")==0 && type ==0)
        {
            Anno.x = Annotation[frame-offset].m_dets[j]->getX();
            Anno.y = Annotation[frame-offset].m_dets[j]->getY();
            cout << "left annotation = " << Anno;
            return Anno;
        }

        if(annoClass.compare("Right")==0 && type ==1)
        {
            Anno.x = Annotation[frame-offset].m_dets[j]->getX();
            Anno.y = Annotation[frame-offset].m_dets[j]->getY();
            cout << "right annotation = " << Anno;
        }
    }    
    return Anno;
}
