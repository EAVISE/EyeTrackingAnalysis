#include "processthread.h"

#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/cstdint.hpp>

#include <iostream>
#include "mainwindow.h"
#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

#include "FacedDetection.h"
#include "DetBody.h"
#include "DetHand.h"
#include "SaveDetections.h"
#include "Detector_VJ.h"
#include "KalmanDHU.h"


ProcessThread::ProcessThread(QObject *parent) :
    QThread(parent)
{
    // constructor
}

ProcessThread::~ProcessThread()
{
    // destructor
}

void ProcessThread::process(string folder, int startFrame, int NrOfFrames)
{
    // MUTEX
    QMutexLocker locker(&mutex);
    this->DetectionsPath = folder;
    this->StartFrame = startFrame;
    this->NrOfFrames = NrOfFrames;

    if (!isRunning())
    {
         start(TimeCriticalPriority);
    }
    else
    {
        restart = true;
        condition.wakeOne();
    }
}

void ProcessThread::run()
{
    //cout << "we are in the processing thread... " << endl;
    //cout << "---> Initalisation upper body detection." << endl;
    DetBody body("../FFLD/UB_1c.txt", -1);
    //cout << "---> Initalisation hand detection." << endl;
    DetHand hand;

    //cout << this->DetectionsPath << endl;
    SaveDetections* save;
    QString XMLFile = QString::fromStdString(this->DetectionsPath) + "AllDetections.xml";
    save = new SaveDetections(QString::fromStdString(this->DetectionsPath),XMLFile);

    // KALMAN UPPERBODY
    bool initKAlman_U = true;
    KalmanFilter KF_U(4, 2, 0);
    Mat_<float> measurement_U(2,1);
    measurement_U.setTo(Scalar(0));

    int Valid_Torso = 0;
    int Prediction_Torso = 0;
    Point predictPt_U;
    int avg_width = 0;
    char fileName[200];

    Detector_VJ FaceDetector;
    vector<DHU::Detection> FaceDetections;

    bool Face_detected = false;
    bool Torso_detected = false;

    Point Offset;
    Rect faceDHU;
    float score = 0;

    boost::posix_time::ptime start = boost::posix_time::microsec_clock::local_time();
    medianU = 0;

    for(int frame=this->StartFrame;frame<(this->StartFrame+this->NrOfFrames);frame=frame+1)
    {
        Face_detected = false;
        Torso_detected = false;

        Mat cameraImage, DrawImage, resizedImage, ArmImage;
        sprintf(fileName, "%s%05d%s", this->DetectionsPath.c_str(),frame,".png");
        //cout << "======================================================================================" << endl;
        cout << "image path = " << fileName << endl;

        // Clear detection vectors
        body.clearDetections();

        // READ IMAGE
        cameraImage = imread(fileName);
        if(cameraImage.empty())
            break;
        else
            //cout << "image dimensions: " << cameraImage.cols << " " << cameraImage.rows << endl;

        DrawImage = cameraImage.clone();
        ArmImage = cameraImage.clone();

        int scaleParameter = 2;
        // extra test for prediction --> if prediction width > prediction height --> stop using small window;

//------------- HALF IMAGE SIZE -------------//
        Size size(cameraImage.cols/scaleParameter,cameraImage.rows/scaleParameter);
        resize(cameraImage,resizedImage,size);

        save->newFrame(frame);        
        if(Valid_Torso>5 || (frame<5 && frame > 1))
        {
            double eps = 0.50;
            cout << "USE SMALLWINDOW!!" << endl;
            Rect NewWindow;
            NewWindow.x =predictPt_U.x;
            NewWindow.y = predictPt_U.y;
            NewWindow.height = avg_width*1.81;
            NewWindow.width = avg_width;

            NewWindow.x -= NewWindow.width * eps;
            NewWindow.y -= NewWindow.height * eps;
            NewWindow.width += NewWindow.width * eps * 2;
            NewWindow.height += NewWindow.height * eps * 2;

            // Secure an in image //coutout
            if(NewWindow.x>resizedImage.cols)
                NewWindow.x = resizedImage.cols-5;

            if(NewWindow.y>resizedImage.rows)
                NewWindow.y = resizedImage.rows-5;

            if(NewWindow.x < 0) {
                NewWindow.width = NewWindow.width + NewWindow.x;
                NewWindow.x = 0;
            }

            if(NewWindow.y <0) {
                NewWindow.height = NewWindow.height + NewWindow.y;
                NewWindow.y = 0;
            }

            if(NewWindow.x + NewWindow.width > resizedImage.cols)  {
                NewWindow.width = resizedImage.cols - NewWindow.x;
            }

            if(NewWindow.y + NewWindow.height > resizedImage.rows)  {
                NewWindow.height = resizedImage.rows - NewWindow.y;
            }

            Mat crop = resizedImage(NewWindow);
            // run upperbody detection
            body.runDetection(crop);

            // Add offset
            Offset.x = NewWindow.x;
            Offset.y = NewWindow.y;
            body.AddOffset(Offset,0);

            // Do face detection in small window in case there was an upperbody detection:            
            if(body.getRect().size()>0)
            {
                Torso_detected = true;

                Rect FaceImage;
                FaceImage.height = body.getRect()[0].height *scaleParameter;
                FaceImage.height = FaceImage.height/3;
                FaceImage.width = body.getRect()[0].width *scaleParameter;
                FaceImage.x = body.getRect()[0].x*scaleParameter;
                FaceImage.y = body.getRect()[0].y*scaleParameter;

                Mat FaceRoi = cameraImage(FaceImage);

                //char OutputFileName[200];
                //sprintf(OutputFileName,"%s%05d%s","Face/",frame,".jpg");
                //imwrite(OutputFileName, FaceRoi);

                FaceDetections.clear();
                FaceDetections = FaceDetector.Detect(FaceRoi);

                if(FaceDetections.size()>0)
                {
                    Face_detected = true;
                    faceDHU.x = FaceDetections[0].x + FaceImage.x;
                    faceDHU.y = FaceDetections[0].y + FaceImage.y;
                    faceDHU.width = FaceDetections[0].width;
                    faceDHU.height = FaceDetections[0].height;
                    score = FaceDetections[0].score;
                }
            }
        }      

        if(Valid_Torso<=5 || !Torso_detected)
        {
            body.runDetection(resizedImage);

            if(body.getRect().size()>0)
                Torso_detected = true;

            initKAlman_U = true;
        }

        if(initKAlman_U && body.getRect().size()>0)
        {
            cout << "Torso kalman initialization... " << endl;
            //KF_U.transitionMatrix = *(Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
            KF_U.transitionMatrix = (Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
            KF_U.statePre.at<float>(0) = body.getRect()[0].x;
            KF_U.statePre.at<float>(1) = body.getRect()[0].y;
            KF_U.statePre.at<float>(2) = 0;
            KF_U.statePre.at<float>(3) = 0;
            setIdentity(KF_U.measurementMatrix);
            setIdentity(KF_U.processNoiseCov, Scalar::all(1e-4));
            setIdentity(KF_U.measurementNoiseCov, Scalar::all(0.005));
            setIdentity(KF_U.errorCovPost, Scalar::all(.1));
            initKAlman_U = false;
            Mat estimated_U;
            for(int j=0;j<10;j++)
            {
                Mat prediction_U = KF_U.predict();

                measurement_U(0) = body.getRect()[0].x;
                measurement_U(1) = body.getRect()[0].y;
                estimated_U = KF_U.correct(measurement_U);
            }
        }              

        // Do kalman prediction
        if(!initKAlman_U)
        {
            Mat prediction_U = KF_U.predict();
            predictPt_U.x = prediction_U.at<float>(0);
            predictPt_U.y = prediction_U.at<float>(1);
        }

        Point Pred,Det;
        Pred.x = predictPt_U.x;
        Pred.y = predictPt_U.y;
        Det.x = body.getRect()[0].x;
        Det.y = body.getRect()[0].y;
        double dist = norm(Pred-Det);
        cout << "distance between prediction and detection = " << dist << endl;

        if(Valid_Torso>1 && dist>100)
            body.SetHeight(0,0);

        // Do correction
        Mat estimated_U;
        if(body.getRect()[0].height>0)
        {
            cout << " do correction with co " << body.getRect()[0].x << "," << body.getRect()[0].y << endl;
            measurement_U(0) = body.getRect()[0].x;
            measurement_U(1) = body.getRect()[0].y;
            estimated_U = KF_U.correct(measurement_U);

            Valid_Torso++;
            Prediction_Torso--;
            avg_width= body.getRect()[0].width*0.3 + 0.7*avg_width;
        }
        else if(Prediction_Torso<=5 && Valid_Torso>5)
        {
            cout << "Do correction with prediction... " << endl;
            measurement_U(0) = predictPt_U.x;
            measurement_U(1) = predictPt_U.y;
            estimated_U = KF_U.correct(measurement_U);
            Prediction_Torso++;
            Valid_Torso--;
        }
        else
        {
            cout << "Too much predictions, re-init kalman " << endl;
            initKAlman_U = true;
            Valid_Torso=0;
            Prediction_Torso=0;
        }


        if(frame%100==0)    // every 100 frames, reinit kalman
        {
            cout << "re-init kalman just to be sure..." << endl;
            initKAlman_U = true;
            Valid_Torso=0;
            Prediction_Torso=0;
        }

        Rect Final;
        Final.x = predictPt_U.x;
        Final.y = predictPt_U.y;

        if(body.getRect()[0].height>0)
        {
            Final.width = body.getRect()[0].size().width;
            Final.height = body.getRect()[0].size().height;
        }
        else
        {
            Final.width = avg_width;
            Final.height = avg_width*1.81;
        }

        body.UpdateDet(Final,0);

        Rect Resize;
        Resize.height = body.getRect()[0].height *scaleParameter;
        Resize.width = body.getRect()[0].width *scaleParameter;
        Resize.x = body.getRect()[0].x*scaleParameter;
        Resize.y = body.getRect()[0].y*scaleParameter;

        if(body.getRect()[0].x + body.getRect()[0].width > cameraImage.cols)
            cout << "RECTANGLE IS TOO WIDE" << endl;

        if(body.getRect()[0].y + body.getRect()[0].height > cameraImage.rows)
            cout << "RECTANGLE IS TOO TALL" << endl;

        body.UpdateDet(Resize,0);
        save->newUpperBody(body.getRect()[0]);

        //------------- END HALF IMAGE SIZE -------------//
        rectangle(DrawImage,body.getRect()[0],Scalar(255,100,155),3);
        //rectangle(ArmImage,body.getRect()[0],Scalar(255,100,155),3);

        if(!Face_detected && Torso_detected) // this is the case when there was no face detected in the previous step
        {
            Rect FaceImage;
            FaceImage.height = body.getRect()[0].height;
            FaceImage.height = FaceImage.height/2;
            FaceImage.width = body.getRect()[0].width;
            FaceImage.x = body.getRect()[0].x;
            FaceImage.y = body.getRect()[0].y;            

            if(FaceImage.y>cameraImage.rows)
            {
                FaceImage.y = cameraImage.rows-10;
            }

            if(FaceImage.y + FaceImage.height > cameraImage.rows )
            {
                int offset = (FaceImage.y + FaceImage.height)-cameraImage.rows;
                FaceImage.height -= offset;
            }
            if(FaceImage.height<0)
                FaceImage.height = 0;

            Mat FaceRoi = cameraImage(FaceImage);

            if(FaceRoi.cols>0 && FaceRoi.rows>0)
            {
                FaceDetections.clear();
                FaceDetections = FaceDetector.Detect(FaceRoi);
            }
            cout << "\t#face detections = " << FaceDetections.size() << endl;

            if(FaceDetections.size()>0)
            {
                Face_detected = true;
                faceDHU.x = FaceDetections[0].x+ body.getRect()[0].x;
                faceDHU.y = FaceDetections[0].y + body.getRect()[0].y;
                faceDHU.width = FaceDetections[0].width;
                faceDHU.height = FaceDetections[0].height;
                score = FaceDetections[0].score;
            }
        }

        QString ScoreAsString = QString::number(score);
        save->newFace(faceDHU,ScoreAsString);
        cout << "face DHU = " << faceDHU.x << " , " << faceDHU.y << " w = " << faceDHU.width << " , h = " << faceDHU.height << endl;

        // make face cut-out:        
        if(faceDHU.width>0)
        {
			Rect FaceCutOut = faceDHU;
            //FaceCutOut.x += FaceCutOut.width*0.2;
            //FaceCutOut.y += FaceCutOut.height*0.2;
            //FaceCutOut.width = FaceCutOut.width*0.7;
            //FaceCutOut.height = FaceCutOut.height*0.7;
            FaceCutOut.x += FaceCutOut.width*0.2;
            FaceCutOut.y += FaceCutOut.height*0.5;
            FaceCutOut.width = FaceCutOut.width*0.6;
            FaceCutOut.height = FaceCutOut.height*0.6;
			Mat FaceImage = Mat(cameraImage, FaceCutOut);

            //rectangle(cameraImage,FaceCutOut,Scalar(255,54,54),1);
            //char pathFace[200];
            //sprintf(pathFace,"%s%d%s","Skin/Face",frame,".jpg");
            //imwrite(pathFace,cameraImage);
			
			Mat src_luv_face;
			cvtColor(FaceImage, src_luv_face, CV_BGR2Luv);
			
			// split face image in three channels: L,U,V
			vector<Mat> luv_channels_face;
			split(src_luv_face, luv_channels_face);
			
			// caluclate median. min and max value of U channel
            vector<double> U;
			
			double max = 0;
			double min = 999;
    
			for(int row=0; row<FaceImage.rows; row++)
			{
				for(int col=0; col<FaceImage.cols; col++){
				   U.push_back(luv_channels_face[1].at<uchar>(row,col));
				   if(luv_channels_face[1].at<uchar>(row,col)>max)
						max = luv_channels_face[1].at<uchar>(row,col);
				   if(luv_channels_face[1].at<uchar>(row,col)<min)
					   min = luv_channels_face[1].at<uchar>(row,col);
				}
            }
			medianU = vecMed(U);
		}        

        // remove Face!
        cout << "face.x = " << faceDHU.x << " width = " << faceDHU.width << endl;
        cout << "face.y = " << faceDHU.y << " height = " << faceDHU.height << endl;
        Rect FaceTemp = faceDHU;
        FaceTemp.height = FaceTemp.height*1.5;
        rectangle(cameraImage,FaceTemp,Scalar(255,255,255),-1);
        char pathStretch[200];
        //sprintf(pathStretch,"%s%d%s","Skin/Input",frame,".jpg");
        //imwrite(pathStretch,cameraImage);
        rectangle(ArmImage,body.getRect()[0],Scalar(255,0,255),3);

        // STRETCH BOUNDING BOX        
        body.CalculateCutOuts(cameraImage);
        Mat Stretch = Mat(cameraImage, body.getRect()[0]);
        Mat Draw = Stretch.clone();
        rectangle(ArmImage,faceDHU,Scalar(255,0,255),0);

        Mat tempStretch = Stretch.clone();
        Mat src_hsv2;
        cvtColor(tempStretch, src_hsv2, CV_BGR2HSV);

        // SKIN SEGMENTATION
        Mat Skin = hand.skinSegmentation(Stretch,medianU, 8,20);

        // DILATION + EROSION
        /*
        Mat Input = Skin.clone();
        Mat dilated,eroded;
        cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));

        for(int i=0;i<2;i++)
        {
            dilate(Input, dilated, element); // temp = open(img)
            Input = dilated;
        }       

        for(int i=0;i<2;i++)
        {
            erode(Input, eroded, element);
            Input = eroded;
        }
        */

        // NEW DILATION AND EROSION IMPLEMENTATION FOR SAGA DATASET
        cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
        cv::Mat element2 = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(5, 5));
        Mat dilated,eroded;
        Mat Input = Skin.clone();

        erode(Input,eroded,element);

        dilate(eroded,dilated,element);
        dilate(dilated,dilated,element);
        dilate(dilated,dilated,element2);

        eroded = dilated;

        //char pathSkin[200];
        //sprintf(pathSkin,"%s%d%s","Skin/Skin",frame,".jpg");
        //imwrite(pathSkin,eroded);

        // FIND CONTOURS
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;        
        findContours( eroded, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
        vector<RotatedRect> minRect;

        for( int i = 0; i < contours.size(); i++ )
        {
            if( contours[i].size() > 20 )
            {
                minRect.push_back( minAreaRect( Mat(contours[i])) );              
            }
        }        

        RNG rng(12345);
        Mat drawing = Mat::zeros( eroded.size(), CV_8UC3 );
        drawing = Stretch.clone();
        Mat drawing2 = Mat::zeros( eroded.size(), CV_8UC3 );
        drawing2 = Stretch.clone();
        vector<Vec4i>ArmItems;

        for( int i = 0; i < contours.size(); i++ )
        {
            if( contours[i].size() > 20 )
            {
                Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
                drawContours( drawing2, contours, i, color, 2, 8, hierarchy, 0, Point() );
            }
        }

        //char pathContour[200];
        //sprintf(pathContour,"%s%d%s","Skin/Contour",frame,".jpg");
        //imwrite(pathContour,drawing2);

        for( int i = 0; i< minRect.size(); i++ )
        {
            Scalar color = Scalar(rng.uniform(0,255), rng.uniform(0, 255), 190);
            ellipse(drawing, minRect[i], color, 2, 8 );
            int length;
            float angle;

            if(minRect[i].size.width < minRect[i].size.height)
            {                
                length = minRect[i].size.height;
                angle = 90+abs(minRect[i].angle);
            }
            else
            {                
                length = minRect[i].size.width;
                angle = abs(minRect[i].angle);
            }

            //drawResult(drawing,minRect[i],color,2);
            //char pathRect[200];
            //sprintf(pathRect,"%s%d%s","Skin/Rect",frame,".jpg");
            //imwrite(pathRect,drawing);


            int offset_x = cos(angle * CV_PI / 180)*(length/2);
            int offset_y = sin(angle * CV_PI / 180)*(length/2);

            Point P1(minRect[i].center.x+offset_x,minRect[i].center.y-offset_y);
            Point P2(minRect[i].center.x-offset_x,minRect[i].center.y+offset_y);

            Vec4i Temp;
            Temp[0] = P1.x;
            Temp[1] = P1.y;
            Temp[2] = P2.x;
            Temp[3] = P2.y;
            ArmItems.push_back(Temp);
        }

        Scalar color1 = Scalar(rng.uniform(0,255), rng.uniform(0, 255), 240);
        Scalar color2 = Scalar(rng.uniform(0,255), 250, rng.uniform(0, 255));

        Rect TempFace;
        double eps = 0.2;
        TempFace.x -= faceDHU.width * eps;
        TempFace.y -= faceDHU.height * eps;
        TempFace.width += faceDHU.width * eps * 2;
        TempFace.height += faceDHU.height * eps * 2;

        cout << "#arm detections: " << ArmItems.size() << endl;
        for(int arm=0;arm<ArmItems.size();arm++)
        {
            Point2f P1,P2;
            P1.x = ArmItems[arm][0]+body.getRect()[0].x;
            P1.y = ArmItems[arm][1]+body.getRect()[0].y;
            P2.x = ArmItems[arm][2]+body.getRect()[0].x;
            P2.y = ArmItems[arm][3]+body.getRect()[0].y;

            Point2f Center;
            Center.x = (P1.x+P2.x)/2;
            Center.y = (P1.y+P2.y)/2;
            //circle(ArmImage,Center,3,Scalar(0,255,0),2);

            if (faceDHU.contains(Center))
            {
                cout << "reject hand..." << endl;
                //circle(ArmImage,Center,3,Scalar(0,0,255),2);
            }
            else
            {
                circle(ArmImage,P1,3,color2,2);
                circle(ArmImage,P2,3,color2,2);
                line(ArmImage,P1,P2,color1,2);

                Vec4i Temp;
                Temp[0] = P1.x;
                Temp[1] = P1.y;
                Temp[2] = P2.x;
                Temp[3] = P2.y;
                save->newDet(Temp,"none");
            }
        }

        //rectangle(ArmImage,faceDHU,Scalar(255,0,255),3);

        char OutputFileName[200];
        sprintf(OutputFileName,"%s%05d%s","Skin/Arms",frame,".jpg");
        imwrite(OutputFileName, ArmImage);
    }

    boost::posix_time::ptime end = boost::posix_time::microsec_clock::local_time();
    std::cout << "Total detection took: " << end - start << std::endl;

    save->~SaveDetections();
    cout << "processing thread is finished... " << endl;
}

void ProcessThread::drawResult(Mat& img, RotatedRect &box, const Scalar& color, int thickness)
{
    Point2f corners[4];
    box.points(corners);

    for(int i = 0; i < 4; i++) {
        line(img, corners[i], corners[(i + 1) % 4], color, thickness);
    }

    Point dirLine(-sin(box.angle * CV_PI / 180) * 10 + box.center.x, cos(box.angle * CV_PI / 180) * 10 + box.center.y);
    line(img, box.center, dirLine, color);
    circle(img, box.center, 3, color);
}

double ProcessThread::vecMed(std::vector<double> vec) 
{
    if(vec.empty()) return 0;
    else {
        std::sort(vec.begin(), vec.end());
        if(vec.size() % 2 == 0)
                return (vec[vec.size()/2 - 1] + vec[vec.size()/2]) / 2;
        else
                return vec[vec.size()/2];
    }
}


