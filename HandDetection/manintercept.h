#ifndef MANINTERCEPT_H
#define MANINTERCEPT_H

#include <QtWidgets/QDialog>
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
#include "opencv2/opencv.hpp"

#include <QtWidgets/QDialog>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include "MyButtonGroup.h"

class QCheckBox;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QPushButton;

using namespace std;
using namespace cv;

struct CatList
{
    String cat;
    int cat_id;
    Point pos;
};

class ManIntercept : public QDialog
{
    Q_OBJECT

public slots:
    void UpdateList(int NewItem);
    void UpdateProbList(int Item);

    void CloseDialog();

    void LeftWristOKDialog();
    void RightWristOKDialog();
    void LeftElbowOKDialog();
    void RightElbowOKDialog();

public:
    ManIntercept(vector<string> categories, vector<string> Probcategories, QWidget *parent = 0);
    string GetCurrentCat();    
    string GetCurrentProbCat();
    int GetCurrentID();

    bool GetLeftWristOK();
    bool GetRightWristOK();
    bool GetLeftElbowOK();
    bool GetRightElbowOK();

    QLabel *label_Name;
    QLabel *label_Cat;
    QCheckBox *caseCheckBox;
    QLineEdit *lineEdit_Name;
    QLineEdit *lineEdit_Cat;
    QPushButton *ConfirmButton;
    QPushButton *CancelButton;
    QPushButton *ClearButton;
    QPushButton *ProbLeftWristButton;
    QPushButton *ProbRightWristButton;
    QPushButton *ProbLeftElbowButton;
    QPushButton *ProbRightElbowButton;

    string currentCat;
    int currentID;
    bool getClose();

    string currentProbCat;
    string CurrentProbID;

private:
    vector <string>VectorOfCats;
    vector <CatList> List;
    vector <CatList> ProbList;


    int maxCat;
    char FileName[200];
    bool Close;
    bool LeftWristOK;
    bool RightWristOK;
    bool LeftElbowOK;
    bool RightElbowOK;
};

#endif // MANINTERCEPT_H
