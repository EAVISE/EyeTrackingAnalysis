#include "hand.h"

using namespace cv;
using namespace std;

Hand::Hand()
{
    this->ManIndex = 0;
    this->setPrediction(0);
}

void Hand::SetHand(int x, int y)
{
    this->HandCo_.x = x;
    this->HandCo_.y = y;
}

void Hand::SetHand(Point P)
{
    this->HandCo_.x = P.x;
    this->HandCo_.y = P.y;
}

Point Hand::GetHand()
{
    return  this->HandCo_;
}

int Hand::GetX()
{
    return this->HandCo_.x;
}

int Hand::GetY()
{
    return this->HandCo_.y;
}

/*
 * checks displacement between current en predicted poistion of the Hand
 * return true if displacement is below TH
 * returns false if displacement is too large
  */
bool Hand::CheckDisplacement(Point Prediction, int TH)
{
    if(this->HandCo_.x>0&&this->HandCo_.y>0&&Prediction.x>0 &&Prediction.y>0)
    {
        Point2f CurrLeft(this->HandCo_.x,this->HandCo_.y);
        Point2f PredLeft(Prediction.x,Prediction.y);
        double displace = norm(CurrLeft-PredLeft);
        if(displace>TH)
            return false;
        else
            return true;
    }
    else
        return true;
}

void Hand::GetCandidates(Point2f prediction, vector<Vec4i> Det)
{
    this->minDist_ = 999;
    this->secminDist_ = 999;
    vector<Vec4i> DetCopy = Det;

    // loop over all detections and search best value:
    for(int det=0;det<DetCopy.size();det++)
    {
        Point2f P1,P2;
        P1.x = DetCopy[det][0];
        P1.y = DetCopy[det][1];
        P2.x = DetCopy[det][2];
        P2.y = DetCopy[det][3];
        double res1 = norm(P1-prediction);
        double res2 = norm(P2-prediction);

        if(findMin(res1,res2))
        {
            // res 1 is smallest value
            if(res1<this->minDist_)
            {
                this->bestIndex_ = det;
                this->minDist_ = res1;
                this->best_[0]=P1.x;
                this->best_[1]=P1.y;
                this->best_[2]=P2.x;
                this->best_[3]=P2.y;
            }
        }
        else
        {
            // res 2 is smallest value
            if(res2<this->minDist_)
            {
                this->bestIndex_ = det;
                this->minDist_ = res2;
                this->best_[0]=P2.x;
                this->best_[1]=P2.y;
                this->best_[2]=P1.x;
                this->best_[3]=P1.y;
            }
        }
    }
   // Remove best value in vector
    DetCopy.erase (DetCopy.begin()+this->bestIndex_);

    // search for the second best value...
    for(int det=0;det<DetCopy.size();det++)
    {
        Point2f P1,P2;
        P1.x = DetCopy[det][0];
        P1.y = DetCopy[det][1];
        P2.x = DetCopy[det][2];
        P2.y = DetCopy[det][3];
        double res1 = norm(P1-prediction);
        double res2 = norm(P2-prediction);

        if(findMin(res1,res2))
        {
            // res 1 is smallest value
            if(res1<this->secminDist_)
            {
                this->secminDist_ = res1;
                this->secbest_[0]=P1.x;
                this->secbest_[1]=P1.y;
                this->secbest_[2]=P2.x;
                this->secbest_[3]=P2.y;
            }
        }
        else
        {
            // res 2 is smallest value
            if(res2<this->minDist_)
            {
                this->secminDist_ = res2;
                this->secbest_[0]=P2.x;
                this->secbest_[1]=P2.y;
                this->secbest_[2]=P1.x;
                this->secbest_[3]=P1.y;
            }
        }
    }
}

bool Hand::findMin(double A,double B)
{
    if(A<B)
        return true;
    else
        return false;
}

Vec4i Hand::GetBest()
{
    return this->best_;
}

double Hand::GetBestDist()
{
    return this->minDist_;
}

Vec4i Hand::GetSecBest()
{
    return this->secbest_;
}

double Hand::GetSecBestDist()
{
    return this->secminDist_;
}

bool Hand::NotEmpty()
{
     if(this->HandCo_.x!=0 && this->HandCo_.y !=0)
         return true;
     else
         return false;
}

void Hand::SetManIndex(int index)
{
    this->ManIndex = index;
}

int Hand::GetManIndex()
{
   return this->ManIndex;
}

int Hand::GetBestIndex()
{
    return this->bestIndex_;
}

int Hand::GetSecBestIndex()
{
    return this->secbestIndex_;
}

double Hand::GetUsedDist()
{
    return this->usedDist_;
}

void Hand::SetUsedDist(double dist)
{
    this->usedDist_ = dist;
}

int Hand::getPrediction()
{
    return this->prediction_;
}

void Hand::setPrediction(int pred)
{
    this->prediction_ = pred;
}

void Hand::increasePrediction()
{
    this->prediction_ = this->prediction_+1;
}

void Hand::decreasePrediction()
{
    if(this->prediction_>0)
    {
        this->prediction_ = this->prediction_-1;
    }
}

void Hand::setValid(int valid)
{
    this->valid_ = valid;
}

int Hand::getValid()
{
    return this->valid_;
}

void Hand::increaseValid()
{
    this->valid_ = this->valid_+1;
}

void Hand::decreaseValid()
{
    if(this->valid_>0)
    {
        this->valid_ = this->valid_ -1;
    }
}
