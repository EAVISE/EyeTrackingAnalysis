var searchData=
[
  ['close',['close',['../classMatlabIO.html#a77a8a636fd3069dab8b8cf27998eeaf3',1,'MatlabIO']]],
  ['collatematrixfields',['collateMatrixFields',['../classMatlabIO.html#a381c9b5d7d30ae14ab62b7db18cd10ea',1,'MatlabIO']]],
  ['constructcell',['constructCell',['../classMatlabIO.html#ae70205ee74fc3b5ca831265317d53afe',1,'MatlabIO']]],
  ['constructmatrix',['constructMatrix',['../classMatlabIO.html#a86a5a57fe5ef8f636c2ee8838b3ee172',1,'MatlabIO']]],
  ['constructsparse',['constructSparse',['../classMatlabIO.html#aed0962e7b0c5bd53153b429e9e28fa25',1,'MatlabIO']]],
  ['constructstring',['constructString',['../classMatlabIO.html#af6932c5fdd2333201bef549c52a7febb',1,'MatlabIO']]],
  ['constructstruct',['constructStruct',['../classMatlabIO.html#ab5a35f3b0b204a182b7e6e0db857b8f6',1,'MatlabIO']]]
];
