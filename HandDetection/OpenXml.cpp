#include "OpenXml.h"

OpenXml::OpenXml(string xmlFile)
{
    /* We'll parse the example.xml */
    QFile* file = new QFile(QString::fromStdString(xmlFile));
    /* If we can't open it, let's show an error message. */
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        //cout << "Enable to open file." << endl;
        return;
    }
    xml = new QXmlStreamReader(file);
}

void OpenXml::XmlToData() {    
    while( !xml->atEnd() && !xml->hasError()) {
        QXmlStreamReader::TokenType token = xml->readNext();
        /* If token is just StartDocument, we'll go to next.*/
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if (xml->name() == "File") {
                xml->readNext();
                this->filenameAvi = xml->text().toString().toStdString();
                //cout << "File name:      " << this->filenameAvi << endl;
            }
            vector<Rect> ub;           
            vector<Point> left;
            vector<Point> right;
            vector<Vec4i> armvector;
            vector<double> handscore;
            vector<double> armscore;
            vector<float> facescore;

            string side;
            if (xml->name() == "Frame") {
                //cout << "Frame ";
                this->face.push_back(Rect());
                xml->readNext();
                while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Frame")) {
                    xml->readNext();
                    if (xml->name() == "Number") {
                        xml->readNext();
                        this->frameNr.push_back(xml->text().toString().toInt());
                        //cout << "   " << this->frameNr.back() << endl;
                        xml->readNext();
                    }


                    if (xml->name() == "UpperBody") {

                        while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "UpperBody")) {

                            xml->readNext();
                            if (xml->name() == "Face") {

                                QXmlStreamAttributes FaceAttributes = xml->attributes();
                                if(FaceAttributes.hasAttribute("Score")) {
                                    /* We'll add it to the map. */
                                    facescore.push_back(FaceAttributes.value("Score").toFloat());
                                }

                                xml->readNext();
                                while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Face")) {
                                    xml->readNext();
                                    if (xml->name() == "Rect"){                                        
                                        //this->face.push_back(this->readRect());
                                        this->face.at(this->face.size()-1) = this->readRect();
                                    }
                                }
                            }

                            QXmlStreamAttributes attributes = xml->attributes();
                                /* Let's check that person has id attribute. */
                                if(attributes.hasAttribute("Side")) {
                                    /* We'll add it to the map. */
                                    side = attributes.value("Side").toString().toStdString();
                                }

                            if (xml->name() == "Rect") {
                                //cout << "       Upperbody Rect" << endl;
                                ub.push_back(this->readRect());
                            }

                            if (xml->name() == "Hand") {
                                if(side.compare("Left")==0)
                                {
                                    left.push_back(this->readPoint());
                                }
                                if(side.compare("Right")==0)
                                {
                                    right.push_back(this->readPoint());
                                }
                            }

                            if (xml->name() == "Arm") {
                                if(side.compare("none")==0)
                                {
                                    armvector.push_back(this->readLine());
                                    armscore.push_back(this->score);
                                }
                            }
                        }
                    }
                }
                this->upperBody.push_back(ub);
                this->left_hand.push_back(left);
                this->right_hand.push_back(right);
                this->handscore.push_back(handscore);
                this->arm.push_back(armvector);
                this->armscore.push_back(armscore);
                this->facescore.push_back(facescore);

                ub.clear();                          
                handscore.clear();               
                armvector.clear();
                armscore.clear();
            }
        }
    }


    /* Error handling. */
    if(xml->hasError()) {
        //cout << "An error has occurs when parsing the XML" << endl;
    }
    xml->clear();
}

Rect OpenXml::readRect() {
    int x = 0, y = 0, Width = 0, Height = 0;

    while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Rect"))  {
        xml->readNext();
        if ( xml->name() == "x" ) {
            xml->readNext();
            x = xml->text().toString().toInt();
            xml->readNext();
            //cout << "       x: " << x << endl;
        }
        if ( xml->name() == "y" ) {
            xml->readNext();
            y = xml->text().toString().toInt();
            xml->readNext();
            //cout << "       y: " << y << endl;
        }
        if ( xml->name() == "Width" ) {
            xml->readNext();
            Width = xml->text().toString().toInt();
            xml->readNext();
            //cout << "       Width: " << Width << endl;
        }
        if ( xml->name() == "Height" ) {
            xml->readNext();
            Height = xml->text().toString().toInt();
            xml->readNext();
            //cout << "       Height: " << Height << endl;
        }
    }
    return Rect(x, y, Width, Height);
}

RotatedRect OpenXml::readHand() {
    float x = 0, y = 0, Width = 0, Height = 0, angle = 0;
    while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Hand"))  {
        xml->readNext();
        if ( xml->name() == "x" ) {
            xml->readNext();
            x = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       x: " << x << endl;
        }
        if ( xml->name() == "y" ) {
            xml->readNext();
            y = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       y: " << y << endl;
        }
        if ( xml->name() == "Width" ) {
            xml->readNext();
            Width = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       Width: " << Width << endl;
        }
        if ( xml->name() == "Height" ) {
            xml->readNext();
            Height = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       Height: " << Height << endl;
        }
        if ( xml->name() == "Rotation" ) {
            xml->readNext();
            angle = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       angle: " << angle << endl;
        }
        if ( xml->name() == "Score" ) {
            xml->readNext();
            this->score = xml->text().toString().toDouble();
            xml->readNext();
            //cout << "       Score: " << score << endl;
        }
    }
    return RotatedRect(Point2f(x, y), Size2f(Width, Height), angle);
}


Point OpenXml::readPoint() {
    float x=0,y=0;
    while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Hand"))  {
        xml->readNext();
        if ( xml->name() == "x" ) {
            xml->readNext();
            x = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       x: " << x << endl;
        }
        if ( xml->name() == "y" ) {
            xml->readNext();
            y = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       y: " << y << endl;
        }
    }
    return Point(x, y);
}

Vec4i OpenXml::readLine() {
    float x1=0;
    float y1=0;
    float x2=0;
    float y2=0;

    while (!(xml->tokenType() == QXmlStreamReader::EndElement && xml->name() == "Arm"))  {
        xml->readNext();
        if ( xml->name() == "x1" ) {
            xml->readNext();
            x1 = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       x1: " << x1 << endl;
        }
        if ( xml->name() == "y1" ) {
            xml->readNext();
            y1 = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       y1: " << y1 << endl;
        }
        if ( xml->name() == "x2" ) {
            xml->readNext();
            x2 = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       x2: " << x2 << endl;
        }
        if ( xml->name() == "y2" ) {
            xml->readNext();
            y2 = xml->text().toString().toFloat();
            xml->readNext();
            //cout << "       y2: " << y2 << endl;
        }
    }
    return Vec4i(x1, y1,x2,y2);
}
