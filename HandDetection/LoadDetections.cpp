#include "LoadDetections.h"

using namespace rapidxml;

/*!
	\file The implementation of a function to read an xml-based detection-list

*/


/*! \brief Read an xml-based detection file and return all detetections per image as a vector

	This function reads a detection files (in our own XML-format) and returns these detections as a vector of DetectionList classes. This function can both be used to read annotations for training-/evaluation-purposes and to evaluate the detections a detector has performed on a range of images. The filenames are also stored in the DetectionList so the listed detections can easely be shown on the image (a detection is nothing without the image behind it).
	\param filename The filename to read
	\return A vector of DetectionList classes, which holds the filename and a vector of Detection pointers 

        \author F. De Smedt
        \date 2014
*/
std::vector<DetectionList> ReadDetectionFile(std::string filename)
{
    std::vector<DetectionList> Dets;
    std::cout << "Parsing Detections/Annotations" << std::endl;
    xml_document<> doc;
    xml_node<> * root_node;
    std::ifstream theFile (filename.c_str());
    if(!theFile.is_open())
        return Dets;

    std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);
    root_node = doc.first_node("Detections");

    DetectionList TempD;

    for(xml_node<> * Fs = root_node->first_node("File"); Fs; Fs = Fs->next_sibling()) {
        TempD.m_path =  Fs->first_node("Path")->value();
        TempD.m_dets.clear();
        for(xml_node<> * Ds = Fs->first_node("Detection"); Ds; Ds = Ds->next_sibling()) {
            //xml_node<> *  = ITrees->first_node("nodes");
            //TempD.filename = Ds->first_node("Path")->value();
            Detection* Temp = new Detection();            
            Temp->setClass(Ds->first_attribute("class")->value());

            Temp->setAngle(atof(Ds->first_attribute("angle")->value()));
            Temp->setX(atof(Ds->first_node("x")->value()));
            Temp->setY(atof(Ds->first_node("y")->value()));
            Temp->setWidth(atof(Ds->first_node("width")->value()));
            Temp->setHeight(atof(Ds->first_node("height")->value()));
            Temp->setScore(atof(Ds->first_node("score")->value()));
            Temp->setColor( cv::Scalar(255,255,255));
            //std::cout << "Assigning: " << Temp->x << std::endl;
            TempD.m_dets.push_back(Temp);
        }
        Dets.push_back(TempD);
    }
    theFile.close();

    return Dets;
}
