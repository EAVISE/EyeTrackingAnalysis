#ifndef KALMANDHU_H
#define KALMANDHU_H

//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/video/tracking.hpp"
#include "opencv2/opencv.hpp"

#include <stdio.h>

using namespace cv;

class KalmanDHU
{
    public:
        ///constructor
        KalmanDHU(); //start point for tracker
        ///destructor
        virtual ~KalmanDHU();

        void Init(Point startPoint);

        void predictKalman();

        void correctKalman(Point input);

        float getPreX();

        float getPreY();

        Point getPreXY();

        float getCorrX();

        float getCorrY();

        Point getCorrXY();

        bool GetInit();


    protected:
        float pre_X;            //predicted X position
        float pre_Y;            //predicted Y position

        float corr_X;           //corrected values
        float corr_Y;       

        KalmanFilter KF;
        Mat_<float> state;
        Mat processNoise;
        Mat_<float> measurement;

        bool KalManInitialized_;
        Point InitialPoint;
        Mat prediction;

        bool initKalman;

    private:


};

#endif // KALMANDHU_H
