//—————————————————————————————————————————————————
// Implementation of the paper "Exact Acceleration of Linear Object Detectors", 12th European
// Conference on Computer Vision, 2012.
//
// Copyright (c) 2012 Idiap Research Institute, <http://www.idiap.ch/>
// Written by Charles Dubout <charles.dubout@idiap.ch>
//
// This file is part of FFLD (the Fast Fourier Linear Detector)
//
// FFLD is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License version 3 as published by the Free Software Foundation.
//
// FFLD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along with FFLD. If not, see
// <http://www.gnu.org/licenses/>.
//
// This file was adapted by Stijn De Beugher 201.
//--------------------------------------------------------------------------------------------------
#include "Detection.h"

/************************************************************************


************************************************************************/
void detect(const Mixture & mixture, int width, int height, const HOGPyramid & pyramid,
            double threshold, double overlap, vector<DetectionFFLD> & detections)
{
    // Compute the scores
    vector<HOGPyramid::Matrix> scores;
    vector<Mixture::Indices> argmaxes;
    vector<vector<vector<Model::Positions> > > positions;
    vector<DetectionFFLD> TempVector;

    mixture.convolve(pyramid, scores, argmaxes, &positions);

    // Cache the size of the models
    vector<pair<int, int> > sizes(mixture.models().size());

    for (int i = 0; i < sizes.size(); ++i)
        sizes[i] = mixture.models()[i].rootSize();

    // For each scale   
    for (int z = 0; z < scores.size(); ++z) {
        const double scale = pow(2.0, static_cast<double>(z) / pyramid.interval() + 2);

        const int rows = static_cast<int>(scores[z].rows());
        const int cols = static_cast<int>(scores[z].cols());
        //cout << "rows = " << rows << endl;
        for (int y = 0; y < rows; ++y) {
            //cout << "cols = " << cols << endl;
            for (int x = 0; x < cols; ++x) {
                const double score = scores[z](y, x);

                if (score > threshold) {
                    // Non-maxima suppresion in a 3x3 neighborhood
                    if (((y == 0) || (x == 0) || (score >= scores[z](y - 1, x - 1))) &&
                        ((y == 0) || (score >= scores[z](y - 1, x))) &&
                        ((y == 0) || (x == cols - 1) || (score >= scores[z](y - 1, x + 1))) &&
                        ((x == 0) || (score >= scores[z](y, x - 1))) &&
                        ((x == cols - 1) || (score >= scores[z](y, x + 1))) &&
                        ((y == rows - 1) || (x == 0) || (score >= scores[z](y + 1, x - 1))) &&
                        ((y == rows - 1) || (score >= scores[z](y + 1, x))) &&
                        ((y == rows - 1) || (x == cols - 1) ||
                         (score >= scores[z](y + 1, x + 1)))) {
                        Rectangle bndbox((x - pyramid.padx()) * scale + 0.5,
                                         (y - pyramid.pady()) * scale + 0.5,
                                         sizes[argmaxes[z](y, x)].second * scale + 0.5,
                                         sizes[argmaxes[z](y, x)].first * scale + 0.5);

                        // Truncate the object
                        bndbox.setX(max(bndbox.x(), 0));
                        bndbox.setY(max(bndbox.y(), 0));
                        bndbox.setWidth(min(bndbox.width(), width - bndbox.x()));
                        bndbox.setHeight(min(bndbox.height(), height - bndbox.y()));

                        if (!bndbox.empty())
                        {
                            DetectionFFLD temp(score, x, y, z, bndbox);
                            TempVector.push_back(temp);
                        }
                    }
                }
            }
        }
    }

    // Non maxima suppression
    sort(TempVector.begin(), TempVector.end());

    for (int i = 1; i < TempVector.size(); ++i)
        TempVector.resize(remove_if(TempVector.begin() + i, TempVector.end(),
                                    Intersector(TempVector[i - 1], overlap, true)) -
                          TempVector.begin());

    for(int i = 0; i < TempVector.size(); ++i)
    {
        detections.push_back(TempVector[i]);
    }
}

/************************************************************************
This is this initialization function
This functions opens the model and transforms it into a mixture structure
input: pointer to mixture structure, path to model
output: -1 if something was wrong
************************************************************************/
int Init(Mixture & mixture, string model)
{
    int maxRows = 192;
    int maxCols = 336;

    //******************************************************//
    // Try to open the mixture
    ifstream in(model.c_str(), ios::binary);
    if (!in.is_open()) {
       cerr << "\nInvalid model file " << model << endl;
       return -1;
    }

    in >> mixture;

    if (mixture.empty()) {
       cerr << "\nInvalid model file " << model << endl;
       return -1;
    }    

   if (!Patchwork::InitFFTW((maxRows) & ~15, (maxCols) & ~15)) {
        cerr << "\nCould not initialize the Patchwork class" << endl;
        return -1;
    }
    //******************************************************//
    // Filter transform
    mixture.cacheFilters();
    //******************************************************//
}
/************************************************************************
This function is an extra non maxima suppression for the ffld_based detector
intput is a vector of dections, a vector of "unique detections" and a overlap factor
Detections are clustered together if there is enough overlap between their bounding boxes
clustered detections are written into the "unique detections" vector
************************************************************************/
void NonMaxSuppr(vector<DetectionFFLD> input,vector<UniqueDetections> & output, int nmsFactor)
{
    for(int j=0;j<input.size();j++)
    {
       const FFLD::Rectangle & rect = input[j];
       Point P1(rect.left(),rect.top());
       Point P2(rect.right(),rect.bottom());
       Rect R1(P1,P2);
       int width = rect.right()-rect.left();
       int height = rect.top()-rect.bottom();
       Point Center;
       Center.x = rect.left()+width/2;
       Center.y = rect.bottom()+height/2;
       int assigned = 0;

       for(int DetSize=0;DetSize<output.size();DetSize++)
       {
           double res = norm(Center-output[DetSize].Center);
           if(res<nmsFactor&&res>0)
           {
               assigned = 1;
               output[DetSize].Center = Center; // update center
               if(input[j].score > output[DetSize].BestScore)
               {
                    output[DetSize].BestScore = input[j].score;
                    output[DetSize].detect = R1;
                    break;
               }
           }
       }

      if(assigned==0)
       {
           UniqueDetections UniqueDet;
           UniqueDet.BestScore = input[j].score;
           UniqueDet.detect = R1;
           output.push_back(UniqueDet);
       }
    }
}

void ConvertFormat(vector<DetectionFFLD> input,vector<UniqueDetections> & output)
{
    for(int j=0;j<input.size();j++)
    {
       const FFLD::Rectangle & rect = input[j];
       Point P1(rect.left(),rect.top());
       Point P2(rect.right(),rect.bottom());
       Rect R1(P1,P2);
       int width = rect.right()-rect.left();
       int height = rect.top()-rect.bottom();
       Point Center;
       Center.x = rect.left()+width/2;
       Center.y = rect.bottom()+height/2;
       int assigned = 0;

        UniqueDetections UniqueDet;
        UniqueDet.BestScore = input[j].score;
        UniqueDet.detect = R1;
        output.push_back(UniqueDet);
    }
}
