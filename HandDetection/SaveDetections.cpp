#include "SaveDetections.h"

SaveDetections::SaveDetections(QString filenameAvi,QString Path)
{    
    this->debtCounter = 0;
    file = new QFile(Path);
    file->open(QIODevice::WriteOnly);
    xmlWriter = new QXmlStreamWriter(file);
    xmlWriter->setAutoFormatting(true);
    xmlWriter->writeStartDocument();
    xmlWriter->writeEndDocument();
    xmlWriter->writeStartElement("Saves");
    xmlWriter->writeTextElement("File", filenameAvi);
}

void SaveDetections::newFrame(int frameNr)
{
    // Depth = 1
    for(int i = this->debtCounter; i >= 1; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 1;
    xmlWriter->writeStartElement("Frame");
    xmlWriter->writeTextElement("Number", QString::number(frameNr));
}

void SaveDetections::newUpperBody(Rect box)
{
    // Dept = 2
    for(int i = this->debtCounter; i >= 2; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 2;
    xmlWriter->writeStartElement("UpperBody");
    xmlWriter->writeStartElement("Rect");
    xmlWriter->writeTextElement("x", QString::number(box.x));
    xmlWriter->writeTextElement("y", QString::number(box.y));
    xmlWriter->writeTextElement("Width", QString::number(box.width));
    xmlWriter->writeTextElement("Height", QString::number(box.height));
    xmlWriter->writeEndElement();
}

void SaveDetections::newHand(Point Center, QString side, double score)
{
    // Dept = 3
    for(int i = this->debtCounter; i >= 3; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 3;
    xmlWriter->writeStartElement("Hand");
    xmlWriter->writeAttribute( QString("Side"), side );
    xmlWriter->writeStartElement("Point");
    xmlWriter->writeTextElement("x", QString::number(Center.x));
    xmlWriter->writeTextElement("y", QString::number(Center.y));
    xmlWriter->writeEndElement();
}

void SaveDetections::newJoint(Point Center, QString side, double score)
{
    // Dept = 3
    for(int i = this->debtCounter; i >= 3; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 3;
    xmlWriter->writeStartElement("Joint");
    xmlWriter->writeAttribute( QString("Side"), side );
    xmlWriter->writeStartElement("Point");
    xmlWriter->writeTextElement("x", QString::number(Center.x));
    xmlWriter->writeTextElement("y", QString::number(Center.y));
    xmlWriter->writeEndElement();
}

void SaveDetections::newDet(Vec4i Line, QString side)
{
    // Dept = 3
    for(int i = this->debtCounter; i >= 3; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 3;
    xmlWriter->writeStartElement("Arm");
    xmlWriter->writeAttribute( QString("Side"), side );
    xmlWriter->writeStartElement("Line");
    xmlWriter->writeTextElement("x1", QString::number(Line[0]));
    xmlWriter->writeTextElement("y1", QString::number(Line[1]));
    xmlWriter->writeTextElement("x2", QString::number(Line[2]));
    xmlWriter->writeTextElement("y2", QString::number(Line[3]));
    xmlWriter->writeEndElement();
}

void SaveDetections::newFace(Rect box, QString score)
{
    // Dept = 3
    for(int i = this->debtCounter; i >= 3; i--) {
        xmlWriter->writeEndElement();
    }

    this->debtCounter = 3;
    xmlWriter->writeStartElement("Face");
    xmlWriter->writeAttribute( QString("Score"), score);
    xmlWriter->writeStartElement("Rect");
    xmlWriter->writeTextElement("x", QString::number(box.x));
    xmlWriter->writeTextElement("y", QString::number(box.y));
    xmlWriter->writeTextElement("Width", QString::number(box.width));
    xmlWriter->writeTextElement("Height", QString::number(box.height));
    xmlWriter->writeEndElement();
}

void SaveDetections::runtime(float time)
{
    this->debtCounter--;
    xmlWriter->writeEndElement();
    xmlWriter->writeTextElement("Runtime", QString::number(time));
}


SaveDetections::~SaveDetections()
{
    for(int i = this->debtCounter; i >= 0; i--) {
        xmlWriter->writeEndElement();
    }

    xmlWriter->writeEndDocument();
    file->close();
}
