#ifndef DETECTOR_H
#define DETECTOR_H
#include <vector>
#include "detectionDHU.h"
#include "opencv2/opencv.hpp"


class Detector
{
public:
    /** Default constructor */
    Detector();
    //float getX();
    //float getY();
    //float getWidth();
    //float getHeight();
    //float getScore();
    /** Default destructor */
    virtual ~Detector();
    virtual std::vector<DHU::Detection> Detect(cv::Mat im);


protected:

    //void setX(float );
    //void setY(float );
    //void setWidth(float );
    //void setHeight(float );
    //void setScore(float );
private:
};

#endif // DETECTOR_H
