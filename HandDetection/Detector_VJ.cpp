#include "Detector_VJ.h"

#include <cstdio>
#include <ctime>
using namespace std;

Detector_VJ::Detector_VJ()
{
    if(loadCascades("../haarcascade_frontalface_alt.xml","../haarcascade_profileface.xml")!=0) //frotal face and profile left face
    {
        cout << "Can't load cascades for face detection" << endl;
    }
}

Detector_VJ::~Detector_VJ()
{
    //dtor
}

int Detector_VJ::loadCascades(String face,String profileleft)
{
    if( !face_cascade.load(face) )
    {
        printf("--(!)Error loading frontal face. Face.cpp\n");
        cout << face << endl;
        return -1;
    };//For faces
    if( !profileleft_cascade.load(profileleft) )
    {
        printf("--(!)Error loading profile face. Face.cpp\n");
        return -1;
    };//For eyes
    return 0;
}

std::vector<DHU::Detection> Detector_VJ::Detect(cv::Mat im)
{
    std::clock_t startTime;
    double duration;
    startTime = std::clock();

    ///To know whether the face is looking to the right,left or front, the score (size of detections) is added with
    ///0 -> left , 1000 -> right, 2000 -> front
    //detection vector
    std::vector<DHU::Detection> det_vec;
    det_vec.clear();
    //rescale image for facedetection
    int scale = 1;

    cout << "input size = " << im.size() << endl;
    Mat smallImg( cvRound (im.rows/scale), cvRound(im.cols/scale), CV_8UC1 );
    resize( im, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    cout << "output size = " << smallImg.size() << endl;
    //find faces
    std::vector<Rect> faces;
    std::vector<Rect> profile_faces_left;
    std::vector<Rect> profile_faces_right;
    vector<float> test;
    vector<double> weight_left;
    vector<double> weight_right;
    vector<double> weight_front;
    vector<int> rejectLevels;
    faces.clear();
    profile_faces_left.clear();
    profile_faces_right.clear();
    Mat frame_gray,flipped_frame_gray;
    vector<float> v_return_value;
    int leftsize,rightsize,frontsize;
    cvtColor( smallImg, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );

    double scaleFactor = 1.1;
    int minNeighbors_score_determ = 0;
    int minNeighbors = 5;

    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces,scaleFactor, minNeighbors_score_determ, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    frontsize = faces.size();
    //--
    profileleft_cascade.detectMultiScale( frame_gray, profile_faces_left,scaleFactor, minNeighbors_score_determ, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    leftsize = profile_faces_left.size();
    //--
    flip(frame_gray, flipped_frame_gray, 1);
    profileleft_cascade.detectMultiScale( flipped_frame_gray, profile_faces_right, scaleFactor, minNeighbors_score_determ, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
    rightsize = profile_faces_right.size();
    /** @todo nog verandern !**/

    if(profile_faces_right.size() > 0 && profile_faces_right.size() > profile_faces_left.size() && profile_faces_right.size() > faces.size() )
    {
        profile_faces_right.clear();
        profileleft_cascade.detectMultiScale( flipped_frame_gray, profile_faces_right,scaleFactor, minNeighbors, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
        if(profile_faces_right.size()>0)
        {
            for(unsigned int I=0; I<profile_faces_right.size(); I++)
            {
                DHU::Detection det;
                profile_faces_right[I].x = frame_gray.cols - profile_faces_right[I].x - profile_faces_right[I].width;
                det.x=profile_faces_right[I].x*scale;
                det.y=profile_faces_right[I].y*scale;
                det.width=profile_faces_right[I].width*scale;
                det.height=profile_faces_right[I].height*scale;
                det.score = rightsize+1000; //To see the difference between left and right
                det_vec.push_back(det);     //Put detection in vector
                //cout << "x" << det.x << endl;
                //cout << "y" << det.y << endl;
            }
        }
    }
    else if(profile_faces_left.size() > 0 && profile_faces_left.size() > faces.size())
    {
        profile_faces_left.clear();
        profileleft_cascade.detectMultiScale( frame_gray, profile_faces_left,scaleFactor, minNeighbors, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
        if(profile_faces_left.size()>0)
        {
            for(unsigned int I=0; I<profile_faces_left.size(); I++)
            {
                DHU::Detection det;
                //profile_faces_left[I].x = flipped_frame_gray.cols - profile_faces_left[I].x - profile_faces_left[I].width;
                det.x=profile_faces_left[I].x*scale;
                det.y=profile_faces_left[I].y*scale;
                det.width=profile_faces_left[I].width*scale;
                det.height=profile_faces_left[I].height*scale;
                det.score = leftsize; //To see the difference between left and right
                det_vec.push_back(det);     //Put detection in vector
            }
        }
    }
    else if(faces.size()>0)
    {
        faces.clear();
        face_cascade.detectMultiScale( frame_gray, faces,scaleFactor, minNeighbors, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );
        if(faces.size()>0)
        {
            for(unsigned int I=0; I<faces.size(); I++)
            {
                DHU::Detection det;
                //faces[I].x = flipped_frame_gray.cols - faces[I].x - faces[I].width;
                det.x=faces[I].x*scale;
                det.y=faces[I].y*scale;
                det.width=faces[I].width*scale;
                det.height=faces[I].height*scale;
                det.score = leftsize+2000; //To see the difference between left and right
                det_vec.push_back(det);     //Put detection in vector
            }
        }
    }

    duration = ( std::clock() - startTime ) / (double) CLOCKS_PER_SEC;
    std::cout << "--> time face detection = "<< duration <<'\n';

    return det_vec;
}
