#ifndef PROCESSTHREAD_H
#define PROCESSTHREAD_H

#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include <QThread>

#include <string>

//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/legacy/legacy.hpp>
//#include "opencv2/ml/ml.hpp"
//#include <opencv2/gpu/gpu.hpp>


#include "opencv2/opencv.hpp"


using namespace std;
using namespace cv;

class ProcessThread : public QThread
{
    Q_OBJECT
public:   
    ProcessThread(QObject *parent = 0);
    ~ProcessThread();

    void process(string folder, int startFrame, int NrOfFrames);
    bool intersection(Point2f o1, Point2f p1, Point2f o2, Point2f p2,Point2f &r);
    void drawResult(Mat& img, RotatedRect &box, const Scalar& color, int thickness);
    double vecMed(std::vector<double> vec); 

protected:
    void run();

private:private:
    QMutex mutex;
    QWaitCondition condition;
    bool restart;
    bool abort;

    string DetectionsPath;
    int StartFrame;
    int NrOfFrames;

    bool initKalmanFace;
    int avgFaceWidth;
    int facePredictions;

    double medianU;

    vector<int> frames;
    vector<int> width;
    vector<int> height;
    vector<Point> corner;

};

#endif // PROCESSTHREAD_H
